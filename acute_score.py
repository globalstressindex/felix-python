#
# take windows data and calculate acute stress scores
# 1. recalculate percentiles using only non-sleep and non-exercise data
# 2. use a linear scoring model to evaluate 3 min windows
# 3. aggregate 3 min windows into large windows using mode/mean
#
from accel_functions import calc_percentile_features
import numpy as np


def value_to_score(value, min_value, max_value, min_score, max_score, inverse):
    value_range = max_value - min_value
    thresholded_value = min(max(value - min_value, 0.0), value_range)
    value_ratio = thresholded_value/value_range
    if inverse:
        min_score, max_score = max_score, min_score
    score = ((1-value_ratio) * min_score) + (value_ratio * max_score)
    return score


def calculate_acute_score(windows):

    windows_stress = windows.loc[(windows['awake_sleep'] == 2), ['rmssd_milliseconds', 'mean_hr', 'hrq_mean_hr', 'br_v04']].copy() # TODO ignoring exercise at the moment as it's broken
    percentile_features = windows_stress.columns
    calc_percentile_features(windows_stress, percentile_features)

    # user parameters
    # dob
    # gender
    # fitness_stairs
    # fitness_weakly excerise
    # if count(windows) not sleep/ex < 20: use predefined ranges
    # Ignore for now, implement only the percentile version

    rmssd_quantiles = windows['rmssd_milliseconds'].dropna().quantile([0.1, 0.9])
    mean_hr_quantiles = windows['mean_hr'].dropna().quantile([0.1, 0.9])
    hrq_mean_hr_quantiles = windows['hrq_mean_hr'].dropna().quantile([0.1, 0.9])
    br_v04_quantiles = windows['br_v04'].dropna().quantile([0.1, 0.9])

    # main option - RR derivatives
    windows_stress['mean_hr_score'] = windows_stress['mean_hr'].apply(lambda x: value_to_score(x, min_value=mean_hr_quantiles[0.1], max_value=mean_hr_quantiles[0.9], min_score=0.0, max_score=10.0, inverse=False))
    windows_stress['br_v04_score'] = windows_stress['br_v04'].apply(lambda x: value_to_score(x, min_value=br_v04_quantiles[0.1], max_value=br_v04_quantiles[0.9], min_score=0.0, max_score=10.0, inverse=False))
    windows_stress['rmssd_score'] = windows_stress['rmssd_milliseconds'].apply(lambda x: value_to_score(x, min_value=rmssd_quantiles[0.1], max_value=rmssd_quantiles[0.9], min_score=0.0, max_score=30.0, inverse=False))

    # fallback option - HRQ score
    windows_stress['hrq_mean_hr_score'] = windows_stress['hrq_mean_hr'].apply(lambda x: value_to_score(x, min_value=hrq_mean_hr_quantiles[0.1], max_value=hrq_mean_hr_quantiles[0.9], min_score=0.0, max_score=10.0, inverse=False))

    # if all RR options deliver NA/0 - check what HRQ says
#    windows_stress.fillna(0, inplace=True)

    # TODO here - improve how rescaling is done (100/max(sum_scores))
    windows_stress['score'] = (windows_stress['mean_hr_score'] + windows_stress['br_v04_score'] + windows_stress['rmssd_score'])*2

    windows_stress.loc[windows_stress['score'].isnull(), 'score'] = windows_stress['hrq_mean_hr_score']*10
    windows_stress.loc[windows_stress['score'] == 100] = 99   # TODO upper bound is not included to avoid 4 + 2 = 6 (exercise) on the next line
    windows_stress['acute_score'] = np.floor(windows_stress['score']/25) + 2

    # update windows with the score
    windows['acute_score'] = np.nan
    windows.update(windows_stress['acute_score'])
