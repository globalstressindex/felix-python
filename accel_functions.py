import numpy as np
import pandas as pd 
from options import Options
from heart_functions import *
from breathing_rate.breathing_rate import *
from breathing_rate.breathing_rate_v04 import *
from rr_features import lfhf_welch, lfhf_ls, lfhf_ls_astro

def calc_accel_features(accel_data):
    """
    accel_models: [Acceleration]

    returns AccelFeatures
    """
    if len(accel_data):
        windows = pd.DataFrame(index=accel_data['window'].unique(),
                               dtype='float64')
        
        accel_data['3d'] = np.linalg.norm(accel_data[['x','y','z']],axis=1) # faster than apply norm axis=1
     
        accel_windows = accel_data.groupby('window')
     
        # windows = windows.join(accel_windows['x'].mean().rename('accel_mean_x'))
        # windows = windows.join(accel_windows['y'].mean().rename('accel_mean_y'))
        # windows = windows.join(accel_windows['z'].mean().rename('accel_mean_z'))
        windows = windows.join(accel_windows['3d'].mean().rename('accel_mean_3d_magnitude'))
        # windows = windows.join(accel_windows['3d'].std().rename('accel_std_3d_magnitude'))
        # windows = windows.join(accel_windows['3d'].max().rename('accel_max_3d_magnitude'))

    else:

        windows = pd.DataFrame(index=accel_data['window'].unique(), columns = [# 'accel_mean_x',
                                                                               # 'accel_mean_y',
                                                                               # 'accel_mean_z',
                                                                               'accel_mean_3d_magnitude'
        ], dtype='float64')
    
    return windows

percentile_features = ['rmssd_milliseconds',
                       'mean_hr',
                       'hrq_mean_hr',
                       'br_v04']

def calc_percentile_features(windows, percentile_features):
    """
    takes a list of fields for which to compute the CDF
    as of now also rounds down to the nearest decile
    adds the percentile feature columns back to the original DF
    """
    for feature in percentile_features:
        windows[feature+'_rank'] = windows[feature].rank(ascending=0)
        windows[feature+'_cdf'] = (len(windows[feature].dropna()) - windows[feature+'_rank'] + 1)/len(windows[feature].dropna())
        # TODO for now round down to the nearest decile 
        windows[feature+'_per01ile_all'] = np.floor(windows[feature+'_cdf']*10)/10
        windows.drop(feature+'_rank',axis=1,inplace=True)
        windows.drop(feature+'_cdf',axis=1,inplace=True)


def frequency_features(r2r):
    """
    frequency domain analysis
    """
    windows = pd.DataFrame(index=r2r['window'].unique(),
                           columns=['lf_a',
                                    'hf_a',
                                    'vlf_a',
                                    'tot_a'
                                ],
                           dtype='float64')    
    r2r_windows = r2r.groupby('window')                        
    if len(r2r_windows):
        windows.update(r2r_windows.apply(lambda group: lfhf_ls_astro(group)))  # lf_a, hf_a, vlf_a, tot_a

    return windows
        
def breathing_features(r2r):
    """
    calculates breathing rate and related things
    """
    windows = pd.DataFrame(index=r2r['window'].unique(),
                           columns=['br_v04'
                                ],
                           dtype='float64')    
    r2r_windows = r2r.groupby('window')                        
    if len(r2r_windows):    
        # Breathing rate v4 - need for current stress model
        fc_options = Options()
        windows['br_v04'].update(r2r_windows.apply(lambda x: calc_br_v04(x['rr_interval'].reset_index(drop=True),fc_options)))

    return windows        
        
def calc_base_features(r2r, hrquality):
    """
    calculates the basic features
    """

    windows = pd.DataFrame(index=r2r['window'].unique(),
                           columns=['rmssd_milliseconds',
                                    'mean_hr',
                                    'sdnn',
                                    'hrq_mean_hr'
                                ],
                           dtype='float64')
    if len(r2r):            
        r2r_windows = r2r.groupby('window')
    else:
        r2r_windows = pd.DataFrame()

    if len(hrquality):    
        hrq_windows = hrquality.groupby('window')
    else:
        hrq_windows = pd.DataFrame()

    if len(r2r_windows):
        windows['rmssd_milliseconds'].update(r2r_windows.apply(lambda x: calc_rmssd(x['rr_interval'])))
        windows['sdnn'].update(r2r_windows.rr_interval.std(ddof=1))
        ## TODO - should be a better place to deal with this
        ## converting to ms as the classifiers are trained this way
        windows['rmssd_milliseconds'] = windows['rmssd_milliseconds']*1000
        windows['mean_hr'].update(r2r_windows.apply(lambda x: calc_mean_hr(x['rr_interval'])))


    if len(hrq_windows)>0:             # sometimes we don't have hrq data
        windows['hrq_mean_hr'].update(hrq_windows.apply(lambda x: calc_mean_hr_from_hrq(x['heartrate'])))

    return windows


def calc_local_time_features(windows):
    """
    computes distance from the nearest  3am point
    adds the columns to the original DF
    """
    windows['day_second'] = windows.index.hour * 3600 + windows.index.minute * 60 + windows.index.second
    windows['3h_dist_sec'] = windows['day_second'].apply(lambda x: min([abs(x - i) for i in [3*3600,27*3600]]))
