from sqlalchemy import create_engine
from sklearn import tree
#from sklearn.cross_validation import StratifiedKFold
from sklearn.model_selection import GridSearchCV, cross_val_score, KFold, StratifiedKFold
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.feature_selection import SelectFromModel
from sklearn.preprocessing import PolynomialFeatures
from sklearn import metrics
import numpy as np


try:
    con = create_engine('postgresql:///felix')
except:
    raise Exception('Database not found')
            
NUM_TRIALS = 10 

## TEST 1 - USE ABSOLUTE VALUES

X_learn = pd.read_sql('mit_bih_5m',con)

X_learn = X_learn.dropna(subset=['sleep'])

X_learn.drop(['zwhen','NREM','record','stage','deep'],axis=1,inplace=True) 

X = X_learn.drop('sleep',axis=1)
y = X_learn['sleep']


def CV_dt(X,y):
    dt = tree.DecisionTreeClassifier()
    parameter_grid = {#'criterion': ['gini', 'entropy'],
    #                  'splitter': ['best', 'random'],
                      'max_depth': [3, 4, 5, 6, 7, 8]}#,
    #                  'max_features': list(range(1,X.shape[1]+1))}
     

    non_nested_scores = np.zeros(NUM_TRIALS)
    nested_scores = np.zeros(NUM_TRIALS)
    
    for i in range(NUM_TRIALS):
     
    #    i = 0 
        inner_cv = StratifiedKFold(n_splits=10, shuffle=True, random_state=i)
        outer_cv = StratifiedKFold(n_splits=10, shuffle=True, random_state=i)
     
        ## non-nested parameter search
     
        clf = GridSearchCV(estimator=dt,
                           param_grid=parameter_grid,
                           cv=inner_cv)
        clf.fit(X,y)
        non_nested_scores[i] = clf.best_score_
     
        ## nested CV
        nested_score = cross_val_score(clf, X, y, cv=outer_cv)
        nested_scores[i] = nested_score.mean()
     
     
    score_difference = non_nested_scores - nested_scores
     
    print("DT Average difference of {0:6f} with std. dev. of {1:6f}."
           .format(score_difference.mean(), score_difference.std()))
    print('DT nested_mean', nested_scores.mean())
    print('DT non-nested_mean', non_nested_scores.mean())    
        
    return clf.best_estimator_


def CV_lr(X,y):
    
    non_nested_scores = np.zeros(NUM_TRIALS)
    nested_scores = np.zeros(NUM_TRIALS)
    
    for i in range(NUM_TRIALS):
     
    #    i = 0 
        inner_cv = StratifiedKFold(n_splits=10, shuffle=True, random_state=i)
        outer_cv = StratifiedKFold(n_splits=10, shuffle=True, random_state=i)
     
        ## non-nested parameter search
        clf = LogisticRegressionCV(cv=inner_cv,penalty='l1',solver='liblinear')
        clf.fit(X,y)
        non_nested_scores[i] = clf.score(X,y)
     
        ## nested CV
        nested_score = cross_val_score(clf, X, y, cv=outer_cv)
        nested_scores[i] = nested_score.mean()
     
     
    score_difference = non_nested_scores - nested_scores
     
    print("LR Average difference of {0:6f} with std. dev. of {1:6f}."
           .format(score_difference.mean(), score_difference.std()))
    print('LR nested_mean', nested_scores.mean())
    print('LR non-nested_mean', non_nested_scores.mean())    
        
    return clf


for best_clf in [CV_dt(X,y), CV_lr(X,y)]:
     preds = best_clf.predict_proba(X)[:,1]
     fpr, tpr, threshold = metrics.roc_curve(X_learn.sleep, preds)
     roc_auc = metrics.auc(fpr, tpr)
     plt.figure()
     plt.title('Receiver Operating Characteristic')
     plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
     plt.legend(loc = 'lower right')
     plt.plot([0, 1], [0, 1],'r--')
     plt.xlim([0, 1])
     plt.ylim([0, 1])
     plt.ylabel('True Positive Rate')
     plt.xlabel('False Positive Rate')


