import pandas  as pd 
from astropy.stats import LombScargle
from scipy import signal
from scipy.signal import lombscargle, welch
import numpy as np


from scipy import signal
import matplotlib.pyplot as plt

def br_welch(rr_window):
    """
    Gets a RR window from a groupby on the raw RR data
    Returns breathing rate - peak frequency of the LF band
    Uses Welch method to compute the PSD
    Arguments:
    - `rr_window`:
    """
    if len(rr_window) < 64:
        return pd.Series({'lf_w': np.nan, 'hf_w': np.nan, 'lfhf_w': np.nan})
    rr_window.index = pd.to_timedelta(rr_window.rr_interval.cumsum(),'s')   # milliseconds
    rr_4hz = rr_window.resample('250L').mean()
    rr_4hz.index = rr_4hz.index.total_seconds()
    rr_inter = rr_4hz.interpolate(method='spline',order=3,s=0)
    y = rr_inter.rr_interval

    Fxx, Pxx = welch(y-y.mean(),fs=4,nperseg=256, noverlap=128, detrend=False)

    plt.figure()
    plt.plot(Fxx[Fxx <= 0.4], Pxx[Fxx <= 0.4])
    plt.axvline(0.003,color='k',linestyle='--',lw=0.5)
    plt.axvline(0.04,color='k',linestyle='--',lw=0.5)
    plt.axvline(0.15,color='k',linestyle='--',lw=0.5)
    plt.axvline(0.4,color='k',linestyle='--',lw=0.5)
#    plt.xticks([0,5,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24, 30, 40, 50], rotation='vertical')
    peak = np.argmax(Pxx[(Fxx>=0.15) & (Fxx<=0.4)])
    peak_freq = Fxx[(Fxx>=0.15) & (Fxx<=0.4)][peak]

    return  pd.Series({'br_w': 60*peak_freq})




def br_ls_astro(rr_window, plot=False):
    """
    Gets a RR window from a groupby on the raw RR data
    Returns breathing rate - peak frequency of the LF band
    Uses Welch method to compute the PSD
    Arguments:
    - `rr_window`:
    """
    # if len(rr_window) < 30:
    #     return pd.Series({'br_a': np.nan})
    #x = (rr_window.index - rr_window.index.min()).total_seconds()
    x = rr_window['rr_interval'].cumsum()    # NOT using db timestamps as they are meaningless !
    # y = signal.detrend(rr_window.rr_interval,type='linear')
    # y = detrend(rr_window.rr_interval)
    y = np.array(rr_window['rr_interval'] - rr_window['rr_interval'].mean())  # no need to interpolate
    Fxx = np.linspace(1e-10, 0.4, 10000)
    Pxx = LombScargle(x,y).power(Fxx,normalization='psd')
    # Pxx = 2*Pxx*(x.max()/len(x))
    if plot:
        plt.figure()
        plt.plot(Fxx, Pxx)
        plt.axvline(0.003,color='k',linestyle='--',lw=0.5)
        plt.axvline(0.04,color='k',linestyle='--',lw=0.5)
        plt.axvline(0.15,color='k',linestyle='--',lw=0.5)
        plt.axvline(0.4,color='k',linestyle='--',lw=0.5)
#    plt.xticks([0,5,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24, 30, 40, 50], rotation='vertical')
    peak = np.argmax(Pxx[(Fxx>=0.15) & (Fxx<=0.4)])
    peak_freq = Fxx[(Fxx>=0.15) & (Fxx<=0.4)][peak]
    return  pd.Series({'br_a': 60*peak_freq})



def spectrum_ls_astro(rr_window):
    x = rr_window['rr_interval'].cumsum()   # NOT using db timestamps as they are meaningless
    y = np.array(rr_window['rr_interval'] - rr_window['rr_interval'].mean())  # no need to interpolate
    Fxx = np.linspace(1e-10, 0.4, 1000)
    Pxx = LombScargle(x,y).power(Fxx,normalization='psd')    
    return np.round(60*Fxx,1), Pxx

def spectrum_welch(rr_window):
    rr_window.index = pd.to_timedelta(rr_window.rr_interval.cumsum(),'s')   # milliseconds
    rr_4hz = rr_window.resample('250L').mean()
    rr_4hz.index = rr_4hz.index.total_seconds()
    rr_inter = rr_4hz.interpolate(method='spline',order=3,s=0)
    y = rr_inter.rr_interval
    Fxx, Pxx = welch(y-y.mean(),fs=4,nperseg=256, detrend=False)
    return np.round(60*Fxx[Fxx<=0.4],1), Pxx[Fxx<=0.4]



def spectro_ls(rr_window):
## plotting
# sb.heatmap(spectro_ls(r2r.ix['2017-02-09 11:06:00':'2017-02-09 11:27:00']),yticklabels=40,xticklabels=10)
    powers = []
    for window in rr_window.index.floor('5T').unique():
        Fxx, Pxx = spectrum_ls_astro(rr_window.ix[rr_window.index.floor('5T') == window])
        powers.append(Pxx)
    specgram = pd.DataFrame(dict(zip(rr_window.index.floor('5T').unique(),powers)),index=Fxx)
    specgram.sort_index(ascending=False,inplace=True)
    return specgram


def spectro_welch(rr_window):
## plotting
# sb.heatmap(spectro_ls(r2r.ix['2017-02-09 11:06:00':'2017-02-09 11:27:00']),yticklabels=40,xticklabels=10)
    powers = []
    for window in rr_window.index.floor('5T').unique():
        Fxx, Pxx = spectrum_welch(rr_window.ix[rr_window.index.floor('5T') == window])
        powers.append(Pxx)
    specgram = pd.DataFrame(dict(zip(rr_window.index.floor('5T').unique(),powers)),index=Fxx)
    specgram.sort_index(ascending=False,inplace=True)
    return specgram




def artificial_rr():
    fs = 1000
    seconds = 300
    time = np.arange(0,seconds,1/fs)
    hr = lambda x: 60 + 2*np.sin(0.095*(2*np.pi)*x) + 2.5*np.sin(0.275*(2*np.pi)*x+0) ### from clifford's paper
    #hr = lambda x: 60 + 2.5*np.sin(0.275*(2*np.pi)*x+0) ### from clifford's paper
    timesamples = []
    rr = []
    ti = 0
    while time[ti] <= seconds:
        timesamples.append(ti)
        rr.append(60/hr(time[ti]))
        try:
            ti = np.min(np.where(time >= sum(60/hr(time[timesamples]))))
        except:
            break
    return timesamples,rr







