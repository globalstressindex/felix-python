###
##  This is a stage 1 solution of [DEV-333], hence baselines are chosen as just distributions
##  over all available data. No attempt to optimize further is made, as the "finals" (see below)
##  are meant to be evolving over time, hence a better "dynamic" solution planned for stage 3 is
##  going to address this issue. Same applies to the threshold determination.
##  Stage 2 - modify baselines with gender/fitness parameters
##  Stage 3 - bayesian updating of the distribution from Stage 2. 
##

from sklearn import linear_model
con = create_engine('postgresql:///felix')

windows = pd.read_sql('zinsightcalc3',con)
windows.start_time = pd.to_datetime(windows.start_time,unit='s') + APPLE_DELTA
windows.set_index('start_time',inplace=True)

data = windows.loc[(windows.exercising_or_not == 0) & (windows.awake_sleep == 2), ['userid','rmssd_milliseconds','mean_hr','br_v04']].dropna()

## RMSSD milliseconds
## Generate BASELINE distr as a mixture of all data (percentiles 0-100)

baseline = data.rmssd_milliseconds.quantile(np.linspace(0,1,101))

## Generate FINAL distributions for each user (percentiles 0-100)

finals = data.groupby('userid').rmssd_milliseconds.quantile(np.linspace(0,1,101)).unstack(level=0)
finals['baseline'] = baseline

## Generate "after day x" distr for each user and compute the difference (L2, Linf) between it and FINAL
## and FINAL and BASELINE. Note the day when own histogram becomes better

results = pd.DataFrame(columns=['user','start_time','l2','linf']);

## Extreme percentiles are quite volatile, so we use the 3-97 range for threshold estimation.
## As explained above, at this stage the solution is deliberately simple.

for user in data.userid.unique():
    userdata = data.loc[data.userid == user].copy()
    userdata.sort_index(inplace=True)
    userdata['day'] = userdata.index.floor('d')
    for day in sorted(userdata.day.unique())[1:]:   # sort the days and start from day2
        refquant = userdata.loc[day+pd.to_timedelta(-30,'d'):day].rmssd_milliseconds.quantile(np.linspace(0,1,101))
        l2 = norm(finals.loc[0.03:0.97][user] - refquant.loc[0.03:0.97],2)
        linf = norm(finals.loc[0.03:0.97][user] - refquant.loc[0.03:0.97],inf)
        results = results.append({'user':user,'start_time':day,'l2':l2, 'linf':linf},ignore_index=True)


results.set_index(['user','start_time'],inplace=True)

for user in data.userid.unique():
    ax = results.loc[user].plot(marker='o', title='user'+str(user))
    ax.axhline(norm((finals.loc[0.03:0.97][user] - baseline.loc[0.03:0.97]),2),color='k',linestyle='--',lw=0.5, label = 'base l2')
    ax.axhline(norm((finals.loc[0.03:0.97][user] - baseline.loc[0.03:0.97]),inf),color='r',linestyle='--',lw=0.5, label = 'base linf')
    ax.legend()


### EXPLORE THE FOLLOWING TO SEE WHEN LINF becomes better than the baseline
### about ~500 samples seems to be a good compromise (don't want to keep it for too long)

data['day'] = data.index.floor('d')
data.groupby(['userid','day']).size().unstack(level=0).cumsum()


###
###  STAGE 2   DEV-356
###

## How do user parameters impact the baseline?

### Not much difference (as of now) in parameters other than rmssd

data.groupby('userid').mean_hr.describe()

#          count       mean        std        min        25%        50%        75%         max
# userid                                                                                       
# 2.0     3095.0  78.037610  11.658721  52.440342  69.657946  75.392996  84.283206  118.145538 
# 4.0     1152.0  77.322131  14.490704  49.356732  66.747710  73.469521  86.270641  117.809934 
# 7.0     1005.0  78.376746  12.496555  53.112151  68.852323  75.843592  85.051999  118.267994 
# 8.0     3530.0  76.530842  12.246536  49.670119  67.614936  74.235698  83.223484  119.085680 
# 9.0     1646.0  76.597730  10.403568  54.782812  69.188114  74.067247  81.455108  116.576310 

   
data.groupby('userid').rmssd_milliseconds.describe()

#          count       mean        std        min        25%        50%        75%         max
# userid                                                                                       
# 2.0     3095.0  60.180446  20.843562   7.018779  45.627003  58.959037  73.359845  180.618995 
# 4.0     1152.0  63.665553  22.151876   9.186602  48.239716  62.603820  78.150012  148.845698 
# 7.0     1005.0  71.581340  23.477682   8.424020  55.667619  69.759285  85.868714  176.095818 
# 8.0     3530.0  78.232155  22.856631  13.549953  62.370495  77.516219  92.288602  200.248726 
# 9.0     1646.0  73.884302  20.339787  22.031363  59.804052  73.032134  86.709904  180.914540 

    
data.groupby('userid').br_v04.describe()

#          count       mean       std       min        25%        50%        75%        max
# userid                                                                                    
# 2.0     3095.0  18.912314  4.302373  5.763412  16.199798  18.478596  21.265285  47.942469 
# 4.0     1152.0  18.130765  5.643690  6.034396  14.278501  17.199370  21.248340  47.058824 
# 7.0     1005.0  19.431968  3.667563  8.664260  16.949153  19.286403  21.671608  36.900369 
# 8.0     3530.0  18.660442  3.624237  6.722689  16.280016  18.399264  20.790021  41.152263 
# 9.0     1646.0  18.827774  3.780573  6.922811  16.399044  18.455860  20.942408  37.371535 

### very similar - differences probably due to missing data     
baseline = data.mean_hr.quantile(np.linspace(0,1,101))
finals = data.groupby('userid').mean_hr.quantile(np.linspace(0,1,101)).unstack(level=0)
finals['baseline'] = baseline
finals.plot()

## not much trust in BR anyway. 
baseline = data.br_v04.quantile(np.linspace(0,1,101))
finals = data.groupby('userid').br_v04.quantile(np.linspace(0,1,101)).unstack(level=0)
finals['baseline'] = baseline
finals.plot()

    
### now let's join vitals
about = pd.read_sql('about',con)
about.set_index('userid',inplace=True)

data = data.join(about, on='userid')
data['bmi'] = data['height']/data['weight']

# data[['userid','dob','bmi']].drop_duplicates()
# userid          dob       bmi
                             
#      8 -419299200.0  2.608696
#      9 -513561600.0  2.459459
#      7 -631152000.0  2.542857
#      2 -939254400.0  2.337838
#      4 -967939200.0  2.346154

data[['dob','bmi']].drop_duplicates().corr()

## pretty correlated
#           dob       bmi
# dob  1.000000  0.903497
# bmi  0.903497  1.000000

#
sb.boxplot(x="dob", y="rmssd_milliseconds", data=data)
sb.boxplot(x="bmi", y="rmssd_milliseconds", data=data)

# DOB seems to be a good predictor, linear model fits well
sb.lmplot(x="dob", y="rmssd_milliseconds", data=data, x_estimator=np.mean)
sb.lmplot(x="bmi", y="rmssd_milliseconds", data=data, x_estimator=np.mean)

### However, we need model each quantile of the CDF  (finals)
## Quanitile regression? Do something simpler instead

X  = data.groupby('userid')[['dob','bmi']].mean().values
finals = data.groupby('userid').rmssd_milliseconds.quantile(np.linspace(0,1,101)).unstack(level=0)
lr = linear_model.LinearRegression()

finals_hat = pd.DataFrame(columns=finals.columns, index=finals.index)
coef = pd.DataFrame(columns = ['intercept', 'dob', 'bmi'], index = finals.index)

for i in linspace(0,1,101):    
    y = finals.loc[i].values
    lr.fit(X,y)
    finals_hat.loc[i] = lr.predict(X)
    coef.loc[i] = r_[lr.intercept_, lr.coef_]

### ARMA MAT FORMAT
print(coef.to_csv(sep=" ", line_terminator=";"))
    


## DYNAMIC BASELINE GENERATION [DEV-357]

## starting with some prior we adjust it as samples come along
## first thing to verify is stationarity
## in principle, I do not expect stationarity, but this could be a good first approximation

## the data has gaps which is (in principle) a problem. Below I simply
## "close" them by adjoining various pieces of data into an indexed
## (not time-indexed) array. This is not great, and might be even
## completely wrong, but I'm reluctant to interpolate (gaps are too
## large) and I don't see a better way for now, given the data that we
## have.  It's ok to close the "exercise" and "sleep" gaps in this
## way, but it can be a problem if there is a gap which spans a few
## days.

## Generally, my expectation is that the data is "slow-changing" in
## the sense that it is stationary given a certain window. For
## example, it is stationary in the normal conditions, but if I go on
## vacation to a hot country, my HR and HRV would be affected during
## that time, though also "locally" stationary.

##
## Remember, that we look at the averages of data over 3 min periods,
## hence CLT applies and indeed distributions are quite close to
## normal (visually)


from statsmodels.tsa.stattools import adfuller

## with time removed, data visually looks stationary
## quick ADF check also strongly rejects non-stationarity 
for user in windows.userid.unique():
    data = windows.loc[(windows.exercising_or_not == 0) & (windows.awake_sleep == 2) & (windows.userid == user),'rmssd_milliseconds'].values
    result = adfuller(data)
    print('userid: ' ,user)
    print('ADF Statistic: %f' % result[0])
    print('p-value: %f' % result[1])
    print('Critical Values:')
    for key, value in result[4].items():
        print('\t%s: %.3f' % (key, value))
    plt.plot(data, label = 'user: '+str(user))
    
ax = plt.gca()
ax.legend()



   
   
