rr01a = pd.read_csv('/home/gb/felix/research/sleep classification/data/mit-bih/www.physionet.org/physiobank/database/slpdb/slp01a.csv',sep='\t', header=None)
rr01b = pd.read_csv('/home/gb/felix/research/sleep classification/data/mit-bih/www.physionet.org/physiobank/database/slpdb/slp01b.csv',sep='\t', header=None)
rr01 = pd.concat([rr01a,rr01b])
rr01.columns=['rr_interval','zwhen']
rr01.zwhen = pd.to_datetime(rr01.zwhen,format='[%H:%M:%S.%f %d/%m/%Y]')
rr01.set_index('zwhen',inplace=True)
rr01.plot()


ann01a = pd.read_csv('/home/gb/felix/research/sleep classification/data/mit-bih/www.physionet.org/physiobank/database/slpdb/anslp01a.csv',sep='\t', header=None,skiprows=1)
ann01b = pd.read_csv('/home/gb/felix/research/sleep classification/data/mit-bih/www.physionet.org/physiobank/database/slpdb/anslp01b.csv',sep='\t', header=None,skiprows=1)
ann01 = pd.concat([ann01a,ann01b])
ann01.columns=['zwhen','stage']
ann01.zwhen = pd.to_datetime(ann01.zwhen,format='[%H:%M:%S.%f %d/%m/%Y]')
ann01.set_index('zwhen',inplace=True)
ann_map={'W':6,'R':5,'1':4,'2':3,'3':2,'4':2, 'MT':8}   ### According to 2007 AASM rules R&K stages 3,4 are merged in N3
plt.figure()
ax = ann01.stage.apply(lambda x: ann_map[x]).plot()
ax.yaxis.set_major_locator(FixedLocator([2,3,4,5,6,8]))
ax.yaxis.set_ticklabels(['N3','N2','N1','R','W','MT'])


rr02a = pd.read_csv('/home/gb/felix/research/sleep classification/data/mit-bih/www.physionet.org/physiobank/database/slpdb/slp02a.csv',sep='\t', header=None)
rr02b = pd.read_csv('/home/gb/felix/research/sleep classification/data/mit-bih/www.physionet.org/physiobank/database/slpdb/slp02b.csv',sep='\t', header=None)
rr02 = pd.concat([rr02a,rr02b])
rr02.columns=['rr_interval','zwhen']
rr02.zwhen = pd.to_datetime(rr02.zwhen,format='[%H:%M:%S.%f %d/%m/%Y]')
rr02.set_index('zwhen',inplace=True)
rr02.plot()


ann02a = pd.read_csv('/home/gb/felix/research/sleep classification/data/mit-bih/www.physionet.org/physiobank/database/slpdb/anslp02a.csv',sep='\t', header=None,skiprows=1)
ann02b = pd.read_csv('/home/gb/felix/research/sleep classification/data/mit-bih/www.physionet.org/physiobank/database/slpdb/anslp02b.csv',sep='\t', header=None,skiprows=1)
ann02 = pd.concat([ann02a,ann02b])
ann02.columns=['zwhen','stage']
ann02.zwhen = pd.to_datetime(ann02.zwhen,format='[%H:%M:%S.%f %d/%m/%Y]')
ann02.set_index('zwhen',inplace=True)
ann_map={'W':6,'R':5,'1':4,'2':3,'3':2,'4':2, 'MT':8}   ### According to 2007 AASM rules R&K stages 3,4 are merged in N3
plt.figure()
ax = ann02.stage.apply(lambda x: ann_map[x]).plot()
ax.yaxis.set_major_locator(FixedLocator([2,3,4,5,6,8]))
ax.yaxis.set_ticklabels(['N3','N2','N1','R','W','MT'])




rr03 = pd.read_csv('/home/gb/felix/research/sleep classification/data/mit-bih/www.physionet.org/physiobank/database/slpdb/slp03.csv',sep='\t', header=None)
rr03.columns=['rr_interval','zwhen']
rr03.zwhen = pd.to_datetime(rr03.zwhen,format='[%H:%M:%S.%f %d/%m/%Y]')
rr03.set_index('zwhen',inplace=True)
rr03.loc[rr03.rr_interval < 2].plot()


ann03 = pd.read_csv('/home/gb/felix/research/sleep classification/data/mit-bih/www.physionet.org/physiobank/database/slpdb/anslp03.csv',sep='\t', header=None,skiprows=1)
ann03.columns=['zwhen','stage']
ann03.zwhen = pd.to_datetime(ann03.zwhen,format='[%H:%M:%S.%f %d/%m/%Y]')
ann03.set_index('zwhen',inplace=True)
ann_map={'W':6,'R':5,'1':4,'2':3,'3':2,'4':2, 'MT':8}   ### According to 2007 AASM rules R&K stages 3,4 are merged in N3
plt.figure()
ax = ann03.stage.apply(lambda x: ann_map[x]).plot()
ax.yaxis.set_major_locator(FixedLocator([2,3,4,5,6,8]))
ax.yaxis.set_ticklabels(['N3','N2','N1','R','W','MT'])




rr04 = pd.read_csv('/home/gb/felix/research/sleep classification/data/mit-bih/www.physionet.org/physiobank/database/slpdb/slp04.csv',sep='\t', header=None)
rr04.columns=['rr_interval','zwhen']
rr04.zwhen = pd.to_datetime(rr04.zwhen,format='[%H:%M:%S.%f %d/%m/%Y]')
rr04.set_index('zwhen',inplace=True)
rr04.loc[rr04.rr_interval < 2].plot()


ann04 = pd.read_csv('/home/gb/felix/research/sleep classification/data/mit-bih/www.physionet.org/physiobank/database/slpdb/anslp04.csv',sep='\t', header=None,skiprows=1)
ann04.columns=['zwhen','stage']
ann04.zwhen = pd.to_datetime(ann04.zwhen,format='[%H:%M:%S.%f %d/%m/%Y]')
ann04.set_index('zwhen',inplace=True)
ann_map={'W':6,'R':5,'1':4,'2':3,'3':2,'4':2, 'MT':8}   ### According to 2007 AASM rules R&K stages 3,4 are merged in N3
plt.figure()
ax = ann04.stage.apply(lambda x: ann_map[x]).plot()
ax.yaxis.set_major_locator(FixedLocator([2,3,4,5,6,8]))
ax.yaxis.set_ticklabels(['N3','N2','N1','R','W','MT'])

def aggr_stage(window):
    wmode = window.mode()
    if len(wmode) != 1:     # empty or multiple modes
        return pd.Series({'stage':nan})
    else:        
        return pd.Series({'stage':wmode.ix[0].stage})

w = []

for rec in ['slp01a', 'slp01b', 'slp02a', 'slp02b', 'slp03' , 'slp04' , 'slp14' , 'slp16' , 'slp32' , 'slp37' , 'slp41' , 'slp45' , 'slp48' , 'slp59' , 'slp60' , 'slp61' , 'slp66']:
    r2r = pd.read_csv('/home/gb/felix/research/sleep classification/data/mit-bih/www.physionet.org/physiobank/database/slpdb/'+rec+'.csv',sep='\t', header=None)
    r2r.columns=['rr_interval','zwhen']
    r2r = r2r.loc[r2r.rr_interval < 2] ##  filtering
    r2r.zwhen = pd.to_datetime(r2r.zwhen,format='[%H:%M:%S.%f %d/%m/%Y]')
    r2r.set_index('zwhen',inplace=True)
    r2r['window'] = r2r.index.floor('5T')
    windows = pd.DataFrame(index=r2r['window'].unique(),
                           columns=['rmssd_milliseconds',
                                    'mean_hr',
                                    'sdnn',
                                    'lf_a',
                                    'hf_a',
                                    'vlf_a',
                                    'tot_a',
                                    'lfhf_a',
                                    'stage'
                                ],
                           dtype='float64')
    r2r_windows = r2r.groupby('window')
    windows['rmssd_milliseconds'].update(r2r_windows.apply(lambda x: calc_rmssd(x['rr_interval'])))
    windows['sdnn'].update(r2r_windows.rr_interval.std(ddof=1))
    windows['mean_hr'].update(r2r_windows.apply(lambda x: calc_mean_hr(x['rr_interval'])))
    windows.update(r2r_windows.apply(lambda group: lfhf_ls_astro(group)))  # lf_a, hf_a, lfhf_a
    windows['lfhf_a'] = windows['lf_a']/windows['hf_a']

    
    ann = pd.read_csv('/home/gb/felix/research/sleep classification/data/mit-bih/www.physionet.org/physiobank/database/slpdb/an'+rec+'.csv',sep='\t', header=None,skiprows=1)
    ann.columns=['zwhen','stage']
    ann.zwhen = pd.to_datetime(ann.zwhen,format='[%H:%M:%S.%f %d/%m/%Y]')
    ann.set_index('zwhen',inplace=True)
    ann = ann.groupby(ann.index.floor('5T')).apply(aggr_stage)
    windows.update(ann)
    windows.dropna(inplace=True)

    windows['record'] = rec

    w.append(windows)

    
