import pandas as pd
import seaborn as sb
from sklearn import preprocessing
from sklearn.cluster import KMeans
import scipy
import numpy as np

from sqlalchemy import create_engine
con = create_engine('postgresql:///felix')

windows = pd.read_sql('windows_0309',con)
windows.set_index('start_time',inplace=True)


## look at how the old classifier performs

from trained_models.trained_utils import make_own_lr_classifier

### dist_3h_sec has adjustments for TZ which were made on the devices when the feedback was given
### but not present when recalculating all windows in c++ when recreating windows from raw data
features = ['mean_hr','rmssd_milliseconds','sdnn','lf_a','hf_a','dist_3h_sec']

#### current version of the classifier - cpp/classifiers/share/sleepClassifierV04_LR--2017-06-02.xml
coefficients = [-0.5698331517417778, -0.0532191077676702, 0.07389884320679085, 0.008576236879670152, 0.0066256330079018065, -0.0016593277708444425]
intercept = 71.54018075296419
classes = [0,1]
lrsleep = make_own_lr_classifier(features, coefficients, intercept, classes)

### drop a few rows without data
windows.dropna(subset = features, inplace=True)
X = windows[features]

windows['sleep']=lrsleep.predict(X)
windows['sleep_proba']=lrsleep.predict_proba(X)[:,1]
windows['sleep_linear'] = np.dot(X.values, coefficients) + intercept


## Assumption - week is the proper seasonality period
## waking up later on weekends/ earlier on weekdays

windows['tstamp'] = pd.to_datetime(windows.index,unit='s') + APPLE_DELTA + pd.to_timedelta(1,'h')

### TZ adjustment
windows.loc[(windows.userid == 24) & (windows.index >= 525257417.5342), 'tstamp'] += pd.to_timedelta(1,'h');

windows.loc[(windows.userid == 32) & (windows.index >= 523277598.9397) & (windows.index <= 523626927.5176), 'tstamp'] += pd.to_timedelta(1,'h');
 
windows.loc[(windows.userid == 37) & (windows.index >= 524656918.5334) & (windows.index <= 525265216.4842), 'tstamp'] += pd.to_timedelta(1,'h');


windows['minuteweek'] = windows['tstamp'].dt.dayofweek * 24 * 60 + windows['tstamp'].dt.hour * 60 + windows['tstamp'].dt.minute


### get a slightly smoothed version of the periodic signal which can be used as a feature
### replacing dist_3h_sec

medhr = windows.groupby(['userid','minuteweek']).mean_hr.median().unstack(level=0).median(axis=1)

medhr_filt = pd.Series(scipy.signal.medfilt(medhr, 21), index= medhr.index)

windows['medhr'] = medhr_filt[windows.minuteweek].values

### labelling - very conservative

windows.sleep_gt = nan

windows.loc[(windows.minuteweek.between(474,1335)), 'sleep_gt'] = 0
windows.loc[(windows.minuteweek.between(1920,2706)), 'sleep_gt'] = 0
windows.loc[(windows.minuteweek.between(3399,4215)), 'sleep_gt'] = 0
windows.loc[(windows.minuteweek.between(4844,5645)), 'sleep_gt'] = 0
windows.loc[(windows.minuteweek.between(6221,7077)), 'sleep_gt'] = 0
windows.loc[(windows.minuteweek.between(7848,8520)), 'sleep_gt'] = 0
windows.loc[(windows.minuteweek.between(9314,9906)), 'sleep_gt'] = 0

windows.loc[(windows.minuteweek.between(0,406)), 'sleep_gt'] = 1
windows.loc[(windows.minuteweek.between(1412,1857)), 'sleep_gt'] = 1
windows.loc[(windows.minuteweek.between(2928,3282)), 'sleep_gt'] = 1
windows.loc[(windows.minuteweek.between(4317,4718)), 'sleep_gt'] = 1
windows.loc[(windows.minuteweek.between(5817,6174)), 'sleep_gt'] = 1
windows.loc[(windows.minuteweek.between(7269,7585)), 'sleep_gt'] = 1
windows.loc[(windows.minuteweek.between(8637,9048)), 'sleep_gt'] = 1
windows.loc[(windows.minuteweek.between(10020,10077)), 'sleep_gt'] = 1

### filter out all wakings
### unlikely sleep with  HR > 80 (75??) or steps > 0

windows.loc[windows.steps > 0, 'sleep_gt'] = 0
windows.loc[windows.mean_hr > 80, 'sleep_gt'] = 0

### quick check to see we are not violating feedback already

g = sb.FacetGrid(windows.loc[windows.userid  == 7].fillna(2).reset_index(), hue='sleep_gt',size=5, aspect=1.5)
g.map(plt.scatter, 'tstamp','mean_hr').add_legend()
g.ax.set(xlabel='Date',
         ylabel='Value',
         title='mean HR')
g.fig.autofmt_xdate()

plt.scatter(windows.loc[(windows.userid  == 24) & (windows.sleepwalk)].tstamp.values, windows.loc[(windows.userid  == 24) & (windows.sleepwalk), 'mean_hr'].values, color='black', marker='+')

plt.scatter(windows.loc[(windows.userid  == 24) & (windows.wrongsleep)].tstamp.values, windows.loc[(windows.userid  == 24) & (windows.wrongsleep), 'mean_hr'].values, color='red', marker='+')


# ## lags

# from pandas.plotting import autocorrelation_plot
# from pandas.plotting import lag_plot
# from statsmodels.graphics.tsaplots import plot_acf

# lag_plot((medhr - medhr_filt).dropna())
# autocorrelation_plot((medhr - medhr_filt).dropna())
# plot_acf((medhr - medhr_filt).dropna())

# for user in windows.userid.unique():
#     print(user)
#     a = windows.loc[windows.userid == user]
#     a['tlag'] = a.index + 180
#     a = a[['tlag','mean_hr']].set_index('tlag')

#     windows.loc[windows.userid == user, 'lag1'] = a['mean_hr']










