from data_loading_pg import *

con = create_engine('postgresql:///felix')
# Adjust TZ and create TZ features

windows = pd.read_sql('zinsightcalc2', con);
windows.start_time = pd.to_datetime(windows.start_time,unit='s') + APPLE_DELTA

windows.columns = ['zuser', 'recorded_timestamp', 'rmssd_milliseconds', 'mean_hr', 'sdnn', 'br_v04', 'vlf_a', 'lf_a', 'hf_a', 'tot_a', 'accel_mean_3d_magnitude', 'hrq_mean_hr', 'dist_3h_sec', 'exercising_or_not', 'awake_sleep']

w = []
for user in windows.zuser.unique():
    wuser = windows.loc[windows.zuser == user].copy()
    localize_time(wuser, user, con)
    w.append(wuser)

windows = pd.concat(w).reset_index()
    
windows.columns = ['zwhen', 'zuser', 'rmssd_milliseconds', 'mean_hr', 'sdnn', 'br_v04', 'vlf_a', 'lf_a', 'hf_a', 'tot_a', 'accel_mean_3d_magnitude', 'hrq_mean_hr', 'dist_3h_sec', 'exercising_or_not', 'awake_sleep']

###
# use the old classifier to train the new

windows.loc['sleep_old' ] = 3
windows.loc[(windows.awake_sleep == 2) , 'sleep_old' ] = 0
windows.loc[(windows.awake_sleep.isin([0,1,3])) , 'sleep_old' ] = 1




#### 

windows['sleep_gt'] = 3

# ALEX - TZ adjusted
windows.loc[(windows.zuser==7), 'sleep_gt'] = 3 
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-11-24 11:00') & (windows.zwhen <= '2016-11-24 23:20'), 'sleep_gt'] = 0
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-11-24 23:21') & (windows.zwhen <= '2016-11-25 07:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-11-25 09:00') & (windows.zwhen <= '2016-11-25 23:12'), 'sleep_gt'] = 0
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-11-25 23:15') & (windows.zwhen <= '2016-11-26 07:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-11-26 07:00') & (windows.zwhen <= '2016-11-26 22:49'), 'sleep_gt'] = 0
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-11-26 22:50') & (windows.zwhen <= '2016-11-27 06:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-11-28 00:00') & (windows.zwhen <= '2016-11-28 06:40'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-11-28 23:10') & (windows.zwhen <= '2016-11-29 04:30'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-11-29 23:10') & (windows.zwhen <= '2016-11-30 06:30'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-11-30 23:10') & (windows.zwhen <= '2016-12-01 06:25'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-12-01 23:10') & (windows.zwhen <= '2016-12-02 06:10'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-12-02 22:38') & (windows.zwhen <= '2016-12-03 06:10'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-12-03 22:30') & (windows.zwhen <= '2016-12-04 06:30'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-12-04 23:25') & (windows.zwhen <= '2016-12-05 06:50'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-12-05 23:50') & (windows.zwhen <= '2016-12-06 05:30'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-12-06 23:20') & (windows.zwhen <= '2016-12-07 07:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-12-07 23:40') & (windows.zwhen <= '2016-12-08 08:45'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-12-09 00:40') & (windows.zwhen <= '2016-12-09 07:40'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-12-09 23:20') & (windows.zwhen <= '2016-12-10 07:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-12-10 23:22') & (windows.zwhen <= '2016-12-11 07:45'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-12-11 23:30') & (windows.zwhen <= '2016-12-12 00:00'), 'sleep_gt'] = 1

windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-12-16 22:00') & (windows.zwhen <= '2016-12-17 06:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-12-17 23:30') & (windows.zwhen <= '2016-12-18 07:06'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-12-18 23:54') & (windows.zwhen <= '2016-12-19 07:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-12-19 22:45') & (windows.zwhen <= '2016-12-20 05:48'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-12-19 22:45') & (windows.zwhen <= '2016-12-20 05:48'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-12-20 23:00') & (windows.zwhen <= '2016-12-21 06:15'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==7) & (windows.zwhen >= '2016-12-21 23:00') & (windows.zwhen <= '2016-12-22 05:30'), 'sleep_gt'] = 1

windows.loc[(windows.zuser==7) & (windows.sleep_gt == 3), 'sleep_gt' ] = 0  # label rest as WAKE 1
windows.loc[(windows.zuser==7) , 'sleep_old' ] = 3
windows.loc[(windows.zuser==7) & (windows.awake_sleep == 2) , 'sleep_old' ] = 0
windows.loc[(windows.zuser==7) & (windows.awake_sleep.isin([0,1,3])) , 'sleep_old' ] = 1





# TRAVIS - TZ adjusted
windows.loc[(windows.zuser==2), 'sleep_gt'] = 0
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-06-08 21:54') & (windows.zwhen <= '2016-06-09 04:50'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-06-09 22:00') & (windows.zwhen <= '2016-06-10 05:30'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-07-04 23:10') & (windows.zwhen <= '2016-07-05 06:10'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-07-09 21:50') & (windows.zwhen <= '2016-07-10 04:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-07-10 23:22') & (windows.zwhen <= '2016-07-11 06:30'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-07-11 23:00') & (windows.zwhen <= '2016-07-12 06:30'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-07-12 21:30') & (windows.zwhen <= '2016-07-13 06:30'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-07-13 22:40') & (windows.zwhen <= '2016-07-14 05:40'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-07-14 23:40') & (windows.zwhen <= '2016-07-15 06:10'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-07-16 22:20') & (windows.zwhen <= '2016-07-17 07:15'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-07-17 22:00') & (windows.zwhen <= '2016-07-18 06:00'), 'sleep_gt'] = 1

windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-09-21 23:20') & (windows.zwhen <= '2016-09-22 07:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-09-22 22:37') & (windows.zwhen <= '2016-09-23 06:10'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-09-25 23:20') & (windows.zwhen <= '2016-09-26 05:10'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-09-27 23:30') & (windows.zwhen <= '2016-09-28 06:15'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-09-28 23:30') & (windows.zwhen <= '2016-09-29 06:15'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-09-29 22:09') & (windows.zwhen <= '2016-09-30 06:10'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-09-30 22:15') & (windows.zwhen <= '2016-10-01 06:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-10-01 21:30') & (windows.zwhen <= '2016-10-02 07:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-10-02 22:20') & (windows.zwhen <= '2016-10-03 05:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-10-05 00:15') & (windows.zwhen <= '2016-10-05 06:15'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-10-06 00:00') & (windows.zwhen <= '2016-10-06 05:50'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-10-07 00:10') & (windows.zwhen <= '2016-10-07 05:50'), 'sleep_gt'] = 1

windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-10-08 22:15') & (windows.zwhen <= '2016-10-09 07:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-10-10 00:00') & (windows.zwhen <= '2016-10-10 06:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-10-11 00:35') & (windows.zwhen <= '2016-10-11 07:40'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-10-11 23:47') & (windows.zwhen <= '2016-10-12 08:30'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-10-16 22:18') & (windows.zwhen <= '2016-10-17 06:45'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-10-17 22:00') & (windows.zwhen <= '2016-10-18 05:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-10-19 01:00') & (windows.zwhen <= '2016-10-19 07:00'), 'sleep_gt'] = 1

windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-10-30 21:42') & (windows.zwhen <= '2016-10-31 06:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-11-01 00:33') & (windows.zwhen <= '2016-11-01 06:59'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-11-02 00:00') & (windows.zwhen <= '2016-11-02 06:59'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-11-03 00:50') & (windows.zwhen <= '2016-11-03 07:09'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-11-04 00:00') & (windows.zwhen <= '2016-11-04 07:10'), 'sleep_gt'] = 1

windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-11-07 01:39') & (windows.zwhen <= '2016-11-07 06:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-11-08 00:27') & (windows.zwhen <= '2016-11-08 07:12'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-11-09 00:15') & (windows.zwhen <= '2016-11-09 07:21'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-11-10 02:15') & (windows.zwhen <= '2016-11-10 08:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-11-11 00:00') & (windows.zwhen <= '2016-11-11 07:33'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-11-19 00:18') & (windows.zwhen <= '2016-11-19 07:00'), 'sleep_gt'] = 1

windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-11-26 01:39') & (windows.zwhen <= '2016-11-26 10:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-11-27 00:48') & (windows.zwhen <= '2016-11-27 07:30'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-11-27 22:48') & (windows.zwhen <= '2016-11-28 06:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-11-29 00:00') & (windows.zwhen <= '2016-11-29 07:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-11-29 23:00') & (windows.zwhen <= '2016-11-30 07:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-12-02 00:30') & (windows.zwhen <= '2016-12-02 09:15'), 'sleep_gt'] = 1



windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-12-04 23:54') & (windows.zwhen <= '2016-12-05 06:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-12-06 23:51') & (windows.zwhen <= '2016-12-07 07:42'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-12-07 23:00') & (windows.zwhen <= '2016-12-08 07:18'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-12-08 23:51') & (windows.zwhen <= '2016-12-09 06:59'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-12-10 00:00') & (windows.zwhen <= '2016-12-10 08:12'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-12-11 00:18') & (windows.zwhen <= '2016-12-11 08:15'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-12-11 23:35') & (windows.zwhen <= '2016-12-12 06:45'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-12-13 01:30') & (windows.zwhen <= '2016-12-13 07:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==2) & (windows.zwhen >= '2016-12-13 22:55') & (windows.zwhen <= '2016-12-14 00:10'), 'sleep_gt'] = 1

windows.loc[(windows.zuser==2) & (windows.sleep_gt == 3), 'sleep_gt' ] = 0  # label rest as WAKE 



# MT -  TZ adjusted
windows.loc[(windows.zuser==9), 'sleep_gt'] = 0

# James - TZ adjusted
windows.loc[(windows.zuser==1), 'sleep_gt'] = 3
## NO HRQ - LEFT OUT for now
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-05-24 00:00') & (windows.zwhen <= '2016-05-24 07:00'), 'sleep_gt'] = 1
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-05-24 07:00') & (windows.zwhen <= '2016-05-24 17:00'), 'sleep_gt'] = 0
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-05-25 00:00') & (windows.zwhen <= '2016-05-28 00:00'), 'sleep_gt'] = 0
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-03 00:00') & (windows.zwhen <= '2016-06-04 00:00'), 'sleep_gt'] = 0
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-04 00:00') & (windows.zwhen <= '2016-06-05 01:05'), 'sleep_gt'] = 0
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-05 01:06') & (windows.zwhen <= '2016-06-05 08:40'), 'sleep_gt'] = 1
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-05 08:41') & (windows.zwhen <= '2016-06-06 00:25'), 'sleep_gt'] = 0
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-06 00:26') & (windows.zwhen <= '2016-06-06 07:25'), 'sleep_gt'] = 1
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-06 08:20') & (windows.zwhen <= '2016-06-07 00:00'), 'sleep_gt'] = 0
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-07 00:00') & (windows.zwhen <= '2016-06-08 00:00'), 'sleep_gt'] = 0
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-08 00:01') & (windows.zwhen <= '2016-06-08 08:00'), 'sleep_gt'] = 1
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-08 09:30') & (windows.zwhen <= '2016-06-08 23:20'), 'sleep_gt'] = 0
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-08 23:20') & (windows.zwhen <= '2016-06-09 05:00'), 'sleep_gt'] = 1
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-09 07:00') & (windows.zwhen <= '2016-06-09 22:00'), 'sleep_gt'] = 0
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-10 07:00') & (windows.zwhen <= '2016-06-11 00:00'), 'sleep_gt'] = 0
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-13 00:00') & (windows.zwhen <= '2016-06-13 23:35'), 'sleep_gt'] = 0
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-13 23:36') & (windows.zwhen <= '2016-06-14 07:00'), 'sleep_gt'] = 1
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-14 11:00') & (windows.zwhen <= '2016-06-14 22:00'), 'sleep_gt'] = 0
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-14 11:00') & (windows.zwhen <= '2016-06-14 22:59'), 'sleep_gt'] = 0
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-14 23:00') & (windows.zwhen <= '2016-06-15 06:00'), 'sleep_gt'] = 1
# windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-15 10:00') & (windows.zwhen <= '2016-06-15 22:24'), 'sleep_gt'] = 0

windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-15 22:25') & (windows.zwhen <= '2016-06-16 05:50'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-16 05:55') & (windows.zwhen <= '2016-06-17 00:10'), 'sleep_gt'] = 0
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-17 00:10') & (windows.zwhen <= '2016-06-17 06:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-17 06:00') & (windows.zwhen <= '2016-06-18 04:36'), 'sleep_gt'] = 0
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-18 04:37') & (windows.zwhen <= '2016-06-18 08:45'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-18 08:48') & (windows.zwhen <= '2016-06-18 21:59'), 'sleep_gt'] = 0
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-18 22:00') & (windows.zwhen <= '2016-06-19 06:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-19 06:01') & (windows.zwhen <= '2016-06-19 21:59'), 'sleep_gt'] = 0
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-19 22:00') & (windows.zwhen <= '2016-06-20 05:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-20 08:00') & (windows.zwhen <= '2016-06-20 23:59'), 'sleep_gt'] = 0
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-21 00:00') & (windows.zwhen <= '2016-06-21 07:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-21 07:01') & (windows.zwhen <= '2016-06-21 23:44'), 'sleep_gt'] = 0
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-21 23:45') & (windows.zwhen <= '2016-06-22 07:45'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-22 07:46') & (windows.zwhen <= '2016-06-22 23:08'), 'sleep_gt'] = 0
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-22 23:09') & (windows.zwhen <= '2016-06-23 02:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-23 10:00') & (windows.zwhen <= '2016-06-24 02:10'), 'sleep_gt'] = 0
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-24 02:11') & (windows.zwhen <= '2016-06-24 07:22'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-24 07:22') & (windows.zwhen <= '2016-06-24 15:30'), 'sleep_gt'] = 0
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-27 07:00') & (windows.zwhen <= '2016-06-28 02:03'), 'sleep_gt'] = 0
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-28 02:06') & (windows.zwhen <= '2016-06-28 07:45'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-28 08:30') & (windows.zwhen <= '2016-06-28 22:45'), 'sleep_gt'] = 0
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-28 22:46') & (windows.zwhen <= '2016-06-29 06:45'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-29 06:46') & (windows.zwhen <= '2016-06-29 23:00'), 'sleep_gt'] = 0
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-07-04 12:35') & (windows.zwhen <= '2016-07-04 23:00'), 'sleep_gt'] = 0
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-07-04 12:35') & (windows.zwhen <= '2016-07-04 23:00'), 'sleep_gt'] = 0
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-07-04 23:01') & (windows.zwhen <= '2016-07-05 07:00'), 'sleep_gt'] = 1
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-07-05 11:00') & (windows.zwhen <= '2016-07-05 23:00'), 'sleep_gt'] = 0
windows.loc[(windows.zuser==1) & (windows.zwhen >= '2016-06-30 22:24') & (windows.zwhen <= '2016-07-01 06:00'), 'sleep_gt'] = 1


windows.loc[(windows.zuser==1) & (windows.sleep_gt == 3), 'sleep_gt' ] = 0  # label rest as WAKE 













