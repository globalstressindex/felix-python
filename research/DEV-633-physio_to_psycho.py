## investigating relations of the physio and psycho scores

import pandas as pd
from sqlalchemy import create_engine
import seaborn as sb

APPLE_DELTA = pd.datetime(year=2001, month=1, day=1, hour=0, second=0) - pd.to_datetime(0,unit='s')

con = create_engine('postgresql:///felix')

mood = pd.read_sql('zmoodactivitysamplev3',con)
mood633 = pd.read_sql('mood633',con) # FROM R - Mrsort and utadis sorting based on Travis's input
windows = pd.read_sql('zinsightcalc3',con)


windows.set_index('start_time',inplace=True)

mood['zwhen'] = pd.to_datetime(mood['zwhen'], unit='s') + APPLE_DELTA

### 

mood633['addscore'] = mood633[['anxiety', 'concentration', 'focus', 'irritation', 'Overwhelmed','Underpressure', 'Stressed', 'tension']].sum(axis=1) 

T_BACK=20*60   ### tried 5 and 20. No qualitative difference

mood633['mean_hr'] = mood633.apply(lambda x: windows.loc[(windows.userid == x.zuser) & (windows.index >= x.zwhen - T_BACK) & (windows.index <= x.zwhen), 'mean_hr'].mean(),axis=1)

mood633['rmssd_milliseconds'] = mood633.apply(lambda x: windows.loc[(windows.userid == x.zuser) & (windows.index >= x.zwhen - T_BACK) & (windows.index <= x.zwhen), 'rmssd_milliseconds'].mean(),axis=1)

mood633['acute_score'] = mood633.apply(lambda x: windows.loc[(windows.userid == x.zuser) & (windows.index >= x.zwhen - T_BACK) & (windows.index <= x.zwhen), 'acute_score'].mean(),axis=1)


def plot_cats(data, ranker):
    sb.violinplot(x=ranker,y='acute_score',data=data, order=['VL','L','M','H'], inner='point')
    plt.figure();
    sb.violinplot(x=ranker,y='rmssd_milliseconds',data=data, order=['VL','L','M','H'], inner='point')
    plt.figure();
    sb.violinplot(x=ranker,y='mean_hr',data=data, order=['VL','L','M','H'], inner='point')





sb.violinplot(x='assignments',y='acute_score',data=mood633, order=['VL','L','L/M','M','M/H','H'], inner='point')
plt.figure()
sb.violinplot(x='assignments',y='mean_hr',data=mood633, order=['VL','L','L/M','M','M/H','H'], inner='point')
plt.figure()
sb.violinplot(x='assignments',y='rmssd_milliseconds',data=mood633, order=['VL','L','L/M','M','M/H','H'], inner='point')




sb.violinplot(x='MR',y='acute_score',data=mood633, order=['VL','L','M','H'], inner='point')
plt.figure();
sb.violinplot(x='MR',y='rmssd_milliseconds',data=mood633, order=['VL','L','M','H'], inner='point')
plt.figure();
sb.violinplot(x='MR',y='mean_hr',data=mood633, order=['VL','L','M','H'], inner='point')

sb.lmplot(x='mean_hr',y='rmssd_milliseconds',data=mood633, hue='MR', fit_reg=False)


sb.boxplot(x='UTA',y='acute_score',data=mood633, order=['VL','L','M','H'])
plt.figure();
sb.boxplot(x='UTA',y='rmssd_milliseconds',data=mood633, order=['VL','L','M','H'])
plt.figure();
sb.boxplot(x='UTA',y='mean_hr',data=mood633, order=['VL','L','M','H'])


sb.lmplot(x='addscore',y='acute_score',data=mood633)
plt.figure();
sb.lmplot(x='addscore',y='rmssd_milliseconds',data=mood633)
plt.figure();
sb.lmplot(x='addscore',y='mean_hr',data=mood633)


sb.countplot(x='MR', data=mood633, order=['VL','L','M','H'])
plt.figure();
sb.countplot(x='UTA', data=mood633, order=['VL','L','M','H'])
plt.figure();
sb.distplot(mood633.addscore, bins=range(24))


sb.kdeplot(mood633.loc[mood633.MR=='H'].dropna(subset=['mean_hr','rmssd_milliseconds']).mean_hr, mood633.loc[mood633.MR=='H'].dropna(subset=['mean_hr','rmssd_milliseconds']).rmssd_milliseconds, cmap='Reds',shade=True,shade_lowest=False)

sb.kdeplot(mood633.loc[mood633.MR=='M'].dropna(subset=['mean_hr','rmssd_milliseconds']).mean_hr, mood633.loc[mood633.MR=='M'].dropna(subset=['mean_hr','rmssd_milliseconds']).rmssd_milliseconds, cmap='Greens',shade=True,shade_lowest=False, alpha=0.5)


sb.kdeplot(mood633.loc[mood633.MR=='VL'].dropna(subset=['mean_hr','rmssd_milliseconds']).mean_hr, mood633.loc[mood633.MR=='VL'].dropna(subset=['mean_hr','rmssd_milliseconds']).rmssd_milliseconds, cmap='Blues',shade=True,shade_lowest=False, alpha=0.5)
