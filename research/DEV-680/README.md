### NOTE TO MAC OS CONDA USERS

Conda users should install the `python.app` helper into their environment
by activating the environment and entering

    conda install python.app

_Do not try and install this with `pip` like a Python dependency._

To run graphical programs using `matplotlib` you should use the `pythonw`
command to ensure the neceesary interaction support is available.

Information from [matplotlib documentation](https://matplotlib.org/faq/osx_framework.html#long-version).

## PURPOSE

This directory supports scripts used to train V6 accel-based sleep classifier,
and a visual data labelling tool for that data (`lab-plotting.py`). You
can run this in demo mode on data from the included `DEV-680-windows_sleep-accel_2410.csv`
file 

## CLASSIFIER TRAINING

The training scripts are: 

 - `create_dataset.py` - Creates training dataset (in `DEV-680-windows_sleep-accel_2410.csv`) from raw samples
 - `labelling.py` - Adds manually created sleep-wake labels
 - `lag_features.py` - Creates lag1, lag2, lag3 features by joining dataset with its copy that has date column shifted by 1,2,3 periods
 - `eval.py`  - Partitions data into training and testing subsets (by userdays) and compares accel-based against accel-fallback(one period) and minuteweek-based classifiers
 - `hyperpar-eval.py`  - Used to tune hyperparameters. Plots outcomes of various classifiers with different hyperparameter values
 - `slags.csv` and `slone.csv` - Contain results from CPP implementation of the new classifiers for comparision with python
 - `cpp-valid.py` - Performs this comparison
