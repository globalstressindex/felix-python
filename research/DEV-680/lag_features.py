import pandas as pd
from sqlalchemy import create_engine

APPLE_DELTA = pd.datetime(year=2001, month=1, day=1, hour=0, second=0) - pd.to_datetime(0,unit='s')

con = create_engine('postgresql:///felix')

## load windows and accel and enhance windows with some alternative variance measures

# windows = pd.read_sql('windows_sleep-accel_2410',con)

windows = pd.read_csv('DEV-680-windows_sleep-accel_2410.csv')
# windows.set_index(['userid','start_time'],inplace=True)
# windows.sort_index(inplace=True)

windows.dropna(subset=['mean_hr','ac_mad'], inplace=True)
windows = windows.sort_values(by=['userid','start_time'])
wdist = windows.set_index(['userid','start_time'])['dist_3h_sec']
wweek = windows.set_index(['userid','start_time'])['minute_of_week']

windows = windows[['userid','start_time','mean_hr','ac_mad','sleep_gt','rmssd_milliseconds']]


w2 = windows.copy()
w2['start_time'] = w2['start_time'] + 180
w2.columns = [colname+'_prev1' if colname not in ['userid','start_time'] else colname for colname in w2.columns]

windows = pd.merge(windows,w2,on=['userid','start_time'],how='left').sort_values(by=['userid','start_time'])

w2['start_time'] = w2['start_time'] + 180   ## increment again to get lag=2
w2.columns = [colname[:-1]+'2' if colname not in ['userid','start_time'] else colname for colname in w2.columns]

windows = pd.merge(windows,w2,on=['userid','start_time'],how='left').sort_values(by=['userid','start_time'])

w2['start_time'] = w2['start_time'] + 180   ## increment again to get lag=3
w2.columns = [colname[:-1]+'3' if colname not in ['userid','start_time'] else colname for colname in w2.columns]

windows = pd.merge(windows,w2,on=['userid','start_time'],how='left').sort_values(by=['userid','start_time'])

w2['start_time'] = w2['start_time'] + 180   ## increment again to get lag=4
w2.columns = [colname[:-1]+'4' if colname not in ['userid','start_time'] else colname for colname in w2.columns]

windows = pd.merge(windows,w2,on=['userid','start_time'],how='left').sort_values(by=['userid','start_time'])

w2['start_time'] = w2['start_time'] + 180   ## increment again to get lag=5
w2.columns = [colname[:-1]+'5' if colname not in ['userid','start_time'] else colname for colname in w2.columns]

windows = pd.merge(windows,w2,on=['userid','start_time'],how='left').sort_values(by=['userid','start_time'])

windows.set_index(['userid','start_time'],inplace=True)

windows['dist_3h_sec'] = wdist
windows['minute_of_week'] = wweek

windows.reset_index(inplace=True)

# for col in windows.columns:
#     if (col != 'start_time') & ('tstamp' not in col):
#         if col.endswith('prev'):
#             colnext = col[:-5]+'_cur'  ## current value
#             windows.loc[isnan(windows[col]),col] = windows.loc[isnan(windows[col]),colnext]


# for col in windows.columns:
#     if (col != 'start_time') & ('tstamp' not in col):
#         if col.endswith('prev2'):
#             colnext = col[:-1]  ## lag1 value
#             windows.loc[isnan(windows[col]),col] = windows.loc[isnan(windows[col]),colnext]

# ## drop unused columns
# windows.drop(['vlf_a_cur', 'lf_a_cur', 'hf_a_cur',
#        'tot_a_cur', 'window_size_cur',
#  'vlf_a_prev', 'lf_a_prev', 'hf_a_prev', 'tot_a_prev',
#        'exercising_or_not_prev', 'awake_sleep_prev', 'acute_score_prev',
#        'state_prev', 
#        'rr_num_samples_prev', 'accel_num_samples_prev',
#        'hrq_num_samples_prev', 'window_size_prev', 
# 'vlf_a_prev2', 'lf_a_prev2', 'hf_a_prev2',
#        'tot_a_prev2',  'exercising_or_not_prev2',
#        'awake_sleep_prev2', 'acute_score_prev2', 'state_prev2',
#        'rr_num_samples_prev2', 'accel_num_samples_prev2',
#        'hrq_num_samples_prev2', 'window_size_prev2'], axis=1, inplace=True)
            
# windows = windows.dropna(subset=['mean_hr_cur','ac_std_cur', 'ac_mean_cur', 'ac_iqr_cur', 'ac_mad_cur', 'hrquant_cur'])

windows['tstamp'] = pd.to_datetime(windows.start_time,unit='s') + APPLE_DELTA + pd.to_timedelta(1,'h')

windows['day'] = windows['tstamp'].dt.dayofyear

# windows['day_second'] = windows.tstamp.dt.hour * 3600 + windows.tstamp.dt.minute * 60 + windows.tstamp.dt.second
# windows.loc[isnan(windows.dist_3h_sec),'dist_3h_sec'] = windows['day_second'].apply(lambda x: min([abs(x - i) for i in [3*3600,27*3600]]))
# windows.drop('day_second',axis=1)
# #windows.to_sql('windows_sleep-accel_2410',con,if_exists='replace')
