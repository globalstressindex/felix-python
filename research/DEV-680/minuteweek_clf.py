import json

from trained_models.trained_utils import make_own_dt_classifier

json_file = '/home/gb/felix/repos/cpp/classifiers/share/sleepClassifierV05.json'

with open(json_file, 'r') as f:
        model_definition = json.load(f)
        model_definition.pop('class_descriptions', None)
        model_definition.pop('n_nodes', None)
        model_definition['feature_strings'] = ['mean_hr','minute_of_week']
        clf_v5 = make_own_dt_classifier(**model_definition)


features = ['mean_hr','minute_of_week']


windows = pd.read_csv("DEV-680-windows_sleep-accel_2410.csv")
windows['sleep_lr_v5'] = clf_v5.predict(windows)


users = [2,  31, 43,   7,  73]
for user in users:
    print(user, accuracy_score(windows.loc[windows.userid == user,'sleep_gt'], windows.loc[windows.userid == user,'sleep_lr_v5']))

    
