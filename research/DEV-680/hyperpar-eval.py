import numpy as np
from matplotlib import pyplot as plt

from sklearn.model_selection import GridSearchCV
from sklearn.metrics import make_scorer
from sklearn.metrics import accuracy_score
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn import preprocessing


#print(__doc__)

###############################################################################
# Running ``GridSearchCV`` using multiple evaluation metrics
# ----------------------------------------------------------
#

#features = ['hrquant','accel_std_3d_magnitude','mean_hr']
# features = ['hrquant', 'ac_mad', 'ac_mad_prev', 'hrquant_prev', 'hrquant_prev2', 'ac_mad_prev2', 'ac_std', 'ac_std_prev', 'ac_std_prev2', 'ac_iqr', 'ac_iqr_prev', 'ac_iqr_prev2','minute_of_week']
#features = ['ac_mad', 'mean_hr', 'dist_3h_sec']
#features = ['mean_hr', 'minute_of_week', 'ac_mad']
# features = ['ac_mad', 'dist_3h_sec', 'mean_hr', 'ac_mad_prev', 'mean_hr_prev', 'ac_mad_prev2',  'mean_hr_prev2']

features = ['dist_3h_sec', 'mean_hr', 'ac_mad', 'rmssd_milliseconds',
            'mean_hr_prev1', 'ac_mad_prev1', 'rmssd_milliseconds_prev1',
            'mean_hr_prev2', 'ac_mad_prev2', 'rmssd_milliseconds_prev2',
            'mean_hr_prev3', 'ac_mad_prev3', 'rmssd_milliseconds_prev3']

# data = windows.loc[((windows.tstamp.dt.hour  <= 1) | (windows.tstamp.dt.hour  >= 22)) | ((windows.tstamp.dt.hour  >= 6) & (windows.tstamp.dt.hour  <= 10))].dropna()
data = windows
X = data.dropna(subset=features).dropna(subset=['sleep_gt'])[features]
y = data.dropna(subset=features)['sleep_gt'].dropna()

# The scorers can be either be one of the predefined metric strings or a scorer
# callable, like the one returned by make_scorer
scoring = {'AUC': 'roc_auc', 'Accuracy': make_scorer(accuracy_score)}

# Setting refit='AUC', refits an estimator on the whole dataset with the
# parameter setting that has the best cross-validated AUC score.
# That estimator is made available at ``gs.best_estimator_`` along with
# parameters like ``gs.best_score_``, ``gs.best_parameters_`` and
# ``gs.best_index_``


# clf = DecisionTreeClassifier(class_weight='balanced')
# param_grid={'max_depth': [1,2,3,4,5,7,8,9,10,11,12,13,20]}

X = preprocessing.robust_scale(X)
clf = LogisticRegression(class_weight='balanced')
#clf = KNeighborsClassifier(weights='uniform')
#clf = LinearSVC(class_weight='balanced')
param_grid={'C': linspace(0.0001,5,20)}
#param_grid={'n_neighbors': [5,10,15,20]}


gs = GridSearchCV(clf,
                  param_grid = param_grid,
                  scoring=scoring, cv=10, refit='AUC')
gs.fit(X, y)
results = gs.cv_results_

###############################################################################
# Plotting the result
# -------------------

plt.figure(figsize=(13, 13))
plt.title("GridSearchCV evaluating using multiple scorers simultaneously",
          fontsize=16)

plt.xlabel(list(param_grid.keys())[0])
plt.ylabel("Score")
plt.grid()

ax = plt.axes()
ax.set_xlim(0, max(list(param_grid.values())[0]))
ax.set_ylim(0.8, 1)

# Get the regular numpy array from the MaskedArray
# X_axis = np.array(results['param_max_depth'].data, dtype=float)
X_axis = np.array(results['param_'+list(param_grid.keys())[0]].data, dtype=float)

for scorer, color in zip(sorted(scoring), ['g', 'k']):
    for sample, style in (('train', '--'), ('test', '-')):
        sample_score_mean = results['mean_%s_%s' % (sample, scorer)]
        sample_score_std = results['std_%s_%s' % (sample, scorer)]
        ax.fill_between(X_axis, sample_score_mean - sample_score_std,
                        sample_score_mean + sample_score_std,
                        alpha=0.1 if sample == 'test' else 0, color=color)
        ax.plot(X_axis, sample_score_mean, style, color=color,
                alpha=1 if sample == 'test' else 0.7,
                label="%s (%s)" % (scorer, sample))

    best_index = np.nonzero(results['rank_test_%s' % scorer] == 1)[0][0]
    best_score = results['mean_test_%s' % scorer][best_index]

    # Plot a dotted vertical line at the best score for that scorer marked by x
    ax.plot([X_axis[best_index], ] * 2, [0, best_score],
            linestyle='-.', color=color, marker='x', markeredgewidth=3, ms=8)

    # Annotate the best score for that scorer
    ax.annotate("%0.2f" % best_score,
                (X_axis[best_index], best_score + 0.005))

plt.legend(loc="best")
plt.grid('off')
plt.show()
