import pandas as pd
from sqlalchemy import create_engine

APPLE_DELTA = pd.datetime(year=2001, month=1, day=1, hour=0, second=0) - pd.to_datetime(0,unit='s')

con = create_engine('postgresql:///felix')

## load windows and accel and enhance windows with some alternative variance measures

#windows = pd.read_sql('windows_sleep-accel_2410',con)
windows = pd.read_csv('DEV-680-windows_sleep-accel_2410.csv')

windows.set_index('start_time',inplace=True)


### more detailed labelling - small dataset
### 43 - Mike? 

windows.loc[(windows.userid==43) , 'sleep_gt'] = 0

windows.loc[(windows.userid==43) & (windows.index > 528245640 ) & (windows.index <= 528275160 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index > 528331680 ) & (windows.index <= 528361920 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index > 528362820 ) & (windows.index <= 528365520 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index >= 528674760 ) & (windows.index < 528678180 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index > 528679080 ) & (windows.index <= 528710760 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index > 528761520 ) & (windows.index <= 528763140 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index > 528763680 ) & (windows.index <= 528795000 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index >= 528852060 ) & (windows.index <= 528881220  ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index > 529026840 ) & (windows.index < 529065000  ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index > 529111620 ) & (windows.index < 529135000  ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index >= 529193340 ) & (windows.index < 529225380  ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index >= 529279920 ) & (windows.index < 529281180  ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index >= 529281720 ) & (windows.index < 529314480  ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index >= 529374240 ) & (windows.index < 529398000  ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index >= 529453260 ) & (windows.index < 529490880  ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index >= 529539480 ) & (windows.index < 529540020  ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index == 529540560 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index >= 529541280 ) & (windows.index < 529571160  ), 'sleep_gt'] = 1
### alcohol night - atypical!
windows.loc[(windows.userid==43) & (windows.index >= 529635600 ) & (windows.index < 529670340  ), 'sleep_gt'] = 1
## 
windows.loc[(windows.userid==43) & (windows.index >= 529796700 ) & (windows.index < 529802820  ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index >= 529883280 ) & (windows.index < 529920000  ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index >= 529973100 ) & (windows.index < 529974000  ), 'sleep_gt'] = 1
windows.loc[(windows.userid==43) & (windows.index >= 529885440 ) & (windows.index < 529886520  ), 'sleep_gt'] = 0
# windows.loc[(windows.userid==43) & (windows.index.isin([529653420,529657740,529661880,529662060]) ) , 'sleep_gt'] = 0
#windows.loc[windows.mean_hr >75, 'sleep_gt'] = 0

#### TRAVIS

windows.loc[(windows.userid==2) , 'sleep_gt'] = 0


windows.loc[(windows.userid==2) & (windows.index >= 528770880 ) & (windows.index < 528788880  ), 'sleep_gt'] = 1
windows.loc[(windows.userid==2) & (windows.index >= 528852420 ) & (windows.index < 528882300  ), 'sleep_gt'] = 1
windows.loc[(windows.userid==2) & (windows.index >= 528940080 ) & (windows.index < 528969240 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==2) & (windows.index >= 529030620 ) & (windows.index < 529062300 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==2) & (windows.index >= 529121340 ) & (windows.index < 529151760 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==2) & (windows.index >= 529192980 ) & (windows.index < 529221960 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==2) & (windows.index >= 529282800 ) & (windows.index < 529312500 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==2) & (windows.index >= 529313220 ) & (windows.index < 529318440 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==2) & (windows.index >= 529367040 ) & (windows.index < 529390800 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==2) & (windows.index >= 529382880 ) & (windows.index <= 529383060 ), 'sleep_gt'] = 0
windows.loc[(windows.userid==2) & (windows.index >= 529454700 ) & (windows.index < 529493400 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==2) & (windows.index >= 529541280 ) & (windows.index < 529560540 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==2) & (windows.index >= 529635240 ) & (windows.index < 529664400 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==2) & (windows.index >= 529648200 ) & (windows.index < 529650180 ), 'sleep_gt'] = 0
windows.loc[(windows.userid==2) & (windows.index >= 529717140 ) & (windows.index < 529749000 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==2) & (windows.index >= 529803360 ) & (windows.index < 529825860 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==2) & (windows.index == 529817760 ) , 'sleep_gt'] = 0
windows.loc[(windows.userid==2) & (windows.index >= 529889940 ) & (windows.index < 529921980 ), 'sleep_gt'] = 1
#windows.loc[windows.mean_hr >75, 'sleep_gt'] = 0

### USER 31

windows.loc[(windows.userid==31), 'sleep_gt'] = 0

windows.loc[(windows.userid==31) & (windows.index >= 527471820 ) & (windows.index < 527499360 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==31) & (windows.index >= 527522400 ) & (windows.index < 527527980 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==31) & (windows.index >= 527724720 ) & (windows.index < 527757480 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==31) & (windows.index >= 528415380 ) & (windows.index < 528448320 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==31) & (windows.index >= 528499980 ) & (windows.index < 528504480 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==31) & (windows.index >= 528851340 ) & (windows.index < 528872450 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==31) & (windows.index >= 528935580 ) & (windows.index < 528964560 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==31) & (windows.index >= 529460100 ) & (windows.index < 529478460 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==31) & (windows.index >= 529478820 ) & (windows.index < 529481520 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==31) & (windows.index >= 529481700 ) & (windows.index < 529482420 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==31) & (windows.index >= 529482600 ) & (windows.index < 529482960 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==31) & (windows.index >= 529552080 ) & (windows.index < 529572960 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==31) & (windows.index >= 529720740 ) & (windows.index < 529745940 ), 'sleep_gt'] = 1

# windows.loc[(windows.userid==31) & (windows.mean_hr>=75), 'sleep_gt'] = 0

### Alex userid=7

windows.loc[(windows.userid==7), 'sleep_gt'] = 0

windows.loc[(windows.userid==7) & (windows.index >= 529887240 ) & (windows.index < 529911180 ), 'sleep_gt'] = 1
windows.loc[(windows.userid==7) & (windows.index >= 529970580 ) & (windows.index < 529974000 ), 'sleep_gt'] = 1


### Calvin userid=73


windows.loc[(windows.userid==73), 'sleep_gt'] = 0

windows.loc[(windows.userid==73) & (windows.index >= 527552640 ) & (windows.index < 527585040), 'sleep_gt'] = 1

windows.loc[(windows.userid==73) & (windows.index >= 527639580 ) & (windows.index < 527668200), 'sleep_gt'] = 1

windows.loc[(windows.userid==73) & (windows.index >= 527722920 ) & (windows.index < 527725080), 'sleep_gt'] = 1


windows.loc[(windows.userid==73) & (windows.index >= 527811120 ) & (windows.index < 527847660), 'sleep_gt'] = 1
### ASK CALVIN ABOUT MORNING OF 23/09

windows.loc[(windows.userid==73) & (windows.index >= 527980680 ) & (windows.index < 528013440), 'sleep_gt'] = 1

windows.loc[(windows.userid==73) & (windows.index >= 528241680 ) & (windows.index <= 528255540), 'sleep_gt'] = 1

windows.loc[(windows.userid==73) & (windows.index >= 528678360 ) & (windows.index < 528705360), 'sleep_gt'] = 1



### filter out all wakings
### unlikely sleep with  HR > 80 (75??) or steps > 0
##  windows.loc[windows.steps > 0, 'sleep_gt'] = 0
# windows.loc[windows.mean_hr >75, 'sleep_gt'] = 0  ## err on the sleeping side


windows.to_sql('windows_sleep-accel_2410',con,if_exists='replace')
