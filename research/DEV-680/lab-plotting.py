"""
Use the following keystrokes to label data and navigate

b - select the current point as the beginning of sleep
e - select the current point as the end of sleep
n - move to the next point
p - move to the previous point
w - save new sleep period to the dataframe and replot
x - save new sleep period to the dataframe and replot

NOTE: due to matplotlib handling of dates, currently the tool only allows to do markup
when X-axis is numeric (i.e. epoch). A workaround is to have two plots: first create one with "tstamp" on X-axis, then another is with "start_time" (change plotindex below and run the script twice). Second one is then used for markup and first one for reference.  Any contributions towards sorting this are especially welcome.

Output data is saved in w1 which is a copy of a slice of the input array (windows) for a particular user.
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sb


class PointBrowser(object):
    """
    Click on a point to select and highlight it -- the data that
    generated the point will be shown in the lower axes.  Use the 'n'
    and 'p' keys to browse through the next and previous points
    """

    def __init__(self):
        self.lastind = 0
        self.sleepstartind = -1
        self.sleependind = -1

        self.text = ax.text(0.05, 0.95, 'selected: none, start: none, end: none',
                            transform=ax.transAxes, va='top')
        self.selected, = ax.plot([xs[0]], [ys[0]], 'o', ms=12, alpha=0.4,
                                 color='yellow', visible=False)

    def onpress(self, event):
        if self.lastind is None:
            return
        if event.key not in ('n', 'p', 'b', 'e', 'w', 'x'):
            return
        if event.key == 'n':
            self.lastind += 1
        elif event.key == 'p':
            self.lastind += -1
        elif event.key == 'b':
            self.sleepstartind = self.lastind
        elif event.key == 'e':
            self.sleependind = self.lastind
        elif event.key == 'w':
            if (self.sleepstartind > 0) & (self.sleependind > 0) & (self.sleepstartind <= self.sleependind):
                print(self.sleepstartind, self.sleependind)
                w1.loc[xs[self.sleepstartind]:xs[self.sleependind],'sleep_gt'] = 1
            else:
                print("Please set bounds correctly")
        elif event.key == 'x':
            if (self.sleepstartind >= 0) & (self.sleependind >= 0) & (self.sleepstartind <= self.sleependind):
                print(self.sleepstartind, self.sleependind)
                w1.loc[xs[self.sleepstartind]:xs[self.sleependind],'sleep_gt'] = 0
            else:
                print("Please set bounds correctly")


        self.lastind = np.clip(self.lastind, 0, len(xs) - 1)
        self.update()

    def onpick(self, event):

        if event.artist != line:
            return True

        N = len(event.ind)
        if not N:
            return True

        # the click locations
        x = event.mouseevent.xdata
        y = event.mouseevent.ydata

        distances = np.hypot(x - xs[event.ind], y - ys[event.ind])
        indmin = distances.argmin()
        dataind = event.ind[indmin]

        self.lastind = dataind
        self.update()

    def update(self):
        if self.lastind is None:
            return

        dataind = self.lastind
        start = self.sleepstartind
        end = self.sleependind

        # ax2.cla()
        # ax2.plot(X[dataind])

        # ax2.text(0.05, 0.9, 'mu=%1.3f\nsigma=%1.3f' % (xs[dataind], ys[dataind]),
        #          transform=ax2.transAxes, va='top')
        # ax2.set_ylim(-0.5, 1.5)
        self.selected.set_visible(True)
        self.selected.set_data(xs[dataind], ys[dataind])

        self.text.set_text('selected: %d, start: %d, end: %d' % (dataind, start, end))
        w1.loc[w1.sleep_gt == 0, 'colour'] = 'blue'
        w1.loc[w1.sleep_gt == 1, 'colour'] = 'green'
        w1.loc[w1.sleep_gt == 3, 'colour'] = 'red'
        colours = w1['colour'].values
        line.set_color(colours)
        fig.canvas.draw()



windows = pd.read_csv("DEV-680-windows_sleep-accel_2410.csv")

###windows['sleep_gt'] = nan   ## labels - default value

plotindex = 'start_time'          # try tstamp
plotdim = 'mean_hr'
plotuser = 2

w1 = []
w1 = windows.loc[windows.userid==plotuser].reset_index(drop=True).set_index(plotindex).sort_index()

fig, ax = plt.subplots(1, 1)

w1 = w1.fillna(3)

w1.loc[w1.sleep_gt == 0, 'colour'] = 'blue'
w1.loc[w1.sleep_gt == 1, 'colour'] = 'green'
w1.loc[w1.sleep_gt == 3, 'colour'] = 'red'

xs = w1.index
ys = w1[plotdim].values
colours = w1['colour'].values
line = ax.scatter(xs, ys, marker='o', color=colours , picker=5)  # 5 points tolerance


ax.fill_between(w1.index,
                w1[plotdim] - 0.4*w1['ac_mad'],
                w1[plotdim] + 0.4*w1['ac_mad'],
                alpha=0.1,
                color='red')


browser = PointBrowser()

fig.canvas.mpl_connect('pick_event', browser.onpick)
fig.canvas.mpl_connect('key_press_event', browser.onpress)

plt.show()
