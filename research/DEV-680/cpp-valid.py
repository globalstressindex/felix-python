# This script checks python classifiers output vs cpp immplementation 
# clf is the classifier which looks back 3 periods
# clfOne is the fallback classifier whih kicks in if the historical data is not available
# slags.csv and slone.csv are outputs from a cpp model 

import json
from trained_models.trained_utils import make_own_lr_classifier

sleep_clf_center = np.array([23940.               ,     68.785872393315103,
                                     4.9187912506476  ,     57.0200609989312  ,
                                     68.762281861136898,      4.93885001038573 ,
                                     56.829277075202803,     68.778803124466506,
                                     4.95994195518515 ,     56.792018218525499,
                                     68.804830221791903,      4.97508411341235 ,
                                     56.897592858823302]);

sleep_clf_scale = np.array([21420.               ,     18.876931111351794,
                                    15.544218262252425,     42.224685581094803,
                                    18.864421524212908,     15.551222292957284,
                                    42.128465748088992,     18.8870634474968  ,
                                    15.612658808650037,     42.069916592777396,
                                    18.920934482793704,     15.618300299846787,     42.1838082948885]);

sleep_clfOne_center = np.array([23940, 68.985744930271053, 5.04891660294293 , 57.358059758423394]);
sleep_clfOne_scale = np.array([21240, 18.973430898281464, 15.813078806760785, 42.384295916436955]);


json_f = '/home/gb/felix/repos/cpp/classifiers/share/sleepClassifierV06-LAGS.json'
json_fOne = '/home/gb/felix/repos/cpp/classifiers/share/sleepClassifierV06-ONE.json'


def build_classifier(file_name):
    with open(file_name, 'r') as f:
        model_definition = json.load(f)
        model_definition.pop('class_descriptions', None)
        return make_own_lr_classifier(**model_definition)

clf_lags = build_classifier(json_f)
clf_one = build_classifier(json_fOne)

######### 3 period classifier

features = ['dist_3h_sec', 'mean_hr', 'ac_mad', 'rmssd_milliseconds',
            'mean_hr_prev1', 'ac_mad_prev1', 'rmssd_milliseconds_prev1',
            'mean_hr_prev2', 'ac_mad_prev2', 'rmssd_milliseconds_prev2',
            'mean_hr_prev3', 'ac_mad_prev3', 'rmssd_milliseconds_prev3']

X_train = windows.dropna(subset=features).dropna(subset=['sleep_gt'])[features]
y_train = windows.dropna(subset=features)['sleep_gt'].dropna()

rs = preprocessing.RobustScaler().fit(X_train)
rs.center_ = sleep_clf_center
rs.scale_ = sleep_clf_scale
X_train = rs.transform(X_train)

windows['sleep_lr'] = 3
windows['sleep_lrp'] = nan
windows.loc[windows[features].dropna().index, 'sleep_lr'] = clf_lags.predict(rs.transform(windows[features].dropna()))
windows.loc[windows[features].dropna().index, 'sleep_lrp'] = clf_lags.predict_proba(rs.transform(windows[features].dropna()))[:,1]
windows.loc[windows.sleep_lrp < 0.7, 'sleep_lr'] = 0

########## one period classifier

features = ['dist_3h_sec', 'mean_hr', 'ac_mad', 'rmssd_milliseconds']

X_train = windows.dropna(subset=features).dropna(subset=['sleep_gt'])[features]
y_train = windows.dropna(subset=features)['sleep_gt'].dropna()

rs = preprocessing.RobustScaler().fit(X_train)
rs.center_ = sleep_clfOne_center
rs.scale_ = sleep_clfOne_scale
X_train = rs.transform(X_train)

windows['sleep_lr_one'] = 3
windows['sleep_lrp_one'] = nan
windows.loc[windows[features].dropna().index, 'sleep_lr_one'] = clf_lone.predict(rs.transform(windows[features].dropna()))
windows.loc[windows[features].dropna().index, 'sleep_lrp_one'] = clf_lone.predict_proba(rs.transform(windows[features].dropna()))[:,1]
windows.loc[windows.sleep_lrp < 0.7, 'sleep_lr_one'] = 0


### validation

cpp = pd.read_csv('slags.csv')
cpp.set_index('start_time',inplace=True)
cpp.loc[cpp.awake_sleep == -1, 'awake_sleep'] = 3
cpp.loc[cpp.awake_sleep == 0, 'awake_sleep'] = 1
cpp.loc[cpp.awake_sleep == 2, 'awake_sleep'] = 0

cmpclf = windows.loc[windows.userid==2].set_index('start_time')[['mean_hr','sleep_lr','sleep_lr_one']].join(cpp,lsuffix='_lr')

lowsamples = cmpclf.loc[cmpclf.sleep_lr != cmpclf.awake_sleep].sort_index().index
print (cmpclf.loc[cmpclf.index.isin(lowsamples)])  ## should be LR = 3 for all

cppone = pd.read_csv('slone.csv')
cppone.set_index('start_time',inplace=True)
cppone.loc[cppone.awake_sleep == -1, 'awake_sleep'] = 3
cppone.loc[cppone.awake_sleep == 0, 'awake_sleep'] = 1
cppone.loc[cppone.awake_sleep == 2, 'awake_sleep'] = 0

cmpclfone = windows.loc[windows.userid==2].set_index('start_time')[['mean_hr','sleep_lr','sleep_lr_one']].join(cppone,lsuffix='_lr')

print (cmpclfone.loc[cmpclfone.index.isin(lowsamples) & (cmpclfone.sleep_lr_one != cmpclfone.awake_sleep)])
## should be empty! 

