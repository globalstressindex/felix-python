import pandas as pd
from sqlalchemy import create_engine
import scipy
from scipy import stats
from statsmodels import robust

APPLE_DELTA = pd.datetime(year=2001, month=1, day=1, hour=0, second=0) - pd.to_datetime(0,unit='s')

con = create_engine('postgresql:///felix')

## load windows and accel and enhance windows with some alternative variance measures

windows = pd.read_sql('zinsightcalc3',con)

windows.set_index(['userid','start_time'], inplace=True)
accel = pd.read_sql('zgarminaccelerometer',con)
accel['window'] = accel['zwhen'] - accel['zwhen'] % 180
acc_aggreg = accel.groupby(['zuser','window']).zaccel_mean_3d_magnitude.agg([std,mean,stats.iqr,robust.mad])
acc_aggreg.reset_index(inplace=True)
acc_aggreg.columns = ['userid', 'start_time', 'ac_std', 'ac_mean', 'ac_iqr', 'ac_mad']
acc_aggreg.set_index(['userid','start_time'], inplace=True)
windows = windows.join(acc_aggreg)


### remove users without accel data
windows.reset_index(inplace=True)
windows.set_index('start_time',inplace=True)

windows=windows.loc[windows.userid.isin([2,31,43,71,73,80])]  ## internal users with accel and many points
windows.loc[windows.userid == 80, 'userid'] = 73             ## Calvin
windows.loc[windows.userid == 71, 'userid'] = 7             ## Alex

### hr quantile - works better than HR!
windows['hrquant'] = nan
for user in windows.userid.unique():
    data = windows.loc[windows.userid==user, 'mean_hr']
    total = len(data)
    windows.loc[windows.userid == user, 'hrquant'] = windows.loc[windows.userid == user,'mean_hr'].apply(lambda x: sum(data <= x)/total)

### only keep data with sufficient number of samples    
windows = windows.loc[windows.accel_num_samples > 30]  
windows = windows.loc[windows.rr_num_samples > 30]  

windows.to_sql('windows_sleep-accel_2410',con,if_exists='replace')
    
### Next run DEV-680_labelling    
