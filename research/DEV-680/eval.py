from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn import preprocessing
from sklearn.metrics import accuracy_score, confusion_matrix

from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.svm import LinearSVC


# boundaries = windows.loc[((windows.tstamp.dt.hour  <= 1) | (windows.tstamp.dt.hour  >= 21)) | ((windows.tstamp.dt.hour  >= 6) & (windows.tstamp.dt.hour  <= 10))]
boundaries = windows
# #boundaries = windows.loc[windows.sleep_gt != windows.sleep_gt_prev]

features_v5 = ['mean_hr', 'minute_of_week']
clf_v5_new = DecisionTreeClassifier(class_weight='balanced', max_depth=20)  ## same as old experimental

features = ['dist_3h_sec', 'mean_hr', 'ac_mad', 'rmssd_milliseconds',
            'mean_hr_prev1', 'ac_mad_prev1', 'rmssd_milliseconds_prev1',
            'mean_hr_prev2', 'ac_mad_prev2', 'rmssd_milliseconds_prev2',
            'mean_hr_prev3', 'ac_mad_prev3', 'rmssd_milliseconds_prev3']
            
#features = ['dist_3h_sec', 'mean_hr', 'ac_mad', 'rmssd_milliseconds']
# # features = ['ac_mad', 'mean_hr', 'ac_mad_prev', 'mean_hr_prev', 'ac_mad_prev2',  'mean_hr_prev2']
boundaries = boundaries.dropna(subset=[features])
clf = LogisticRegression(class_weight='balanced', penalty='l2', C=0.5)
#clf = KNeighborsClassifier(n_neighbors=10,weights='uniform')
#clf = DecisionTreeClassifier(class_weight='balanced', max_depth=5)

results=[]
cms = []
results_v5=[]
cms_v5 = []
for i in range(30):
    # boundaries = pd.concat([windows.loc[(windows.sleep_gt != windows.sleep_gt_prev) | (windows.sleep_gt != windows.sleep_gt_prev2)], windows.loc[(windows.sleep_gt == windows.sleep_gt_prev) & (windows.sleep_gt == windows.sleep_gt_prev2)].sample(2000)])    
    all_userdays = boundaries[['userid','day']].drop_duplicates()
    test_userdays = all_userdays.sample(20)
    train_userdays = all_userdays.loc[all_userdays.subtract(test_userdays).isnull().any(axis=1)]

    X_train = pd.merge(boundaries, train_userdays)[features].dropna()
    y_train = pd.merge(boundaries, train_userdays).dropna(subset=[features])['sleep_gt']

    X_test = pd.merge(windows, test_userdays)[features].dropna()
    y_test = pd.merge(windows, test_userdays).dropna(subset=[features])['sleep_gt']

    rs = preprocessing.RobustScaler().fit(X_train)
    poly = preprocessing.PolynomialFeatures(2).fit(X_train)
    X_train = rs.transform(X_train)
#    X_train = poly.transform(X_train)

    clf.fit(X_train,y_train)
    X_test = rs.transform(X_test)
#    X_test = poly.transform(X_test)
    y_predict = clf.predict(X_test)
    y_predict_proba = clf.predict_proba(X_test)[:,1]
    y_predict[y_predict_proba < 0.7] = 0

    results.append(accuracy_score(y_test, y_predict))
    cms.append(np.array(confusion_matrix(y_test, y_predict)))
    
    X_train = pd.merge(boundaries, train_userdays)[features_v5].dropna()
    y_train = pd.merge(boundaries, train_userdays).dropna(subset=[features_v5])['sleep_gt']

    X_test = pd.merge(windows, test_userdays)[features_v5].dropna()
    y_test = pd.merge(windows, test_userdays).dropna(subset=[features_v5])['sleep_gt']

    clf_v5_new.fit(X_train,y_train)
    y_predict_v5 = clf_v5_new.predict(X_test)

    results_v5.append(accuracy_score(y_test, y_predict_v5))
    cms_v5.append(np.array(confusion_matrix(y_test, y_predict_v5)))

print (mean(results), std(results))
print (pd.DataFrame(np.mean(cms,axis=0), columns=['pred wake', 'pred sleep'], index=['wake','sleep']))

print (mean(results_v5), std(results_v5))
print (pd.DataFrame(np.mean(cms_v5,axis=0), columns=['pred wake', 'pred sleep'], index=['wake','sleep']))


