from sklearn.model_selection import GridSearchCV, cross_val_score, KFold, StratifiedKFold
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.feature_selection import SelectFromModel
from sklearn.preprocessing import PolynomialFeatures
from sklearn import metrics
from sklearn import tree

## clean the data a bit
windows.dropna(subset=['mean_hr','rmssd_milliseconds','sdnn','hf_a','lf_a','steps','minuteweek'],inplace=True)

def feature_selector():
    # trying various feature combinations
    ## MAKE SURE FEEDBACK LABELS HAVEN'T BEEN MERGED INTO SLEEP_GT!!
    # initially we had 430 differences between feedback and classifier (old LRv04)
    # numbers below are best outcomes for a DT with max_depth \in [5, 10, ..., 50]
    
    # features = ['mean_hr','rmssd_milliseconds','sdnn','hf_a','lf_a','steps','minuteweek']  ## 235
    # features = ['mean_hr','rmssd_milliseconds','sdnn','hf_a','lf_a','steps']  ## 261 <<< WORST
    # features = ['mean_hr','minuteweek'] ## 74 <<<<<<<<<<<<< BEST
    # features = ['mean_hr','rmssd_milliseconds'] ## 257 <<<<<<<<<<<<< WORST
    # features = ['mean_hr','rmssd_milliseconds','minuteweek'] ## 157
    # features = ['mean_hr','steps','minuteweek'] ## 136
    # features = ['mean_hr','sdnn','minuteweek'] ## 167
    # features = ['mean_hr','hf_a','lf_a','minuteweek'] ## 195

    # an interesting outcome from the signal quality point of view
    # might also be explained due to larger variance in HRV than HR between individuals

    for features in [['mean_hr','rmssd_milliseconds','sdnn','hf_a','lf_a','steps','minuteweek'],
                     ['mean_hr','rmssd_milliseconds','sdnn','hf_a','lf_a','steps'],  
                     ['mean_hr','minuteweek'], 
                     ['mean_hr','rmssd_milliseconds'], 
                     ['mean_hr','rmssd_milliseconds','minuteweek'], 
                     ['mean_hr','steps','minuteweek'], 
                     ['mean_hr','sdnn','minuteweek'], 
                     ['mean_hr','hf_a','lf_a','minuteweek']]:        
        X = windows.dropna(subset=['sleep_gt'])[features]
        y = windows['sleep_gt'].dropna()
        print(features)
        for i in linspace(10,50,9):
            clf = tree.DecisionTreeClassifier(class_weight='balanced',max_depth=i).fit(X,y)
            windows['sleep_lr'] = 3
            windows['sleep_lr'] = clf.predict(windows[features])
            print(i, len(windows.loc[(windows.sleep_lr == 1) & (windows.sleep_feed == 0)]))

def plot_results(feature, userid):

    g = sb.FacetGrid(windows.loc[windows.userid  == userid].fillna(3), hue='sleep_lr',size=5, aspect=1.5)
    g.map(plt.scatter, 'tstamp',feature).add_legend()
    g.ax.set(xlabel='Date',
             ylabel='Value',
             title='mean HR')
    g.fig.autofmt_xdate()

    plt.scatter(windows.loc[(windows.userid  == userid) & (windows['sleep_gt'] == 1) & (windows['sleep_lr'] == 0)].tstamp.values, windows.loc[(windows.userid  == userid) & (windows['sleep_gt'] == 1) & (windows['sleep_lr'] == 0), feature].values, color='blue', marker='v')

    plt.scatter(windows.loc[(windows.userid  == userid) & (windows['sleep_gt'] == 0) & (windows['sleep_lr'] == 1)].tstamp.values, windows.loc[(windows.userid  == userid) & (windows['sleep_gt'] == 0) & (windows['sleep_lr'] == 1), feature].values, color='pink', marker='v')

    plt.scatter(windows.loc[(windows.userid  == userid) & (windows['sleep_lr'] == 1) & (windows['steps'] > 0)].tstamp.values, windows.loc[(windows.userid  == userid) & (windows['sleep_lr'] == 1) & (windows['steps'] > 0), feature].values, color='black', marker='+')
    
    plt.scatter(windows.loc[(windows.userid  == userid) & (windows['sleep_lr'] == 1) & (windows['sleep_feed'] == 0)].tstamp.values, windows.loc[(windows.userid  == userid) & (windows['sleep_lr'] == 1) & (windows['sleep_feed'] == 0), feature].values, color='red', marker='+')


def CV_lr(X,y,num_trials):
    
    non_nested_scores = np.zeros(num_trials)
    nested_scores = np.zeros(num_trials)
    
    for i in range(num_trials):
        print(i)
    #    i = 0 
        inner_cv = StratifiedKFold(n_splits=10, shuffle=True, random_state=i)
#        outer_cv = KFold(n_splits=10, shuffle=True, random_state=i)
     
        ## non-nested parameter search
        # clf = LogisticRegressionCV(cv=inner_cv,penalty='l1',solver='liblinear')
        clf = LogisticRegressionCV(cv=inner_cv)
        clf.fit(X,y)
        non_nested_scores[i] = clf.score(X,y)
     
        ## nested CV
    #     nested_score = cross_val_score(clf, X, y, cv=outer_cv)
    #     nested_scores[i] = nested_score.mean()
     
     
    # score_difference = non_nested_scores - nested_scores
     
    # print("LR Average difference of {0:6f} with std. dev. of {1:6f}."
    #        .format(score_difference.mean(), score_difference.std()))
    # print('LR nested_mean', nested_scores.mean())
    print('LR non-nested_mean', non_nested_scores.mean())    
        
    return clf

def CV_dt(X,y,num_trials):
    dt = tree.DecisionTreeClassifier()
    parameter_grid = {'class_weight':['balanced'],
        #'criterion': ['gini', 'entropy'],
    #                  'splitter': ['best', 'random'],
                      'max_depth': [3, 10, 30]}#,
    #                  'max_features': list(range(1,X.shape[1]+1))}
     

    non_nested_scores = np.zeros(num_trials)
    nested_scores = np.zeros(num_trials)
    
    for i in range(num_trials):
     
    #    i = 0 
        inner_cv = StratifiedKFold(n_splits=10, shuffle=True, random_state=i)
        outer_cv = StratifiedKFold(n_splits=10, shuffle=True, random_state=i)
     
        ## non-nested parameter search
     
        clf = GridSearchCV(estimator=dt,
                           param_grid=parameter_grid,
                           cv=inner_cv)
        clf.fit(X,y)
        non_nested_scores[i] = clf.best_score_
     
        ## nested CV
        nested_score = cross_val_score(clf, X, y, cv=outer_cv)
        nested_scores[i] = nested_score.mean()
     
     
    score_difference = non_nested_scores - nested_scores
     
    print("DT Average difference of {0:6f} with std. dev. of {1:6f}."
           .format(score_difference.mean(), score_difference.std()))
    print('DT nested_mean', nested_scores.mean())
    print('DT non-nested_mean', non_nested_scores.mean())    
        
    return clf.best_estimator_


# num_trials = 10
#clf = CV_lr(X,y, num_trials)  ## doesn't work well
#clf = CV_dt(X,y, num_trials)  ## see sleep_dt_evaluation for better eval

### check which features work best!
feature_selector()

## 20 seems to be the best
features = ['mean_hr','minuteweek'] ## 74 <<<<<<<<<<<<< BEST
X = windows.dropna(subset=['sleep_gt'])[features]
y = windows['sleep_gt'].dropna()
clf = tree.DecisionTreeClassifier(class_weight='balanced',max_depth=20).fit(X,y)

windows['sleep_lr'] = 3
windows['sleep_lr'] = clf.predict(windows[features])
plot_results('mean_hr',24)

print(len(windows.loc[(windows.sleep == 1) & (windows.sleep_feed == 0)]))
print(len(windows.loc[(windows.sleep_lr == 1) & (windows.sleep_feed == 0)]))

for best_clf in [clf]:
     preds = best_clf.predict_proba(X)[:,1]
     fpr, tpr, threshold = metrics.roc_curve(windows['sleep_gt'].dropna(), preds)
     roc_auc = metrics.auc(fpr, tpr)
     plt.figure()
     plt.title('Receiver Operating Characteristic')
     plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
     plt.legend(loc = 'lower right')
     plt.plot([0, 1], [0, 1],'r--')
     plt.xlim([0, 1])
     plt.ylim([0, 1])
     plt.ylabel('True Positive Rate')
     plt.xlabel('False Positive Rate')


### FINALLY merge feedback into labels and retrain with selected features and hyperparams!
windows.loc[windows['sleep_feed'] == 0, 'sleep_gt'] = 0
features = ['mean_hr','minuteweek'] ## 74 <<<<<<<<<<<<< BEST
X = windows.dropna(subset=['sleep_gt'])[features]
y = windows['sleep_gt'].dropna()
clf = tree.DecisionTreeClassifier(class_weight='balanced',max_depth=20).fit(X,y)
windows['sleep_lr'] = 3
windows['sleep_lr'] = clf.predict(windows[features])
plot_results('mean_hr',21)


### Save to MLPACK param file
from utils import DT_writer
clf_exp = DT_writer(clf)
clf_exp.save_xml('/home/gb/felix/repos/cpp/classifiers/share/sleepClassifierV05_DT--2017-09-08.xml')




