from sklearn.tree import DecisionTreeClassifier
#from sklearn.cross_validation import StratifiedKFold
from sklearn.model_selection import GridSearchCV, cross_val_score, KFold, StratifiedKFold
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.feature_selection import SelectFromModel
from sklearn.preprocessing import PolynomialFeatures
import numpy as np
from c_lib.medfilt_module import *
from utils import DT_writer



NUM_TRIALS = 10 

## TEST 1 - USE ABSOLUTE VALUES

mit_bih_5m = pd.read_sql('mit_bih_5m',con)

### SLEEP-WAKE
X_learn = mit_bih_5m[['rmssd_milliseconds','sdnn','lf_a','hf_a','sleep']].dropna(subset=['sleep'])
X = X_learn.drop('sleep',axis=1)
y = X_learn['sleep']

clf_sleep = tree.DecisionTreeClassifier(max_depth=20, class_weight='balanced')
clf_sleep.fit(X,y)

### REM-NREM
X_learn = mit_bih_5m[['rmssd_milliseconds','sdnn','lf_a','hf_a','NREM']].dropna(subset=['NREM'])
X = X_learn.drop('NREM',axis=1)
y = X_learn['NREM']

clf_nrem = DecisionTreeClassifier(max_depth=20, class_weight='balanced')
clf_nrem.fit(X,y)

cj_nrem = DT_writer(clf_nrem)
cj_nrem.feature_strings = ['rmssd_milliseconds','sdnn','lf_a','hf_a']
cj_nrem.class_descriptions = ['NREM','REM']


### deep-light
X_learn = mit_bih_5m[['rmssd_milliseconds','sdnn','lf_a','hf_a','deep']].dropna(subset=['deep'])
X = X_learn.drop('deep',axis=1)
y = X_learn['deep']

clf_deep = tree.DecisionTreeClassifier(max_depth=20, class_weight='balanced')
clf_deep.fit(X,y)

cj_deep = DT_writer(clf_deep)
cj_deep.feature_strings = ['rmssd_milliseconds','sdnn','lf_a','hf_a']
cj_deep.class_descriptions = ['Light','Deep']
cj_deep.save('/tmp/1')



### LOADING "REAL" DATA

user = 7
time_from = pd.to_datetime('2016-12-22 17:00:00')
hours = pd.to_timedelta(30,'d')
window_size_minutes = 5

r2r_alex = read_r2r(user, time_from, hours)

r2r_alex['window'] = r2r_alex.index.floor(str(window_size_minutes)+'T')
base_windows = calc_base_features(r2r_alex, hrquality)
freq_windows = frequency_features(r2r_alex)
w_alex = base_windows.join(freq_windows,how='outer')
w_alex.drop('hrq_mean_hr',axis=1,inplace=True)
w_alex.dropna(inplace=True)
w_alex['sleep'] = 0
w_alex.ix[w_alex.index.floor('3T').isin(windows_alex.loc[windows_alex.awake_sleep == 1].index), 'sleep'] = 1
w_alex.index.name = 'zwhen'
w_alex.rmssd_milliseconds *= 1e-3   ## classifiers use MS values 


w_alex['stage'] = nan

w_alex.loc[w_alex.sleep == 1, 'nrem'] = clf_nrem.predict(w_alex.loc[w_alex.sleep == 1, ['rmssd_milliseconds','sdnn','lf_a','hf_a']])

w_alex.loc[w_alex.sleep == 1, 'deep'] = clf_deep.predict(w_alex.loc[w_alex.sleep == 1, ['rmssd_milliseconds','sdnn','lf_a','hf_a']])

w_alex.loc[w_alex.sleep == 0, 'stage'] = 4
w_alex.loc[(w_alex.sleep == 1) & (w_alex.nrem == 0), 'stage'] = 3
w_alex.loc[(w_alex.sleep == 1) & (w_alex.nrem == 1) & (w_alex.deep == 0), 'stage'] = 2
w_alex.loc[(w_alex.sleep == 1) & (w_alex.nrem == 1) & (w_alex.deep == 1), 'stage'] = 1

