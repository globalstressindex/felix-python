### DEV-548 - create a unified dataset for the new sleep classifier
### adds garmin steps data and user feedback information
### adjusts dist_3h_sec based on timezone data

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sb
from data_loading_pg import *
from options import Options

from sqlalchemy import create_engine

APPLE_DELTA = pd.datetime(year=2001, month=1, day=1, hour=0, second=0) - pd.to_datetime(0,unit='s')
con = create_engine('postgresql:///felix')


## load data and convert time

windows = pd.read_sql('zinsightcalc3',con);
garminextras = pd.read_sql('zgarminextras',con)
timezone = pd.read_sql('ztimezone',con);
feedback = pd.read_sql('zclassificationfeedback',con)

windows.set_index('start_time', inplace=True)

## investigating sleep only
windows['sleep'] = np.nan
windows.loc[windows.awake_sleep.isin([0,1,3]),'sleep'] = 1
windows.loc[windows.awake_sleep == 2,'sleep'] = 0
windows.dropna(subset=['sleep'], inplace=True)

## add garmin info - steps
windows['steps'] = 0

for user in garminextras.zuser.unique():
    stepdelta = garminextras.loc[garminextras.zuser == user].set_index('zwhen').sort_index().zsteps.diff().clip_lower(0)
    stepdelta.fillna(0, inplace=True)
    stepdelta = stepdelta.groupby(stepdelta.index//180).sum()
    stepdelta.index = stepdelta.index*180
    windows.loc[windows.userid==user,'steps'] = stepdelta
    
## add feedback data
### only got a few rows so simply iterate for readability
## yet very slow!!

windows['sleep_feed'] = np.nan

for row in feedback.loc[feedback.zstate == 'asleep'].itertuples():
    ### expand feedback as it is only for 5 minutes most of the time!
    fromtime = row.zfromtime
    totime = row.ztotime
    
    fromhour  = (pd.to_datetime(row.zfromtime,unit='s') + APPLE_DELTA).hour
    tohour  = (pd.to_datetime(row.ztotime,unit='s') + APPLE_DELTA).hour
    
    if (fromhour >= 5) & (fromhour < 12):
        totime = totime + 6*3600;
    if (tohour >= 20):
        fromtime = fromtime - 6*3600;         
    windows.loc[(windows.userid == row.zuser) &
                (windows.index >= fromtime) &
                (windows.index <= totime), 'sleep_feed'] = 0

## now let's check where our classifier doesn't meet reality
    
windows['stepbin'] = 0
windows.loc[windows.steps > 0, 'stepbin'] = 1
windows['sleepwalk'] =windows['stepbin'] & windows['sleep']    

windows['wrongsleep'] = np.nan
windows.loc[(windows.sleep_feed == 0)  & (windows.sleep == 1), 'wrongsleep'] = 1

## stay with cocoa timestamps
#windows.start_time = pd.to_datetime(windows.start_time,unit='s') + APPLE_DELTA

## adjust TZ - some users travelled to GMT+2, thankfully ONCE (can take min/max)
### currently done manually

for user in timezone.loc[timezone.zgmtoffset != 3600,'zuser'].unique():
    tzdata = timezone.loc[timezone.zuser==user].set_index('zwhen').sort_index().zgmtoffset
    tzdata = tzdata.loc[tzdata.shift() != tzdata]
# TODO: PROPER WAY OF DOING THIS        
    print(user,tzdata)
    
windows['tzsign'] = 0    
windows.loc[((pd.to_datetime(windows.index,unit='s') + APPLE_DELTA + pd.to_timedelta('1h')).hour >= 3) &
            ((pd.to_datetime(windows.index,unit='s') + APPLE_DELTA + pd.to_timedelta('1h')).hour < 15), 'tzsign'] = 1

windows.loc[(((pd.to_datetime(windows.index,unit='s') + APPLE_DELTA + pd.to_timedelta('1h')).hour < 3) |
            ((pd.to_datetime(windows.index,unit='s') + APPLE_DELTA + pd.to_timedelta('1h')).hour >= 15)), 'tzsign'] = -1

## windows['dist_3h_backup'] = windows['dist_3h_sec']

windows.loc[(windows.userid == 24) & (windows.index >= 525257417.5342), 'dist_3h_sec'] = abs(windows.loc[(windows.userid == 24) & (windows.index >= 525257417.5342), 'dist_3h_sec'] + windows.loc[(windows.userid == 24) & (windows.index >= 525257417.5342), 'tzsign'] * 3600)

windows.loc[(windows.userid == 32) & (windows.index >= 523277598.9397) & (windows.index <= 523626927.5176), 'dist_3h_sec'] = abs(windows.loc[(windows.userid == 32) & (windows.index >= 523277598.9397) & (windows.index <= 523626927.5176), 'dist_3h_sec'] + windows.loc[(windows.userid == 32) & (windows.index >= 523277598.9397) & (windows.index <= 523626927.5176), 'tzsign'] * 3600)
 
windows.loc[(windows.userid == 37) & (windows.index >= 524656918.5334) & (windows.index <= 525265216.4842), 'dist_3h_sec'] = abs(windows.loc[(windows.userid == 37) & (windows.index >= 524656918.5334) & (windows.index <= 525265216.4842), 'dist_3h_sec'] + windows.loc[(windows.userid == 37) & (windows.index >= 524656918.5334) & (windows.index <= 525265216.4842), 'tzsign'] * 3600)

windows.loc[windows.dist_3h_sec > 12*3600, 'dist_3h_sec'] = 12*3600 - (windows.loc[windows.dist_3h_sec > 12*3600, 'dist_3h_sec'] - 12*3600)

### done adjusting dist_3h_sec (backup in dist_3h_backup)

## SAVE THE DATASET
windows.to_sql('windows_0309',con,if_exists='replace')








                       
