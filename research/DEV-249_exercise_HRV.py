##
## DEV-249 - investigate whether exercise has an impact on HRV in short/med term
##
##

from filtering import *
from heart_functions import *
from rr_features import *



### Mikhail Polar

r2r_garmin = read_r2r_garmin(9,pd.to_datetime('2017-06-01 14:40:00'),pd.to_timedelta(90,'m')) #TZadj -1HR
r2r_polar = read_r2r_polar(9,pd.to_datetime('2017-06-01 14:40:00'),pd.to_timedelta(90,'m'))
w0s =  pd.to_datetime('2017-06-01 14:16:00');
w0e =  pd.to_datetime('2017-06-01 14:26:00');
e0s =  pd.to_datetime('2017-06-01 14:16:00');
e0e =  pd.to_datetime('2017-06-01 14:26:00');
w0s =  pd.to_datetime('2017-06-01 14:16:00');
w0e =  pd.to_datetime('2017-06-01 14:26:00');
w0s =  pd.to_datetime('2017-06-01 14:16:00');
w0e =  pd.to_datetime('2017-06-01 14:26:00');




r2r_garmin = read_r2r_garmin(9,pd.to_datetime('2017-06-03 17:40:00'),pd.to_timedelta(90,'m')) #TZadj -1HR
r2r_polar = read_r2r_polar(9,pd.to_datetime('2017-06-03 17:40:00'),pd.to_timedelta(90,'m'))

r2r_garmin = read_r2r_garmin(9,pd.to_datetime('2017-06-09 15:25:00'),pd.to_timedelta(90,'m')) #TZadj -1HR
r2r_polar = read_r2r_polar(9,pd.to_datetime('2017-06-09 15:25:00'),pd.to_timedelta(90,'m'))




### Mike POLAR
r2r_garmin = read_r2r_garmin(8,pd.to_datetime('2017-06-20 20:30:00'),pd.to_timedelta(90,'m')) #TZadj -1HR
r2r_polar = read_r2r_polar(8,pd.to_datetime('2017-06-20 20:30:00'),pd.to_timedelta(90,'m'))

r2r_garmin = read_r2r_garmin(8,pd.to_datetime('2017-06-16 17:30:00'),pd.to_timedelta(90,'m')) #TZadj -1HR
r2r_polar = read_r2r_polar(8,pd.to_datetime('2017-06-16 17:30:00'),pd.to_timedelta(90,'m'))

# polar only
r2r_garmin = read_r2r_garmin(8,pd.to_datetime('2017-06-15 21:00:00'),pd.to_timedelta(100,'m')) #TZadj -1HR
r2r_polar = read_r2r_polar(8,pd.to_datetime('2017-06-15 21:00:00'),pd.to_timedelta(100,'m'))


## Mike GARMIN ONLY

r2r_garmin = read_r2r_garmin(8,pd.to_datetime('2017-06-18 11:50:00'),pd.to_timedelta(100,'m')) #TZadj -1HR
r2r_polar = read_r2r_polar(8,pd.to_datetime('2017-06-18 11:50:00'),pd.to_timedelta(100,'m'))

r2r_garmin = read_r2r_garmin(8,pd.to_datetime('2017-06-06 16:15:00'),pd.to_timedelta(100,'m')) #TZadj -1HR
r2r_polar = read_r2r_polar(8,pd.to_datetime('2017-06-06 16:15:00'),pd.to_timedelta(100,'m'))








### Plot signaals
plt.figure()
polar = r2r_polar.copy()
ms = r2r_garmin.copy()
ax = plt.gca()
if len(polar):
    ax = polar.rr_interval.plot(marker='.',label='RR polar')
ms.rr_interval.plot(ax=ax, marker='.',label='RR garmin')
###ms.rr_interval += 0.0006  ### DRIFT CORRECTION (see research/ms_drift.py)
filter_rr_data(polar) 
filter_rr_data(ms)
if len(polar):
    polar.rr_interval.plot(marker='.',label='RR polar filtered')
ms.rr_interval.plot(ax=ax, marker='.',label='RR garmin filtered')
ax.legend()

### Plot smooth signaals
plt.figure()
polar = r2r_polar.copy()
ms = r2r_garmin.copy()
ax = plt.gca()
if len(polar):
    ax = polar.rolling('300s').rr_interval.mean().plot(marker='.',label='RR polar')
ms.rolling('300s').rr_interval.mean().plot(ax=ax, marker='.',label='RR garmin')
###ms.rr_interval += 0.0006  ### DRIFT CORRECTION (see research/ms_drift.py)
filter_rr_data(polar) 
filter_rr_data(ms)
if len(polar):
    polar.rolling('300s').rr_interval.mean().plot(marker='.',label='RR polar filtered')
ms.rolling('300s').rr_interval.mean().plot(ax=ax, marker='.',label='RR garmin filtered')
ax.legend()


### Plot RMSSD
plt.figure()
polar = r2r_polar.copy()
ms = r2r_garmin.copy()
ax=plt.gca()
if len(polar):
    ax = polar.rolling('300s').rr_interval.apply(calc_rmssd).plot(label='RMSSD polar')
ms.rolling('300s').rr_interval.apply(calc_rmssd).plot(ax=ax, label='RMSSD garmin')
# ms.rr_interval += 0.0006  ### DRIFT CORRECTION (see research/ms_drift.py)
filter_rr_data(polar) 
filter_rr_data(ms)
if len(polar):
    polar.rolling('300s').rr_interval.apply(calc_rmssd).plot(ax=ax, label='RMSSD polar filtered')
ms.rolling('300s').rr_interval.apply(calc_rmssd).plot(ax=ax, label='RMSSD garmin filtered')
#(log(accel['3d'].rolling('10s').std()*1e3)/50).plot(ax=ax, label='Accel STD over 10s window', alpha=0.5)
# ms = ms.ix[ac_std.ix[ms.index] <= 0.5]
# ms.rolling('300s').rr_interval.apply(calc_rmssd).plot(ax=ax, label='RMSSD ms accel control')
ax.axvline(w0s, color='k', linestyle='--', lw=0.5);
ax.axvline(w0e, color='k', linestyle='--', lw=0.5);
ax.legend()
