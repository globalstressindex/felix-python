import statistics as stats

import numpy as np
from numpy import nan
from .breathing_rate import limit_br


def _brv04_find_neighbours(peaks_or_troughs, index):
    previous_pot = None
    next_pot = None
    for pot in peaks_or_troughs:
        # if pot:
        if pot['index'] >= index:
            next_pot = pot
            break
        previous_pot = pot
    return {'previous': previous_pot, 'next': next_pot}


def _brv04_peak(x0, x1, x2):
    return x0 <= x1 and x1 > x2


def _brv04_trough(x0, x1, x2):
    return x0 >= x1 and x1 < x2


def _brv04_calculate_peaks_and_troughs(rr_intervals):
    peaks = []
    troughs = []
    for i in range(1, len(rr_intervals) - 1):
        value = rr_intervals[i]
        is_peak = _brv04_peak(rr_intervals[i - 1], value, rr_intervals[i + 1])
        is_trough = _brv04_trough(rr_intervals[i - 1], value, rr_intervals[i + 1])
        if is_peak:
            peaks.append({
                'index': i,
                'value': value,
            })
        if is_trough:
            troughs.append({
                'index': i,
                'value': value,
            })
    return dict(peaks=peaks, troughs=troughs)


def _brv04_mutate_peaks__add_info_on_neighbouring_troughs(peaks, troughs):
    for i, peak in enumerate(peaks):
        neighbours = _brv04_find_neighbours(troughs, peak['index'])
        previous_neighbour = neighbours['previous']
        next_neighbour = neighbours['next']
        min_diff = None
        max_diff = None
        avg_diff = None
        if previous_neighbour:
            previous_value = previous_neighbour['value']
            min_diff = max_diff = avg_diff = diff_from_previous = (peak['value'] - previous_value)
        if next_neighbour:
            next_value = next_neighbour['value']
            min_diff = max_diff = avg_diff = diff_from_next = (peak['value'] - next_value)
        if previous_neighbour and next_neighbour:
            min_diff = min(diff_from_previous, diff_from_next)
            max_diff = max(diff_from_previous, diff_from_next)
            avg_diff = stats.mean([diff_from_previous, diff_from_next])
        peak['min_diff'] = min_diff
        peak['max_diff'] = max_diff
        peak['avg_diff'] = avg_diff


def _brv04_mutate_peaks__mark_accepted_by_avg_diff(peaks, rr_intervals, fc_options):
    for i, peak in enumerate(peaks):
        avg_diff = peak['avg_diff']

        # Calculate threshold based on std of local neighbourhood of values
        num_nei = fc_options.br_v04_number_of_neighbouring_peaks_for_std
        start_peak = peaks[max(0, i - num_nei)]
        end_peak = peaks[min(len(peaks) - 1, i + num_nei)]
        std = np.std(rr_intervals[start_peak['index']:end_peak['index']])

        threshold = std * fc_options.br_v04_threshold

        peak['accepted'] = avg_diff is not None and avg_diff > threshold


def _brv04_group_accepted_peaks(peaks):
    # Only accept peaks that have no rejected peaks between them
    # That way we should be hooking into the pure breathing rate.
    # TODO: fall back to using all peaks if there are no neighbouring accepted peaks
    accepted_peak_groups = []
    current_group = []
    for i, peak in enumerate(peaks):
        previous_peak = None
        if ((i - 1) >= 0):
            previous_peak = peaks[i - 1]
        next_peak = None
        if ((i + 1) < len(peaks)):
            next_peak = peaks[i + 1]
        if peak['accepted'] and (
            (previous_peak and previous_peak['accepted']) or (next_peak and next_peak['accepted'])
        ):
            current_group.append(peak)
        elif current_group:
            accepted_peak_groups.append(current_group)
            current_group = []
    return accepted_peak_groups


def _calc_br_v04(rr_intervals, fc_options):
    if len(rr_intervals) < 3:
        return nan

    result = _brv04_calculate_peaks_and_troughs(rr_intervals=rr_intervals)
    peaks = result['peaks']
    _brv04_mutate_peaks__add_info_on_neighbouring_troughs(peaks=peaks, troughs=result['troughs'])
    _brv04_mutate_peaks__mark_accepted_by_avg_diff(peaks=peaks, rr_intervals=rr_intervals, fc_options=fc_options)

    # Only accept peaks that have no rejected peaks between them
    # That way we should be hooking into the pure breathing rate.
    # TODO: fall back to using all peaks if there are no neighbouring accepted peaks
    accepted_peak_groups = _brv04_group_accepted_peaks(peaks)

    sizes_of_peaks_seconds = []
    for peak_group in accepted_peak_groups:
        for i, peak in enumerate(peak_group):
            if i > 0:
                last_peak = peak_group[i - 1]
                time_from_last = sum(rr_intervals[last_peak['index'] + 1:peak['index'] + 1])
                peak['time_from_last'] = time_from_last
                sizes_of_peaks_seconds.append(time_from_last)
    i = None

    breath_rate = 0.0
    if sizes_of_peaks_seconds:
        breath_rate = 60.0 / stats.median(sizes_of_peaks_seconds)
    return limit_br(breath_rate, fc_options)


def calc_br_v04(rr_intervals, fc_options):
    try:
        return _calc_br_v04(rr_intervals, fc_options)
    except:
        print(rr_intervals, fc_options)
        raise
