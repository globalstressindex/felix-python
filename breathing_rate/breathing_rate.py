# import statistics as stats

import numpy as np
from numpy import nan
# from ..single_analysis2 import SingleAnalysis2
# from models.insight_calc import InsightCalcType


def limit_br(breath_rate, fc_options):
    return nan if breath_rate > fc_options.br_max or breath_rate < fc_options.br_min else breath_rate


def calc_br_v01(rr_intervals, fc_options):
    smoothing_window = 1  # number of samples to look forward and backward
    samples_in_window = 1 + (smoothing_window * 2)
    breath_smooth = 2
    breath_threshold = 1

    if len(rr_intervals) <= (samples_in_window + breath_smooth):
        return nan

    breath_count = 0.0

    rr_smoothed = []
    for i in range(smoothing_window, len(rr_intervals) - (smoothing_window * 2)):
        local_sum = 0
        for j in range(samples_in_window):
            local_sum += rr_intervals[i - smoothing_window + j]
        local_average = local_sum / samples_in_window
        rr_smoothed.append(local_average)

    rr_increasing = []
    for i in range(1, len(rr_smoothed) - 1):
        increasing = rr_smoothed[i - 1] < rr_smoothed[i]
        rr_increasing.append(increasing)

    mid_breath = False
    for i in range(len(rr_increasing) - breath_smooth):
        temp = 0
        for j in range(breath_smooth):
            temp += int(rr_increasing[i + j])

        if temp > breath_threshold:
            if not mid_breath:
                breath_count += 1
                mid_breath = True
        else:
            mid_breath = False

    time_factor = 60.0 / sum(rr_intervals)

    breath_rate = breath_count * time_factor
    return limit_br(breath_rate, fc_options)


def calc_br_v02(rr_intervals, fc_options):
    if len(rr_intervals) < 3:
        return nan

    mean_val = rr_intervals.mean()

    def peak(x):
        return x[0] < x[1] and x[2] < x[1]

    def trough(x):
        return x[1] < x[0] and x[1] < x[2]

    zipped_rr_intervals = zip(*[rr_intervals[i:] for i in range(3)])
    peaks_and_troughs = []
    peaks = []
    troughs = []
    for i, x in enumerate(zipped_rr_intervals):
        if peak(x):
            peaks.append({'index': i + 1, 'value': x[1]})
        if trough(x):
            troughs.append({'index': i + 1, 'value': x[1]})
        if peak(x) or trough(x):
            peaks_and_troughs.append(x[1])

    def is_period(x):
        return x[0] > mean_val and x[1] < mean_val

    zipped_peaks_and_troughs = zip(peaks_and_troughs, peaks_and_troughs[1:])
    periods = list(filter(is_period, zipped_peaks_and_troughs))

    breath_rate = len(periods) * (60.0 / sum(rr_intervals))

    return limit_br(breath_rate, fc_options)


def _calc_br_v03(rr_intervals, fc_options):
    if len(rr_intervals) < 3:
        return nan

    def peak(x0, x1, x2):
        return x0 <= x1 and x1 > x2

    def trough(x0, x1, x2):
        return x0 >= x1 and x1 < x2

    peaks = []
    troughs = []
    size_of_current_peak_seconds = 0
    size_of_current_trough_seconds = 0
    sizes_of_peaks_seconds = []
    sizes_of_troughs_seconds = []

    for i in range(1, len(rr_intervals) - 1):
        rr_value = rr_intervals[i]
        is_peak = peak(rr_intervals[i - 1], rr_value, rr_intervals[i + 1])
        is_trough = trough(rr_intervals[i - 1], rr_value, rr_intervals[i + 1])

        size_of_current_peak_seconds += rr_value
        size_of_current_trough_seconds += rr_value

        if is_peak:
            peaks.append({'index': i, 'value': rr_value})
            sizes_of_peaks_seconds.append(size_of_current_peak_seconds)
            size_of_current_peak_seconds = 0

        if is_trough:
            troughs.append({'index': i, 'value': rr_value})
            sizes_of_troughs_seconds.append(size_of_current_trough_seconds)
            size_of_current_trough_seconds = 0

    # accepted_peaks = []
    # i = 0
    # while i < len(rr_intervals, fc_options=None):
    #     # find index of the next peak and trough
    #     next_peak_index = find_first_index(peaks[i:])
    #     next_trough_index = find_first_index(troughs[i:])

    #   periods = list(filter(lambda x: x[0] > mean and x[1] < mean, zip(peaks_and_troughs, peaks_and_troughs[1:])))
    breath_rate = 0.0
    if sizes_of_peaks_seconds:
        breath_rate = 60.0 / np.median(sizes_of_peaks_seconds)
    if breath_rate > fc_options.br_max or breath_rate < fc_options.br_min:
        return nan
    return limit_br(breath_rate, fc_options)


def calc_br_v03(rr_intervals, fc_options):
    try:
        return _calc_br_v03(rr_intervals, fc_options)
    except:
        print(rr_intervals, fc_options)
        raise


def calc_br_v05(rr_intervals):
    if len(rr_intervals) < 1:
        return nan
#        raise # return SingleAnalysis2(dict(value=0.0, confidence=0.0, type=InsightCalcType.br_v05))
    try:
        """
        DO NOT CHANGE (at the moment)

        calc_br_v05 function is used by the msband filtering LogisticRegression model

        Please make a new version of calc_br_v05 or remove from msband filtering
        """

        """
        John Sandall's implementation.
        May not return breaths per minute

        Determine breathing rate by treating HRV data as time series and finding most prominent
        non-zero autocorrelation.

        Returns number of breaths detected.
        """
#        rr_intervals = data['rr_interval']
        # Normalise by deducting mean
        rr_normalised = rr_intervals - np.mean(rr_intervals)

        # Calculate normalised autocorrelations
        autocorr = np.correlate(rr_normalised, rr_normalised, "same") / np.sum(rr_normalised ** 2)
        autocorr_sliced = autocorr[int(len(rr_intervals) / 2):]  # ignore negative autocorrelations

        # ACF is smooth function, so can use a simple approach to find first peak
        # First, find first low point (first place where diffs are positive)
        autocorr_diffs = np.diff(autocorr_sliced)
        first_low = np.argmax(autocorr_diffs > 0)  # argmax stops at first True for booleans

        # Use same approach to find first peak (look for index where diffs turn negative)
        first_peak = np.argmax(autocorr_diffs[first_low:] < 0) + first_low

        # First peak of ACF corresponds to most common period (i.e. intervals per breath)
        # Calculate number of breaths by dividing total_intervals / intervals_per_breath
        value = len(rr_intervals) / first_peak if first_peak > 0 else 0
    except:
        # print('rr_intervals: ', rr_intervals)
        # print('rr_normalised: ', rr_normalised)
        # print('autocorr: ', autocorr)
        # print('autocorr_sliced: ', rr_intervals)
        # print('first_low: ', first_low)
        # print('first_peak: ', first_peak)
        # print('value: ', value)
        # raise
        return nan
    return value
#    return limit_br(value, fc_options)
