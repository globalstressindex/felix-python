

class Options():
    def __init__(self, kwargs={}):
        self.br_v04_threshold = kwargs.pop('br_v04_threshold', 1.5)
        self.br_v04_number_of_neighbouring_peaks_for_std = kwargs.pop('br_v04_number_of_neighbouring_peaks_for_std', 1)

        self.br_min = kwargs.pop('br_min', 4)
        self.br_max = kwargs.pop('br_max', 50)

        # TODO, really don't think these presentation options should be present here
        # but leaving for the moment until I have a better idea of where they should live
        self.window_size_seconds = kwargs.pop('window_size_seconds', 180)
        self.window_increments_seconds = kwargs.pop('window_increments_seconds', 10)

        self.filter_msband = True
        self.filter_msband__chunk_size_in_seconds = 30.0;
        self.filter_msband__expected_features = ["msband_rsd", "msband_breaths"];
        self.filter_msband__classes = [0, 1];
        self.filter_msband__intercept = 29.79638669;
        self.filter_msband__coefficients = [-150.52279488, -1.08043185];

        self.min_hr = kwargs.pop('min_hr', 40.0)
        self.max_hr = kwargs.pop('max_hr', 185.0)
        self.filter_extreme_values = kwargs.pop('filter_extreme_values', True)
        self.filter_extreme_changes = kwargs.pop('filter_extreme_changes', True)
        self.filter_extreme_change_gt_N_seconds = kwargs.pop('filter_extreme_change_gt_N_seconds', 0.2)
        self.filter_extreme_change_N_quiet_pairs = kwargs.pop('filter_extreme_change_N_quiet_pairs', 3)
        self.filter_extreme_local_std = kwargs.pop('filter_extreme_local_std', True)
        self.filter_extreme_local_std_threshold = kwargs.pop('filter_extreme_local_std_threshold', 3)
        self.filter_extreme_local_std_local_size_seconds = kwargs.pop('filter_extreme_local_std_local_size_seconds', 6)
        self.filter_extreme_local_std_local_increment_seconds = kwargs.pop('filter_extreme_local_std_local_increment_seconds', 2)

        self.lomb_scargle__method = kwargs.pop('lomb_scargle__method', 'fast')
        self.lomb_scargle__frequency_kwargs = kwargs.pop('lomb_scargle__frequency_kwargs', dict(
          min=self.br_min / 60,  # down to e.g. 4 breathes per minute (1 breath per 15 seconds)
          max=self.br_max / 60,  # up to e.g. 50 breathes per minute (1 breath per 1.2 seconds)
          steps=500,
        ))

        default_window_size = (60 / self.br_min) * 2
        self.lomb_scargle__window_size_seconds = kwargs.pop('lomb_scargle__window_size_seconds', default_window_size)

        self.debug_info = kwargs.pop('debug_info', False)

        if kwargs:
            raise Exception('Unused kwargs for options: {}'.format(kwargs))


def get_default_options():
    return Options()
