#
# representation uses 15 minute windows
# this code aggregates 3 minute window into chunks of 5
#
import pandas as pd
import numpy as np

def consensus_by_neighbours(values):
    consensus_values = []
    for i, phase in enumerate(values):
        if i > 1 and i < (len(values) - 2):
            if (phase == values[i - 2] and phase == consensus_values[i - 1]) or phase == values[i + 1] or phase == values[i + 2]:
                consensus_values.append(phase)
            else:
                consensus_values.append(consensus_values[i - 1])
        else:
            consensus_values.append(phase)
    return consensus_values
    
def generate_large_windows(windows):
    """
    smoothes 3 minute windows into 15 minute blocks
    Arguments:
    - `windows`:
    """
    ## TODO chunking into 15 minute windows 0, 15, 30, 45
    ## TODO add more flexibility, remove hardcoded values
    windows['large_window'] = windows.index.map(lambda dt: pd.datetime(dt.year, dt.month, dt.day, dt.hour,15*(dt.minute // 15)))
    
    # sleep classifications - consensus by neighbours
    windows['awake_sleep'] = consensus_by_neighbours(windows['awake_sleep'])
    
    large_windows = windows[['large_window','awake_sleep','exercising_or_not', 'acute_score']].copy()
        
    
    # sleep and exercise chunk_mode

    ## chunking: mode or mean if mode is not unique 
    ## Dirty hack which rounds x.5 down to x but does all other values correctly, provided our input data
    ## (series of len <= 5 including 0,1,2)
    large_windows = large_windows.groupby('large_window').agg(lambda x:round(x.mean() - 0.0001))

    # sleep takes precedence over exercise when classifying large windows 
    # AcuteStress.swift
    # At the moment all exercise = ModerateExercise
    # enum DetailedState: Int {
    #     case Unknown = -1
    #     case RestfulSleep = 0
    #     case LightSleep = 1
    #     case VeryRelaxed = 2
    #     case ModeratelyRelaxed = 3
    #     case ModeratelyStressed = 4
    #     case HighlyStressed = 5
    #     case ModerateExercise = 6
    #     case IntenseExercise = 7
    # }
    
    large_windows['classification'] = np.nan
##    large_windows.loc[large_windows['exercising_or_not'] == 1, 'classification'] = 6

    large_windows.loc[large_windows['awake_sleep'] != 2, 'classification'] = large_windows.loc[large_windows['awake_sleep'] != 2, 'awake_sleep']

    ## ##  TODO EXERCISE CLASSIFIER IS NOT WORKING, so all not sleep = acute
    
    large_windows.loc[large_windows['awake_sleep'] == 2, 'classification'] = large_windows.loc[large_windows['awake_sleep'] == 2, 'acute_score']

    return large_windows
