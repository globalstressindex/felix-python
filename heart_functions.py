# import math
# import statistics as stats

# from astropy.stats import LombScargle
import numpy as np

# from .utils import (
#   sample_standard_deviation,
#   time_offset_from_rr_intervals
# )
# from .single_analysis2 import SingleAnalysis2
# from models.insight_calc import InsightCalcType


# # http://astropy.readthedocs.io/en/latest/api/astropy.stats.LombScargle.html#astropy.stats.LombScargle
# def calc_lomb_scargle(rr_intervals, fc_options):
#     min_freq = fc_options.lomb_scargle__frequency_kwargs['min']
#     max_freq = fc_options.lomb_scargle__frequency_kwargs['max']
#     freq_steps = fc_options.lomb_scargle__frequency_kwargs['steps']
#     frequencies = np.linspace(min_freq, max_freq, freq_steps)
#     power = []

#     if rr_intervals:
#         # TODO use datetimes calculated from the original raw_rr_intervals otherwise
#         # we're messing up the data (we could probably just pass raw data to it!!)
#         times = time_offset_from_rr_intervals(rr_intervals)

#         max_t = max(times)
#         # We 0 base the filtered_times.  Not sure it's necessary but doing so just in case.
#         times_shifted = map(lambda t: t - max_t + fc_options.lomb_scargle__window_size_seconds, times)
#         filtered_times = list(filter(lambda x: x >= 0, times_shifted))

#         filtered_rr_intervals = rr_intervals[len(times) - len(filtered_times):]

#         try:
#             power = LombScargle(filtered_times, filtered_rr_intervals).power(
#               frequencies, method=fc_options.lomb_scargle__method, assume_regular_frequency=True)
#         except:
#             print("times ", times)
#             print("filtered_times ", filtered_times)
#             print("rr_intervals ", rr_intervals)
#             print("filtered_rr_intervals ", filtered_rr_intervals)
#             raise
#     return SingleAnalysis2(dict(
#         value={'frequencies': frequencies, 'power': power},
#         confidence=1.0,
#         type=InsightCalcType.lomb_scargle))


def calc_rmssd_old(rr_intervals):
    if len(rr_intervals) < 2:
        return 0
    s = 0.0  # (r[n] - r[n+1])^2
    total = 0.0  # t += s
    for i in range(len(rr_intervals) - 1):
        s = ((rr_intervals[i] - rr_intervals[i + 1])**2)
        total += s
    RMSSD = (total / (len(rr_intervals) - 1.0))**0.5
    return RMSSD


def calc_rmssd(rr_intervals):
    if len(rr_intervals) < 50:
        return np.nan    
    return np.sqrt((np.diff(rr_intervals)**2).mean())


def calc_ln_rmssd_old(rr_intervals):
    rmssd_calc = calc_rmssd(rr_intervals)
    # Calculate the natural log https://docs.python.org/3/library/math.html#math.log
    if rmssd_calc.value == 0:
        return SingleAnalysis2(dict(value=0, confidence=0, type=InsightCalcType.ln_rmssd))
    value = math.log(rmssd_calc.value)
    return SingleAnalysis2(dict(value=value, confidence=rmssd_calc.confidence, type=InsightCalcType.ln_rmssd))

#def calc_ln_rmssd(rr_intervals):

# def calc_sdnn(rr_intervals, fc_options=None):
#     if not rr_intervals:
#         return SingleAnalysis2(dict(value=0, confidence=0.0, type=InsightCalcType.sdnn))
#     value = sample_standard_deviation(rr_intervals)
#     return SingleAnalysis2(dict(value=value, confidence=1.0, type=InsightCalcType.sdnn))


def calc_rsd(data):             # TODO fix this borderline stuff
    if len(data) == 1:
        return np.nan
    return data.std(ddof=1)/data.mean()
    # if not rr_intervals:
    #     return SingleAnalysis2(dict(value=0, confidence=0.0, type=InsightCalcType.rsd))
    # value = calc_sdnn(rr_intervals).value / stats.mean(rr_intervals)
    # return SingleAnalysis2(dict(value=value, confidence=1.0, type=InsightCalcType.rsd))


# def calc_nn50(rr_intervals, fc_options=None):
#     count = 0
#     for i, rr in enumerate(rr_intervals[1:]):
#         if abs(rr - rr_intervals[i]) > 50:
#             count += 1
#     value = count
#     return SingleAnalysis2(dict(value=value, confidence=1.0, type=InsightCalcType.nn50))


# def calc_pnn50(rr_intervals, fc_options=None):
#     if not rr_intervals:
#         return SingleAnalysis2(dict(value=0.0, confidence=0.0, type=InsightCalcType.pnn50))
#     value = float(calc_nn50(rr_intervals)) / len(rr_intervals)
#     return SingleAnalysis2(dict(value=value, confidence=1.0, type=InsightCalcType.pnn50))


def calc_mean_hr(rr_intervals):
    return (60/rr_intervals).mean()

def calc_mean_hr_from_hrq(hrq_data):
    return hrq_data.mean()


# def calc_mean_hr_from_hrq(hrq_models):
#     if len(hrq_models) < 1:
#         return SingleAnalysis2(dict(value=0.0, confidence=0.0, type=InsightCalcType.hrq_mean_hr))

#     mean_hr = 0.0
#     for hrq_sample in hrq_models:
#         # Plus 5.0 because Travis anecdotally noticed it was reporting 5 bpm too low vs Polar
#         mean_hr = mean_hr + hrq_sample.heart_rate_bpm + 5.0

#     mean_hr = mean_hr / len(hrq_models)
#     # TODO maybe use the "locked" value to proxy confidence?
#     return SingleAnalysis2(dict(value=mean_hr, confidence=1.0, type=InsightCalcType.hrq_mean_hr))


# def calc_min_hr(rr_intervals, fc_options=None):
#     if not rr_intervals:
#         return SingleAnalysis2(dict(value=0.0, confidence=0.0, type=InsightCalcType.min_hr))
#     return SingleAnalysis2(dict(value=60.0 / min(rr_intervals), confidence=1.0, type=InsightCalcType.min_hr))


# def calc_max_hr(rr_intervals, fc_options=None):
#     if not rr_intervals:
#         return SingleAnalysis2(dict(value=0.0, confidence=0.0, type=InsightCalcType.max_hr))
#     return SingleAnalysis2(dict(value=60.0 / max(rr_intervals), confidence=1.0, type=InsightCalcType.max_hr))


# # def calc_elite_hrv_score_v01_core(rmssd_calc):
# #   # Magic values from hrv-calcs/i_explore_data/
# #   #    elite_hrv_scores03_try_to_recreate_their_hrv_score_from_their_intermediate_calcs.ipynb
# #   rmssd_a = 13.575025529999998
# #   rmssd_b = -5.75491229
# #   rmssd_c = 8.747051239999994

# #   log_me = (rmssd_calc.value * 1000) + rmssd_b
# #   if(log_me <= 0.0):
# #     return SingleAnalysis2(dict(value=0.0, confidence=0.0, type=InsightCalcType.))
# #   hrv_score_v01 = (rmssd_a * math.log(log_me)) + rmssd_c
# #   hrv_score_v01 = max(hrv_score_v01, 0)
# #   return SingleAnalysis2(dict(value=hrv_score_v01, confidence=rmssd_calc.confidence))


# # def calc_elite_hrv_score_v01(rr_intervals):
# #   if not rr_intervals:
# #     return SingleAnalysis2(dict(value=0.0, confidence=0.0, type=InsightCalcType.))
# #   rmssd_calc = calc_rmssd(rr_intervals)

# #   return calc_elite_hrv_score_v01_core(rmssd_calc)

# if __name__ == '__main__':
#     class AttributeDict(dict):
#         def __getattr__(self, attr):
#             return self[attr]

#         def __setattr__(self, attr, value):
#             self[attr] = value

#     fc_options = AttributeDict(dict(debug_info=False))
#     print(calc_br_v04(rr_intervals=[1010.0, 1020.0, 1010.0], fc_options=fc_options))
