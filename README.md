# README #

Python code and DS scripts

### Structure ###

* / contains the current python version of the app
* /research contain DS-related scripts

### Naming ###

* For ticket-related work (likely to go into /research) name your script DEV-XXX-some_info.py
