import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sb
from data_loading_pg import *
from options import Options
from large_windows import generate_large_windows
from acute_score import calculate_acute_score
from classifiers import classify_data, classifiers
from features import calculate_features

# read 48 hours of data and partition into historical and ofInterest

fc_options = Options()

## NOTHING
user = 7   #  James = 1, Travis = 2, Alex = 7, MT = 9 
time_from = pd.to_datetime('2016-12-11 16:00:00')
hours = pd.to_timedelta(0.9,'h')
# EXERCISE
# user = 9   #  James = 1, Travis = 2, Alex = 7, MT = 9 
# time_from = pd.to_datetime('2016-12-07 18:50:00')
# # SLEEP
# user = 7   #  James = 1, Travis = 2, Alex = 7, MT = 9 
# time_from = pd.to_datetime('2016-11-25 00:50:00')
# hours = pd.to_timedelta(0.9,'h')

# # # sleep
# user = 2   #  James = 1, Travis = 2, Alex = 7, MT = 9 
# time_from = pd.to_datetime('2017-05-22 09:00:00')


# hours = pd.to_timedelta(10,'h')
# window_size = pd.to_timedelta(fc_options.window_size_seconds,'s')
# #  when using sqlite dbs
# #database = '/home/gb/felix/data/me/071216/fix.sqlite'

hrquality, accelerometer, r2r, mood  = read_data(user,
                                                 time_from,
                                                 hours)

# partition the data into 3min windows

window_size_minutes = 5 #int(fc_options.window_size_seconds/60)

r2r['window'] = r2r.index.floor(str(window_size_minutes)+'T')
accelerometer['window'] = accelerometer.index.floor(str(window_size_minutes)+'T')
hrquality['window'] = hrquality.index.floor(str(window_size_minutes)+'T')


# filter data within each window, calculate window metrics, calculate global percentiles
windows = calculate_features(hrquality, accelerometer, r2r)

## classify sleep and exercise (DT clf)
classify_data(windows, classifiers)

# calculate acute stress score
calculate_acute_score(windows)

# generate large windows for
large_windows = generate_large_windows(windows)

large_windows['classification'].plot()
