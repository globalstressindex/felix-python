
import pandas  as pd 
from astropy.stats import LombScargle
from scipy.signal import lombscargle, welch
from scipy.fftpack import fft
import matplotlib.pyplot as plt
import numpy as np
from numpy.linalg import norm


def detrend(rr_data):
    """
    Detrending of RR data using a high-pass filter described in
    Tarvainen. "An advanced detrending method with application to HRV analysis"
    Probably can't run on irregular-spaced data, so resample first!
    Arguments:
    - `rr_window`:
    """
    N = len(rr_data)
    # D2 = np.eye(N-2,N)
    # D2[range(N-2),range(1,N-2+1)] = -2
    # D2[range(N-2),range(2,N-2+2)] = 1
    ### D2TD2 = np.dot(D2.T, D2)  ### a faster version is below - to save time on multiplication
    D2TD2 = np.eye(N)*6
    D2TD2[range(2,N),range(N-2)] = 1
    D2TD2[range(N-2),range(2,N)] = 1
    D2TD2[range(N-1),range(1,N)] = -4
    D2TD2[range(1,N),range(N-1)] = -4
    D2TD2[0,0]=1
    D2TD2[1,1]=5
    D2TD2[1,0]=-2
    D2TD2[0,1]=-2

    D2TD2[N-1,N-1]=1
    D2TD2[N-2,N-2]=5
    D2TD2[N-1,N-2]=-2
    D2TD2[N-2,N-1]=-2
    lam = 300   # as in the paper, but the default value in Kubios is 500
    L = np.eye(N) - np.linalg.inv(np.eye(N) + (lam**2)*D2TD2)   ## unfort., inverse is slow
    rr_dt = np.dot(L, rr_data)
    return rr_dt


def lfhf_welch(rr_window):
    """
    Gets a RR window from a groupby on the raw RR data
    Returns lf and hf - Power for low and high spectrum parts
    Uses Welch method to compute the PSD
    Arguments:
    - `rr_window`:
    """
    if len(rr_window) < 64:
        return pd.Series({'lf_w': np.nan, 'hf_w': np.nan, 'vlf_w': np.nan, 'tot_w': np.nan})
    rr_window.index = pd.to_timedelta(rr_window.rr_interval.cumsum(),'s')   # milliseconds
    rr_4hz = rr_window.resample('250L').mean()
    rr_4hz.index = rr_4hz.index.total_seconds()
    rr_inter = rr_4hz.interpolate(method='spline',order=3,s=0)
    y = rr_inter.rr_interval
#    print(norm(y-y.mean())**2/len(y))
#    y = detrend(rr_inter['rr_interval'])  # might also try welch's detrending instead 

    Fxx, Pxx = welch(y-y.mean(),fs=4,nperseg=256, window='hamming', detrend=False)
#    plt.plot(Fxx,Pxx)
    vlf = np.trapz(Pxx[(Fxx>=0.0033) & (Fxx<=0.04)],Fxx[(Fxx>=0.0033) & (Fxx<=0.04)])
    lf = np.trapz(Pxx[(Fxx>=0.04) & (Fxx<=0.15)],Fxx[(Fxx>=0.04) & (Fxx<=0.15)])
    hf = np.trapz(Pxx[(Fxx>=0.15) & (Fxx<=0.4)],Fxx[(Fxx>=0.15) & (Fxx<=0.4)])
    tot = np.trapz(Pxx,Fxx) # + y.mean()**2  - we remove the mean
#    print(Fxx[np.argmax(Pxx)])
    return pd.Series({'lf_w': lf, 'hf_w': hf, 'vlf_w': vlf, 'tot_w': tot})

    
def lfhf_ls(rr_window):
    """
    Gets a RR window from a groupby on the raw RR data
    Returns lf and hf - Power for low and high spectrum parts
    Uses Lomb-Scargle method to compute the PSD
    Arguments:
    - `rr_window`:
    """
    if len(rr_window) < 64:
        return pd.Series({'lf_ls': np.nan, 'hf_ls': np.nan, 'lfhf_ls': np.nan})
    x = (rr_window.index - rr_window.index.min()).total_seconds()
    y = np.array(rr_window['rr_interval'] - rr_window['rr_interval'].mean())  # no need to interpolate
    f = np.linspace(0.001, 3, 5000)   # radians 
    
    ls = lombscargle(x,y,f)

    lf = np.trapz(abs(np.sqrt(4*(ls/len(x)))[(f/(2*np.pi)>=0.04) & (f/(2*np.pi)<=0.15)]))
    hf = np.trapz(abs(np.sqrt(4*(ls/len(x)))[(f/(2*np.pi)>=0.15) & (f/(2*np.pi)<=0.4)]))    
    return pd.Series({'lf_ls': lf, 'hf_ls': hf, 'lfhf_ls': lf/hf})


def lfhf_ls_astro(rr_window):
    """
    Gets a RR window from a groupby on the raw RR data
    Returns lf and hf - Power for low and high spectrum parts
    Uses Lomb-Scargle from ASTROPY to compute the PSD
    Arguments:
    - `rr_window`:
    """
    if len(rr_window) < 64:
        return pd.Series({'lf_a': np.nan, 'hf_a': np.nan, 'vlf_a': np.nan, 'tot_a': np.nan})
#    x = rr_window['rr_interval'].cumsum()   # NOT using db timestamps as they are meaningless
    x = (rr_window.index - rr_window.index.min()).total_seconds()
    # y = np.array(rr_window['rr_interval'])
    y = np.array(rr_window['rr_interval'] - rr_window['rr_interval'].mean())  # no need to interpolate
#    plt.plot(x,y,marker='.')
    Fxx = np.arange(0.0001,0.4,1e-4)
#    Pxx = lombscargle(np.array(x),np.array(y),Fxx*2*np.pi)
    Pxx = LombScargle(x,y).power(Fxx, normalization='psd')
#    plt.plot(Fxx,Pxx)
    # print(sum(Pxx))
#    L = len(rr_window)
    Pxx = 2*Pxx*(x.max()/len(x))
    # print(sum(Pxx))

    vlf = np.trapz(Pxx[(Fxx>=0.0033) & (Fxx<=0.04)],Fxx[(Fxx>=0.0033) & (Fxx<=0.04)])
    lf = np.trapz(Pxx[(Fxx>=0.04) & (Fxx<=0.15)],Fxx[(Fxx>=0.04) & (Fxx<=0.15)])
    hf = np.trapz(Pxx[(Fxx>=0.15) & (Fxx<=0.4)],Fxx[(Fxx>=0.15) & (Fxx<=0.4)])
    tot = np.trapz(Pxx,Fxx)# + rr_window['rr_interval'].mean()**2

    return pd.Series({'lf_a': lf, 'hf_a': hf, 'vlf_a': vlf, 'tot_a': tot})
