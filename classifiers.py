###
#
#
# Sleep and exercise classification using pre-trained models
#
##

import json
from trained_models.trained_utils import make_own_lr_classifier, make_own_dt_classifier
from sklearn.preprocessing import PolynomialFeatures

classifiers = {'awake_sleep': '/home/gb/felix/repos/ios-hub/ios-hub/InsightCalcs/TrainedModels/sleepClassifierV04_LR--2017-06-02.json',
               'exercising_or_not': '/home/gb/felix/repos/ios-hub/ios-hub/InsightCalcs/TrainedModels/exerciseClassifierV04--2017-06-06.json',
               'NREM': '/home/gb/felix/repos/ios-hub/ios-hub/InsightCalcs/TrainedModels/sleep_NREM.json',
               'DEEP': '/home/gb/felix/repos/ios-hub/ios-hub/InsightCalcs/TrainedModels/sleep_DEEP.json'}


def get_classifier(json_file, clf_name):
    with open(json_file, 'r') as f:
        model_definition = json.load(f)                
        if clf_name in model_definition:
            model_definition = model_definition[clf_name]
        model_definition.pop('class_descriptions', None)
        if 'children_left' in model_definition:
                classifier = make_own_dt_classifier(**model_definition)
        else:
                classifier = make_own_lr_classifier(**model_definition) # everything is a LR now 130117 MT
        return classifier


def classify_data(windows, classifiers):
    ## break into two manual steps - processes are too different at this stage
    ## Exercise
    clf_name = 'exercising_or_not'
    json_file = classifiers[clf_name]
    clf = get_classifier(json_file, clf_name)
    ### get the rows with necessary features
    X_class = windows[['mean_hr', 'rmssd_milliseconds', 'sdnn', 'lf_a', 'hf_a']].dropna()    # TODO better way to pack this info into JSONs 130117 MT
    X_index = X_class.index
    # ### expand the basis as required 
    # poly = PolynomialFeatures(degree=2)
    # X_class = poly.fit_transform(X_class)[:,1:]    # skip the intercept
    ### classify windows - only those that have nonnull features
    windows.ix[X_index,clf_name] = clf.predict(X_class)

    ## Sleep
    clf_name = 'awake_sleep'
    json_file = classifiers[clf_name]
    clf = get_classifier(json_file, clf_name)
    ### get the rows with necessary features
    X_class = windows[['mean_hr', 'rmssd_milliseconds', 'sdnn', 'lf_a', 'hf_a', '3h_dist_sec']].dropna()
    X_index = X_class.index
    ### classify windows - only those that have nonnull features
    windows.ix[X_index,clf_name] = clf.predict(X_class)
    if not windows.loc[windows.awake_sleep == 1].empty:
        ## sleeping - check REM/NREM
        clf_name = 'NREM'
        json_file = classifiers[clf_name]
        clf = get_classifier(json_file, clf_name)
        X_class = windows.loc[windows.awake_sleep == 1, ['rmssd_milliseconds','sdnn','lf_a','hf_a']].dropna()
        X_index = X_class.index
        windows.ix[X_index,clf_name] = clf.predict(X_class)
        if not windows.loc[windows.NREM == 1].empty:
            ## NREM - check deep
            clf_name = 'DEEP'
            json_file = classifiers[clf_name]
            clf = get_classifier(json_file, clf_name)
            X_class = windows.loc[windows.NREM == 1, ['rmssd_milliseconds','sdnn','lf_a','hf_a']].dropna()
            X_index = X_class.index
            windows.ix[X_index,clf_name] = clf.predict(X_class)
        


