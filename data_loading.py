import pandas as pd
from numpy import floor, mod
import sqlite3

# Transform the dates from apple format
APPLE_DELTA = pd.datetime(year=2001, month=1, day=1, hour=0, second=0) - pd.to_datetime(0, unit='s')


def read_data(database_name, time_from, hours=48, window_size=180, snap_hour=True):
    """
    Reads N hours of hrq, accel, moodactivity, r2r  from a database
    """
    try:
        database_connection = sqlite3.connect(database_name)
    except:
        raise Exception('Database not found')
    return _read_data(
        database_connection=database_connection, time_from=time_from,
        hours=hours, window_size=window_size, snap_hour=snap_hour)



def _read_data(database_connection, time_from, hours, window_size, snap_hour):
    """
    Reads N hours of hrq, accel, moodactivity, r2r  from a database connection
    to be re-implemented in django/peewee/sqlalchemy when required

    (function signature takes database connection to ease its testing)
    """
    con = database_connection
    if snap_hour:
        time_from = snap_to_nearest_hour(time_from, window_size)
    zwhen = str((time_from - pd.Timedelta(hours, 'h') - pd.datetime(year=2001, month=1, day=1, hour=0, second=0)).total_seconds())
    # Read from the database
    hrquality = pd.read_sql('SELECT ZUSER,ZWHEN,ZHEARTRATE,ZLOCKED FROM ZMICROSOFTBANDHRQUALITY WHERE ZWHEN >= '+zwhen, con)
    accelerometer = pd.read_sql('SELECT ZUSER,ZWHEN,ZX,ZY,ZZ FROM ZMICROSOFTBANDACCELEROMETER WHERE ZWHEN >= '+zwhen, con)
    # distance = pd.read_sql('SELECT ZUSER,ZWHEN,ZDISTANCETODAY,ZMOTIONTYPE,ZTOTALDISTANCE,ZPACE,ZSPEED FROM ZMICROSOFTBANDDISTANCE',con)
    mood = pd.read_sql('SELECT ZUSER,ZWHEN,ZMOREFEELINGS1,ZMOREFEELINGS2,ZMOREFEELINGS3,ZSTRESS,ZSTARTTIME,ZENDTIME,ZACTIVITY,ZBODYPOSITION,ZMOOD,ZOTHERFEELINGS,ZRELAXINGHOW,ZRELAXINGOTHER,ZSLEEP,ZWORKINGINTENSITY FROM ZMOODACTIVITYSAMPLE WHERE ZWHEN >= '+zwhen, con)
    # TODO Z_ENT = 4 in r2r <=> MSBAND data only
    # r2r = pd.read_sql('SELECT ZUSER, ZWHEN, ZR2R FROM ZDEVICESAMPLE WHERE Z_ENT = 4 AND ZWHEN >= '+zwhen, con)
    r2r = pd.read_sql('SELECT ZUSER, ZWHEN, ZR2R FROM ZDEVICESAMPLE WHERE ZWHEN >= '+zwhen, con)
    contact = pd.read_sql('SELECT ZUSER, ZWHEN, ZSTATE FROM ZMICROSOFTBANDCONTACT WHERE ZWHEN >= '+zwhen, con)

    hrquality.ZWHEN = pd.to_datetime(hrquality.ZWHEN, unit='s') + APPLE_DELTA
    accelerometer.ZWHEN = pd.to_datetime(accelerometer.ZWHEN, unit='s') + APPLE_DELTA
    # distance.ZWHEN = pd.to_datetime(distance.ZWHEN, unit='s') + APPLE_DELTA
    r2r.ZWHEN = pd.to_datetime(r2r.ZWHEN, unit='s') + APPLE_DELTA
    contact.ZWHEN = pd.to_datetime(contact.ZWHEN, unit='s') + APPLE_DELTA
    mood.ZWHEN = pd.to_datetime(mood.ZWHEN, unit='s') + APPLE_DELTA
    # mood = mood.loc[mood.ZSTARTTIME != 'None']
    # mood.ZSTARTTIME = pd.to_datetime(mood.ZSTARTTIME, unit='s') + APPLE_DELTA
    # mood = mood.loc[mood.ZENDTIME != 'None']
    # mood.ZENDTIME = pd.to_datetime(mood.ZENDTIME, unit='s') + APPLE_DELTA

    hrquality.columns = ['user', 'recorded_timestamp', 'heartrate', 'locked']
    accelerometer.columns = ['user', 'recorded_timestamp', 'x', 'y', 'z']
    mood.columns = ['user',
                    'recorded_timestamp',
                    'level_overwhelmed',
                    'level_manageable',
                    'level_irritable',
                    'stress',
                    'starttime',
                    'endtime',
                    'activity',
                    'body_position',
                    'mood',
                    'other_feelings',
                    'relaxing_how',
                    'relaxing_other',
                    'sleep',
                    'working_intensity']
    r2r.columns = ['user', 'recorded_timestamp', 'rr_interval']
    contact.columns = ['user', 'recorded_timestamp', 'state']

    band_off_times = band_off_filter(contact)

    for band_off_window in band_off_times:
        start = band_off_window['start']
        end = band_off_window['end']
        r2r = r2r.loc[(r2r['recorded_timestamp'] < start) | (r2r['recorded_timestamp'] > end)]
        hrquality = hrquality.loc[(hrquality['recorded_timestamp'] < start) | (hrquality['recorded_timestamp'] > end)]
        accelerometer = accelerometer.loc[(accelerometer['recorded_timestamp'] < start) | (accelerometer['recorded_timestamp'] > end)]
        mood = mood.loc[(mood['recorded_timestamp'] < start) | (mood['recorded_timestamp'] > end)]

    return hrquality, accelerometer, mood, r2r, contact, time_from


def snap_to_nearest_hour(time_from, window_size):
    """
    Aligns the windows starting time, so that we get windows snapped to hour
    """
    current_hour = time_from.floor('h')
    delta_from_hour = time_from - time_from.floor('h')
    n_intervals = floor(delta_from_hour / window_size)
    if mod(delta_from_hour, window_size):       # if we are on the dot, do not need to add one more
        n_intervals += 1
    time_from = current_hour + n_intervals * window_size
    return time_from


def band_off_filter(contact):
    band_off_times = []
    band_off_pair = {}
    for row in contact.iterrows():
        if not band_off_pair:
            if row[1]['state'] == 0:
                band_off_pair['start'] = row[1]['recorded_timestamp']
        else:
            if row[1]['state'] == 1:
                band_off_pair['end'] = row[1]['recorded_timestamp']
                band_off_times.append(band_off_pair)
                band_off_pair = {}
    if band_off_pair:
        band_off_pair['end'] = pd.to_datetime('2099-07-16')
        band_off_times.append(band_off_pair)
    return band_off_times
