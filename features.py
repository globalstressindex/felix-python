import pandas as pd
from filtering import filter_rr_data
from accel_functions import calc_base_features, calc_accel_features, percentile_features, calc_percentile_features, calc_local_time_features, frequency_features, breathing_features

    
def calculate_features(hrquality, accelerometer, r2r):
    """
    Inputs: windows dataframe, data dataframes
    Filters the data and mutates the windows dataframe by adding columns with all window features
    """
    if len(r2r) > 0:                     # if r2r is not empty - filtrate
        ## filter each group individually, as this is what we do in C++
        r2r = r2r.groupby('window').apply(lambda x: filter_rr_data(x)).reset_index(level=0, drop=True)
    
    base_windows = calc_base_features(r2r, hrquality)
    freq_windows = frequency_features(r2r)
    br_windows = breathing_features(r2r)
    accel_windows = calc_accel_features(accelerometer)
    windows = base_windows.join(accel_windows,how='outer')
    windows = windows.join(freq_windows,how='outer')
    windows = windows.join(br_windows,how='outer')

    calc_local_time_features(windows)
    
    return windows






