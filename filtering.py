# from itertools import compress
import numpy as np
import pandas as pd
#from sklearn.linear_model import LogisticRegression
from scipy.signal import medfilt
from options import Options

#from breathing_rate import calc_br_v05
# from logistic_regression import LogRegClassifer


def filter_rr_data(data, threshold=0.25, adaptive=True):
    ## c++ doesn't filter if < 140 - TODO
    if len(data) < 140:
        return data
    mutate__filter_extreme_values(data)
    mean_filter(data) 
    return data

def mutate__filter_extreme_values(data):
    """
    remove extreme values
    """
    fc_options = Options()

    if not fc_options.filter_extreme_values:
        return

    MIN_HR_RR = 60.0 / fc_options.max_hr
    MAX_HR_RR = 60.0 / fc_options.min_hr
    
    data.loc[(data['rr_interval'] > MAX_HR_RR), 'rr_interval'] = np.nan  #
    ## remove leading and trailing NANs as interpolate extrapolates with last/first value, which c++ does not do and which is stupid
    data.drop(data.index[(data.index > data.rr_interval.last_valid_index()) | (data.index < data.rr_interval.first_valid_index()) ], inplace=True)

    data['rr_interval'] = data['rr_interval'].interpolate(method='time') # interpolate removed high values

    data.loc[(data['rr_interval'] < MIN_HR_RR), 'rr_interval'] = np.nan  # remove values
    data.dropna(inplace=True)   # get rid of minimal points - do not affect results too much


def mean_filter(data):
    """
    checks if the center of a window differs from the local average by more than a threshold
    removes the point and interpolates using cubic splines
    Arguments:
    - `data`:  DF with a 'rr_interval' column

    Gari Clifford suggests that we remove but don't interpolate, but that requires accurate timestamps
    and can affect the metrics (see Kim et al. papers)
    """
    i=0
    while i<=0:                 # XXX: JUST DO ONE ITERATION AS IN CPP
        WINLENGTH = 41   # window width
        ### scipy pads boundaries with 0
        ### in c++ we pad boundaries with the first element
        data_padded = pd.concat([pd.concat([data.iloc[:1]]*(WINLENGTH//2)), data, pd.concat([data.iloc[-1:]]*(WINLENGTH//2))])
        ## compute medfilt of padded and remove the padding, i.e. reduce back to original length. UGLY!
        data['rr_med'] = medfilt(data_padded['rr_interval'], WINLENGTH)[WINLENGTH//2:-WINLENGTH//2+1]   # apply a median filter first to get a smoothed local average 
        data['mean'] = data['rr_med'].rolling(WINLENGTH, center=True, min_periods=1).mean()  # REMOVE , center=True for CPP
        data['threshold'] = 3*data['rr_interval'].rolling(WINLENGTH, center=True, min_periods=1).std()
        data['diff'] = (data['rr_interval'] - data['mean']).abs()
        data.loc[data['diff'] > data['threshold'], 'rr_interval'] = np.nan  # remove the values that stand out too much
        if data['rr_interval'].isnull().values.any():
            data['rr_interval'] = data['rr_interval'].interpolate(method='time') # now fill the gaps using linear interpolation - CUBIC produces negatives! TODO
            i += 1
        else:
            break
#    data.dropna(inplace=True)   # get rid of nans
    data.drop('rr_med',axis=1,inplace=True)
    data.drop('mean',axis=1,inplace=True)
    data.drop('threshold',axis=1,inplace=True)
    data.drop('diff',axis=1,inplace=True)



def adaptive_filter(data):
    """
    adaptive filtering from
    Logier et al. An efficient algorithm for R-R intervals series filtering 2004
    """
    ## build first window of valid points n=20
    win_end = 20
    win_start = win_end - 20
    int_ix = win_end # interpolation marker - marks the end of the FAIL sequence
    ch_max = len(data) - 1
    for ch_ix in range(21, len(data) ):
        window = data.iloc[win_start:win_end]
        win_mean = window.rr_interval.mean()
        win_std = window.rr_interval.std()
        if int_ix > win_end + 20:
            print ('OKFAIL',ch_ix, win_end, int_ix)            
            win_end = ch_ix
            win_start = win_end - 20
            int_ix = win_end
            continue
        if np.abs(data.iloc[ch_ix].rr_interval - win_mean) <= 2*win_std:
            print('OK',ch_ix, win_end, int_ix)
            if int_ix > win_end:
                ### if there were FAILS - interpolate
                print ('IN',ch_ix, win_end, int_ix)
                data.iloc[win_end+1:int_ix+1] = np.nan  # remove all failed
                data.interpolate(method='time',inplace=True)  # include window end and the first OK element
            #### All uncertain elements become "OK" => move window accordingly
            win_end = ch_ix
            win_start = win_end - 20
            int_ix = win_end
            continue ## increments ch_ix by one and moves forward
        else:
            if ch_ix < ch_max:
                if (data.iloc[ch_ix].rr_interval < win_mean - 2*win_std) and (data.iloc[ch_ix + 1].rr_interval > win_mean - 2*win_std):
                    print('C1',ch_ix, win_end, int_ix)## FAIL
                    int_ix = ch_ix # move interpolate pointer to the current element
                    continue  # do not bother doing other checks
                if (data.iloc[ch_ix].rr_interval < 0.75*data.iloc[ch_ix - 1].rr_interval) or (data.iloc[ch_ix + 1].rr_interval < 0.75*data.iloc[ch_ix - 1].rr_interval):
                    print('C2',ch_ix, win_end, int_ix)## FAIL
                    int_ix = ch_ix
                    continue  # do not bother doing other checks
            if data.iloc[ch_ix].rr_interval > 1.75*data.iloc[ch_ix - 1].rr_interval:
                print('C3',ch_ix, win_end, int_ix)## FAIL
                int_ix = ch_ix
                continue            
            # otherwise UNCERTAIN - move the ch_ix but not window or interpolation marker
            else:
                print ('UN',ch_ix, win_end, int_ix)
                if ch_ix>win_end + 5:
                    print ('OKUN',ch_ix, win_end, int_ix)
                    win_end = ch_ix
                    win_start = win_end - 20
                    int_ix = win_end
