import pandas as pd
from data_loading_pg import *
from features import calculate_features
from sqlalchemy import create_engine
from options import Options

# read 48 hours of data and partition into historical and ofInterest

fc_options = Options()


hours = pd.to_timedelta(24,'h')
window_size_minutes = int(fc_options.window_size_seconds/60)

for user in [7]:
    time_from = pd.to_datetime('2016-11-25 00:00:00')
    while time_from <= pd.to_datetime('2017-03-07 00:00:00'):   #####   <=  !!!!!
## read from the database (PG)
        print(user,time_from)
        hrquality, accelerometer, r2r, mood  = read_data(user,
                                                         time_from,
                                                         hours)
        time_from = time_from + hours # move to the next window
        print (len(r2r),len(accelerometer),len(hrquality))
        if not (len(r2r) | len(accelerometer) | len(hrquality)):
            continue
    
        ## partition the data into 3min windows
        
        r2r['window'] = r2r.index.floor(str(window_size_minutes)+'T')
        accelerometer['window'] = accelerometer.index.floor(str(window_size_minutes)+'T')
        hrquality['window'] = hrquality.index.floor(str(window_size_minutes)+'T')


## filter data within each window, calculate window metrics, calculate global percentiles
        windows = calculate_features(hrquality,accelerometer,r2r)
        windows.insert(0,'zuser',user)

##### Save windows to the database
        if len(windows)>0:
            try:
                con = create_engine('postgresql:///felix')
            except:
                raise Exception('Database not found')
            else:
                windows.to_sql('zwindows_alex', con=con, if_exists='append', index=True, index_label='zwhen')
    
