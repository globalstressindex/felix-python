from . import trained_utils


def get_classifier_v01():
    v01_definition = trained_utils.load_model_definition('exerciseClassifierV01')
    exercise_definition = v01_definition['exercising_or_not']
    classifier_v01 = trained_utils.make_own_dt_classifier_from_json(
        dt_json_definition=exercise_definition)

    return classifier_v01
