from . import trained_utils
from utils.collections import (
    consensus_by_neighbours, chunk_avg, chunk_mode, makeIgnorer)


class CompositeSleepTree():
    def __init__(self, dt_classifier_awake_sleep, dt_classifier_light_or_restful, chunk_size):
        self.dt_classifier_awake_sleep = dt_classifier_awake_sleep
        self.dt_classifier_light_or_restful = dt_classifier_light_or_restful
        self.chunk_size = chunk_size

    def predict(self, input_data):
        # To test and implement in Python.  Copied from swift ios-hub repo
        # if inputData.windows.count == 0 {
        #     return
        # }
        # if inputData.windows.count % self.chunkSize != 0 {
        #     throw SleepClassifierError.windowNumberNotMultipleOfChunkSize(acceptableChunkSize: self.chunkSize)
        # }

        predictions_awake_sleep = self.dt_classifier_awake_sleep.predict(input_data)
        predictions_light_or_restful = self.dt_classifier_light_or_restful.predict(input_data)

        predictions_awake_light_or_restful = []
        for i, p in enumerate(predictions_light_or_restful):
            predictions_awake_light_or_restful.append(2 if predictions_awake_sleep[i] == 2 else p)

        ignoreValues = makeIgnorer([-1])
        predictions_awake_light_or_restful_avg = chunk_avg(predictions_awake_light_or_restful, ignoreValues=ignoreValues, chunk=self.chunk_size)
        predictions_awake_light_or_restful_mode = chunk_mode(predictions_awake_light_or_restful, ignoreValues=ignoreValues, chunk=self.chunk_size)

        # Hack to smooth predictions
        predictions_smoothed = consensus_by_neighbours(predictions_awake_light_or_restful, ignoreValues=ignoreValues)
        predictions_smoothed_and_chunk_avg = chunk_avg(predictions_smoothed, ignoreValues=ignoreValues, chunk=self.chunk_size)
        predictions_smoothed_and_chunk_mode = chunk_mode(predictions_smoothed, ignoreValues=ignoreValues, chunk=self.chunk_size)
        return {
            'predictions_awake_sleep': predictions_awake_sleep,
            'predictions_light_or_restful': predictions_light_or_restful,
            'predictions_awake_light_or_restful': predictions_awake_light_or_restful,
            'predictions_awake_light_or_restful_avg': predictions_awake_light_or_restful_avg,
            'predictions_awake_light_or_restful_mode': predictions_awake_light_or_restful_mode,
            'predictions_smoothed': predictions_smoothed,
            'predictions_smoothed_and_chunk_avg': predictions_smoothed_and_chunk_avg,
            'predictions_smoothed_and_chunk_mode': predictions_smoothed_and_chunk_mode,
        }


_classifier_v01 = None


def get_classifier_v01(chunk_size=5, bust_cache=False):
    global _classifier_v01
    if _classifier_v01 is None or bust_cache:
        v01_definition = trained_utils.load_model_definition('sleepClassifierV01')
        awake_sleep_definition = v01_definition["awake_sleep"]
        dt_classifier_awake_sleep_v01 = trained_utils.make_own_dt_classifier_from_json(
            dt_json_definition=awake_sleep_definition)

        light_or_restful_definition = v01_definition["light_or_restful_sleep"]
        dt_classifier_light_or_restful_v01 = trained_utils.make_own_dt_classifier_from_json(
            dt_json_definition=light_or_restful_definition)

        _classifier_v01 = CompositeSleepTree(
            dt_classifier_awake_sleep=dt_classifier_awake_sleep_v01,
            dt_classifier_light_or_restful=dt_classifier_light_or_restful_v01,
            chunk_size=chunk_size,
        )

    return _classifier_v01
