from copy import copy
import os
import json
from sklearn.linear_model import LogisticRegression

import numpy as np

#import data
# abs_path = os.path.dirname(data.__file__)


def load_model_definition(model_definition_file_name):
    path = abs_path + '/swift/ios-hub/ios-hub/InsightCalcs/TrainedModels/'
    with open(path + model_definition_file_name + '.json', 'r') as f:
        json_data = json.load(f)
    return json_data


def make_own_lr_classifier(feature_strings, coefficients, intercept, classes):
    """
    import LR parameters from a json
    """
    clf = LogisticRegression()
    clf.coef_ = np.array(coefficients).reshape(1,len(coefficients))
    clf.intercept_ = intercept
    clf.classes_ = np.array(classes)
    return clf


def make_own_dt_classifier(children_left, children_right, feature_ints, thresholds, values, classes, feature_strings):
    class T:
        def predict(self, input_data, features_description=[]):
            # TODO, in swift we map the indicies from feature_strings to features_description,
            # here we just use pandas for the moment to filter the columns of the input_data to contain only the
            # features we need / are expecting

            predictions = []
            for index, row in input_data.iterrows():
                node = 0
                confident = True
                while children_left[node] != -1:
                    featureInt = feature_ints[node]
                    feature_string = feature_strings[featureInt]

                    # Check to see if we're still confident about classifying this row
                    # if enable_minus_1_no_confidence_class:
                    #     feature_confidence_string = feature_strings[featureInt] + "_confidence"
                    #     try:
                    #         confident = row[feature_confidence_string] != 0.0
                    #     except:
                    #         pass
                    #     if not confident:
                    #         break

                    if row[feature_string] <= thresholds[node]:
                        node = children_left[node]
                    else:
                        node = children_right[node]

                if confident:
                    node_values = values[node][0]
                    node_max_values_idx = np.argmax(node_values)
                    predictions.append(classes[node_max_values_idx])
                else:
                    predictions.append(-1)
            return predictions
    return T()


def make_own_dt_classifier_from_json(dt_json_definition):
    dt_json_definition = copy(dt_json_definition)
    dt_json_definition.pop('class_descriptions', None)
    return make_own_dt_classifier(**dt_json_definition)
