# import statistics as stats

import numpy as np
import json
from lxml import etree
from lxml.etree import xmlfile

# +++++++++++++++++++++++++ SUPPORTING STATS FUNCTIONS ++++++++++++++++++++++++

def sample_standard_deviation(values):
    return np.std(values, ddof=1)


def population_standard_deviation(values):
    return np.std(values)



class DT_writer(object):
    """
    converts a DT to swift-friendly json or mlpack-friendly XML
    """    
    def __init__(self, clf=None, fname=None):
        """
        reads a sklearn tree or a previously saved json
        """
        if clf:
            self.children_left = clf.tree_.children_left.tolist()
            self.children_right = clf.tree_.children_right.tolist()
            self.feature_ints = clf.tree_.feature.tolist()
            self.thresholds = clf.tree_.threshold.tolist()
            self.values = clf.tree_.value.tolist()
            self.classes = clf.classes_.tolist()
            self.feature_strings=[]
            self.class_descriptions=[]
            self.n_nodes = clf.tree_.node_count
        elif fname:
            with open(fname, 'r') as f:                
                model_definition = json.load(f)
                for key in model_definition:
                    setattr(self, key, model_definition[key])
                self.n_nodes = len(self.children_left)
                
    def save_json(self, fname):
        """
        saves to file
        args: filename
        """
        with open(fname,'w') as outfile: json.dump(self.__dict__,outfile,sort_keys=True)
            
    def save_xml(self, fname):
        """
        Writes out an mlpack-compatible XML
        """
        # The tree structure can be traversed to compute various properties such
        # as the depth of each node and whether or not it is a leaf.
        nxml = {}
        class1 = False
        node_depth = np.zeros(shape=self.n_nodes)
        is_leaves = np.zeros(shape=self.n_nodes, dtype=bool)
        stack = [(0, -1)]  # seed is the root node id and its parent depth
        root = etree.Element('boost_serialization', signature="serialization::archive", version="14")
        nxml[0] = etree.SubElement(root,"dttest",class_id="0",tracking_level="0",version="0")
        while len(stack) > 0:
            node_id, parent_depth = stack.pop()
            node_depth[node_id] = parent_depth + 1
            #
            # If we have a test node
            if (self.children_left[node_id] != self.children_right[node_id]):
                stack.append((self.children_left[node_id], parent_depth + 1))
                stack.append((self.children_right[node_id], parent_depth + 1))
            else:
                is_leaves[node_id] = True

        for i in range(self.n_nodes):
            if is_leaves[i]:
                etree.SubElement(nxml[i], "numChildren").text = "0"
                etree.SubElement(nxml[i], "splitDimension").text = "999999999999999"
                etree.SubElement(nxml[i], "dimensionTypeOrMajorityClass").text = str(np.argmax(self.values[i][0]))
                # if i in self.children_right:
                #     etree.SubElement(nxml[i], "dimensionTypeOrMajorityClass").text = "1"
                # else:
                #     etree.SubElement(nxml[i], "dimensionTypeOrMajorityClass").text = "0"
                if not class1:    
                    cp = etree.SubElement(nxml[i], "classProbabilities",class_id="1",tracking_level="0",version="0")
                    class1 = True
                else:
                    cp = etree.SubElement(nxml[i], "classProbabilities")
                etree.SubElement(cp, "n_rows").text="2" 
                etree.SubElement(cp, "n_cols").text="1" 
                etree.SubElement(cp, "n_elem").text="2" 
                etree.SubElement(cp, "vec_state").text="1"
                if i in self.children_right:
                    etree.SubElement(cp, "item").text="0.0"
                    etree.SubElement(cp, "item").text="1.0"
                else:
                    etree.SubElement(cp, "item").text="1.0"
                    etree.SubElement(cp, "item").text="0.0"
                        
            else:
                etree.SubElement(nxml[i], "numChildren").text = "2"
                nxml[self.children_left[i]] = etree.SubElement(nxml[i], "child0")
                nxml[self.children_right[i]] = etree.SubElement(nxml[i], "child1")
                etree.SubElement(nxml[i], "splitDimension").text = str(self.feature_ints[i])
                etree.SubElement(nxml[i], "dimensionTypeOrMajorityClass").text = str(0)
                cp = etree.SubElement(nxml[i], "classProbabilities")
                etree.SubElement(cp, "n_rows").text="1" 
                etree.SubElement(cp, "n_cols").text="1" 
                etree.SubElement(cp, "n_elem").text="1" 
                etree.SubElement(cp, "vec_state").text="1"
                etree.SubElement(cp, "item").text=str(self.thresholds[i])

        with open(fname, 'w') as xf:
            xf.write(etree.tostring(root, pretty_print=True, doctype='<!DOCTYPE boost_serialization>', encoding='unicode', standalone="yes").replace('\'','\"').replace('encoding=""','encoding="utf-8"'))
        
     
        

class LR_writer(object):
    """
    converts a LR to swift-friendly json or mlpack-friendly XML
    """    
    def __init__(self, clf, class_descriptions, feature_strings):
        self.coef = np.r_[clf.intercept_, np.ravel(clf.coef_)].tolist()  # concat
        self.coefficients = np.ravel(clf.coef_).tolist()
        self.intercept = clf.intercept_[0]
        self.classes = clf.classes_.tolist()
        self.class_descriptions = class_descriptions
        self.feature_strings = feature_strings
        if 'C' in clf.__dict__:
            # LR
            self.lmbda = (1/clf.C)
        elif 'C_' in clf.__dict__:
            # LRCV
            self.lmbda = (1/clf.C_)[0]
        else:
            raise "Wrong parameter"


    def save_json(self, fname, clf_name=None):
        """
        saves to json, pass clf name as a second parameter if necessary (swift legacy)
        """
        json_keys = {x:self.__dict__[x] for x in ['feature_strings', 'coefficients', 'intercept', 'classes','class_descriptions']}
        if clf_name:
            json_keys = {clf_name: json_keys}
        with open(fname,'w') as outfile: json.dump(json_keys,outfile,sort_keys=True)


    def save_xml(self, fname):
        """
        saves to mlpack xml
        """
        root = etree.Element('boost_serialization', signature="serialization::archive", version="14")
        lr = etree.SubElement(root,"lrtest",class_id="0",tracking_level="0",version="0")
        par = etree.SubElement(lr,"parameters",class_id="1",tracking_level="0",version="0")
        etree.SubElement(par,"n_rows").text = str(len(self.coef))
        etree.SubElement(par,"n_cols").text = "1"
        etree.SubElement(par,"n_elem").text = str(len(self.coef))
        etree.SubElement(par,"vec_state").text = "1"
        for wi in self.coef:
            etree.SubElement(par,"item").text = str(wi)
        etree.SubElement(lr,"lambda").text = str(self.lmbda)
        with open(fname, 'w') as xf:
            xf.write(etree.tostring(root, pretty_print=True, doctype='<!DOCTYPE boost_serialization>', encoding='unicode', standalone="yes").replace('\'','\"').replace('encoding=""','encoding="utf-8"'))


        
        
            
        
            
