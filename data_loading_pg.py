import pandas as pd
from numpy import floor, mod, inf
from sqlalchemy import create_engine


# Transform the dates from apple format
APPLE_DELTA = pd.datetime(year=2001, month=1, day=1, hour=0, second=0) - pd.to_datetime(0,unit='s') 


def create_connection():
    ### create postgres connection or die
    try:
        con = create_engine('postgresql:///felix')
    except:
        raise Exception('Database not found')
    else:
        return con

def gen_time_preds(time_from, hours):
    ### Converts time predicates to the epoch format suitable for db requests
    zfrom = str((time_from - pd.Timedelta(hours,'h') - pd.datetime(year=2001, month=1, day=1, hour=0, second=0)).total_seconds())
    zto = str((time_from - pd.datetime(year=2001, month=1, day=1, hour=0, second=0)).total_seconds())
    return zfrom, zto

def gen_band_off_times(time_from, hours, user, con):
    ### provides a list of intervals when the band was not worn
    zfrom, zto = gen_time_preds(time_from, hours)
    contact = pd.read_sql ('SELECT ZUSER, ZWHEN, ZSTATE FROM ZMICROSOFTBANDCONTACT WHERE ZWHEN >= %(zfrom)s AND ZWHEN < %(zto)s AND ZUSER = %(zuser)s ORDER BY ZWHEN',con=con, params={'zfrom':zfrom, 'zto':zto, 'zuser':user})
    contact.zwhen = pd.to_datetime(contact.zwhen,unit='s') + APPLE_DELTA
    contact.columns = ['user', 'recorded_timestamp', 'state']
    band_off_times = band_off_filter(contact)
    return band_off_times
    
def remove_band_off_rows(data, band_off_times):
    ### removes rows when the band was not worn
    for band_off_window in band_off_times:
        data = data.loc[(data['recorded_timestamp'] < band_off_window['start']) | (data['recorded_timestamp'] > band_off_window['end'])]    
    
def localize_time(data, user, con):
    ### set time as index and convert to local time
    user_tz_df = pd.read_sql('user_tz',con)
    user_tz = user_tz_df.loc[user_tz_df['user'] == user,'timezone'].values[0]
    data.set_index('recorded_timestamp',inplace=True)
    data.index = data.index.tz_localize('UTC').tz_convert(user_tz).tz_localize(None)
    
        
def read_r2r(user, time_from, hours=pd.to_timedelta(24,'h'), con=None, band_off_times=None):
    """
    Reads "hours" hours BACK from time_from of r2r from a postgres database
    """
    if not con:
        con = create_connection()

    zfrom, zto = gen_time_preds(time_from, hours)
    r2r = pd.read_sql ('SELECT ZWHEN, ZR2R FROM ZDEVICESAMPLE WHERE Z_ENT = 5 AND ZWHEN >= %(zfrom)s AND ZWHEN < %(zto)s AND ZUSER = %(zuser)s ORDER BY ZWHEN',con=con, params={'zfrom':zfrom, 'zto':zto, 'zuser':user})
    r2r.zwhen = pd.to_datetime(r2r.zwhen,unit='s') + APPLE_DELTA
    r2r.columns = ['recorded_timestamp','rr_interval']

    if not band_off_times:
        band_off_times = gen_band_off_times(time_from, hours, user, con)
    remove_band_off_rows(r2r, band_off_times)

    localize_time(r2r, user, con)

    return r2r


def read_r2r_polar(user, time_from, hours=pd.to_timedelta(24,'h'), con=None, band_off_times=None):
    """
    Reads "hours" hours BACK of POLAR DATA from time_from of r2r from a postgres database
    """
    if not con:
        con = create_connection()

    zfrom, zto = gen_time_preds(time_from, hours)
    r2r = pd.read_sql ('SELECT ZWHEN, ZR2R FROM ZDEVICESAMPLE WHERE Z_ENT = 4 AND ZWHEN >= %(zfrom)s AND ZWHEN < %(zto)s AND ZUSER = %(zuser)s ORDER BY ZWHEN',con=con, params={'zfrom':zfrom, 'zto':zto, 'zuser':user})
    r2r.zwhen = pd.to_datetime(r2r.zwhen,unit='s') + APPLE_DELTA
    r2r.columns = ['recorded_timestamp','rr_interval']

    if not band_off_times:
        band_off_times = gen_band_off_times(time_from, hours, user, con)
    remove_band_off_rows(r2r, band_off_times)

    localize_time(r2r, user, con)

    return r2r

def read_r2r_scosche(user, time_from, hours=pd.to_timedelta(24,'h'), con=None, band_off_times=None):
    """
    Reads "hours" hours BACK of POLAR DATA from time_from of r2r from a postgres database
    """
    if not con:
        con = create_connection()

    zfrom, zto = gen_time_preds(time_from, hours)
    r2r = pd.read_sql ('SELECT ZWHEN, ZR2R FROM ZDEVICESAMPLE WHERE Z_ENT = 6 AND ZWHEN >= %(zfrom)s AND ZWHEN < %(zto)s AND ZUSER = %(zuser)s ORDER BY ZWHEN',con=con, params={'zfrom':zfrom, 'zto':zto, 'zuser':user})
    r2r.zwhen = pd.to_datetime(r2r.zwhen,unit='s') + APPLE_DELTA
    r2r.columns = ['recorded_timestamp','rr_interval']

    if not band_off_times:
        band_off_times = gen_band_off_times(time_from, hours, user, con)
    remove_band_off_rows(r2r, band_off_times)

    localize_time(r2r, user, con)

    return r2r

def read_r2r_garmin(user, time_from, hours=pd.to_timedelta(24,'h'), con=None, band_off_times=None):
    """
    Reads "hours" hours BACK of GARMIN DATA (z_ent = 8) from time_from of r2r from a postgres database
    """
    if not con:
        con = create_connection()

    zfrom, zto = gen_time_preds(time_from, hours)
    r2r = pd.read_sql ('SELECT ZWHEN, ZR2R FROM ZDEVICESAMPLE WHERE Z_ENT = 8 AND ZWHEN >= %(zfrom)s AND ZWHEN < %(zto)s AND ZUSER = %(zuser)s ORDER BY ZWHEN',con=con, params={'zfrom':zfrom, 'zto':zto, 'zuser':user})
    r2r.zwhen = pd.to_datetime(r2r.zwhen,unit='s') + APPLE_DELTA
    r2r.columns = ['recorded_timestamp','rr_interval']

    if not band_off_times:
        band_off_times = gen_band_off_times(time_from, hours, user, con)
    remove_band_off_rows(r2r, band_off_times)

    localize_time(r2r, user, con)

    return r2r


def write_r2r_garmin(user, r2r, con=None):
    """
    Saves Garmin data in the format (index, rr_interval) to postgres
    """
    if not con:
        con = create_connection()

    r2r.index = (r2r.index - pd.datetime(year=2001, month=1, day=1, hour=0, second=0)).total_seconds()
    r2r['z_ent'] = 8  # add entity column
    r2r['zuser'] = user
    r2r.columns = ['zr2r', 'z_ent', 'zuser']
    r2r.to_sql('zdevicesample_stg', con=con, if_exists='append', index=True, index_label='zwhen')
    print("INSERT INTO zdevicesample SELECT * FROM zdevicesample_stg ON CONFLICT (zuser, zwhen, z_ent) DO NOTHING;")


def read_window_clf(user, time_from, hours=pd.to_timedelta(24,'h'), con=None, band_off_times=None):
    """
    Reads "hours" hours BACK from time_from of WINDOWS CLASSIFICATIONS from a postgres database
    """
    if not con:
        con = create_connection()

    zfrom, zto = gen_time_preds(time_from, hours)
    window_clf = pd.read_sql ('SELECT ZWHEN, ZEXERCISEPREDICTION, ZSLEEPPREDICTION, ZACUTESTRESSSCOREPREDICTION FROM ZWINDOW_CLASSIFICATION WHERE ZWHEN >= %(zfrom)s AND ZWHEN < %(zto)s AND ZUSER = %(zuser)s ORDER BY ZWHEN',con=con, params={'zfrom':zfrom, 'zto':zto, 'zuser':user})
    window_clf.zwhen = pd.to_datetime(window_clf.zwhen,unit='s') + APPLE_DELTA
    window_clf.columns = ['recorded_timestamp','exercising','sleep','stress_score']

    localize_time(window_clf, user, con)

    return window_clf




def read_accelerometer(user, time_from, hours=pd.to_timedelta(24,'h'), con=None, band_off_times=None):
    """
    Reads "hours" hours BACK from time_from of accel data from a postgres database
    """
    if not con:
        con = create_connection()
        
    zfrom, zto = gen_time_preds(time_from, hours)
    accelerometer = pd.read_sql('SELECT ZUSER,ZWHEN,ZX,ZY,ZZ FROM ZMICROSOFTBANDACCELEROMETER WHERE ZWHEN >= %(zfrom)s AND ZWHEN < %(zto)s AND ZUSER = %(zuser)s  ORDER BY ZWHEN',con=con, params={'zfrom':zfrom, 'zto':zto, 'zuser':user})
    accelerometer.zwhen = pd.to_datetime(accelerometer.zwhen,unit='s') + APPLE_DELTA
    accelerometer.columns = ['user', 'recorded_timestamp', 'x', 'y', 'z']        
    
    if not band_off_times:
        band_off_times = gen_band_off_times(time_from, hours, user, con)
    remove_band_off_rows(accelerometer, band_off_times)

    localize_time(accelerometer, user, con)

    return accelerometer

def read_hrquality(user, time_from, hours=pd.to_timedelta(24,'h'), con=None, band_off_times=None):
    """
    Reads "hours" hours BACK from time_from of HRQ data from a postgres database
    """
    if not con:
        con = create_connection()
        
    zfrom, zto = gen_time_preds(time_from, hours)
    hrquality = pd.read_sql('SELECT ZUSER,ZWHEN,ZHEARTRATE,ZLOCKED FROM ZMICROSOFTBANDHRQUALITY WHERE ZWHEN >= %(zfrom)s AND ZWHEN < %(zto)s AND ZUSER = %(zuser)s ORDER BY ZWHEN',con=con, params={'zfrom':zfrom, 'zto':zto, 'zuser':user})
    hrquality.zwhen = pd.to_datetime(hrquality.zwhen,unit='s') + APPLE_DELTA
    hrquality.columns = ['user','recorded_timestamp','heartrate','locked']

    if not band_off_times:
        band_off_times = gen_band_off_times(time_from, hours, user, con)
    remove_band_off_rows(hrquality, band_off_times)

    localize_time(hrquality, user, con)

    return hrquality

def read_mood(user, time_from, hours=pd.to_timedelta(24,'h'), con=None, band_off_times=None):
    """
    Reads "hours" hours BACK from time_from of questionnaire data from a postgres database
    """
    if not con:
        con = create_connection()
        
    zfrom, zto = gen_time_preds(time_from, hours)
    mood = pd.read_sql('SELECT ZUSER,ZWHEN,ZMOREFEELINGS1,ZMOREFEELINGS2,ZMOREFEELINGS3,ZSTRESS,ZSTARTTIME,ZENDTIME,ZACTIVITY,ZBODYPOSITION,ZMOOD,ZOTHERFEELINGS,ZRELAXINGHOW,ZRELAXINGOTHER,ZSLEEP,ZWORKINGINTENSITY FROM ZMOODACTIVITYSAMPLE WHERE ZWHEN >= %(zfrom)s AND ZWHEN < %(zto)s AND ZUSER = %(zuser)s  ORDER BY ZWHEN',con=con, params={'zfrom':zfrom, 'zto':zto, 'zuser':user})
    mood.zwhen = pd.to_datetime(mood.zwhen,unit='s') + APPLE_DELTA
    mood.columns = ['user',
                    'recorded_timestamp',
                    'level_overwhelmed',
                    'level_manageable',
                    'level_irritable',
                    'stress',
                    'starttime',
                    'endtime',
                    'activity',
                    'body_position',
                    'mood',
                    'other_feelings',
                    'relaxing_how',
                    'relaxing_other',
                    'sleep',
                    'working_intensity']
    
    localize_time(mood, user, con)

    return mood

def read_mood2(user, time_from, hours=pd.to_timedelta(24,'h'), con=None, band_off_times=None):
    """
    Reads "hours" hours BACK from time_from of new questionnaire data from a postgres database
    """
    if not con:
        con = create_connection()
        
    zfrom, zto = gen_time_preds(time_from, hours)
    mood2 = pd.read_sql('SELECT ZUSER, ZWHEN, ZEMOTIONALACTIVITY, ZEMOTIONALOVERLOAD, ZHEADNECK, ZIRRITABLE, ZMANAGING, ZMENTALACTIVITY, ZPHYSICALACTIVITY, ZSTRESS,  ZACTIVITY, ZALCOHOL, ZBODYPOSITION, ZCAFFINE, ZMOOD, ZOTHERFEELINGS, ZSLEEP FROM ZMOODACTIVITYSAMPLEV2 WHERE ZWHEN >= %(zfrom)s AND ZWHEN < %(zto)s AND ZUSER = %(zuser)s  ORDER BY ZWHEN',con=con, params={'zfrom':zfrom, 'zto':zto, 'zuser':user})
    mood2.zwhen = pd.to_datetime(mood2.zwhen,unit='s') + APPLE_DELTA
    mood2.columns = ['user',
                     'recorded_timestamp',
                     'emotional',
                     'emo_overload',
                     'headneck',
                     'irritable',
                     'managing',
                     'mental_activ',
                     'phys_activ',
                     'stress',
                     'activity',
                     'alcohol',
                     'body_position',
                     'caffeine',
                     'mood',
                     'other_feelings',
                     'sleep']
    
    localize_time(mood2, user, con)

    return mood2

def read_feedback(user, time_from, hours=pd.to_timedelta(24,'h'), con=None, band_off_times=None):
    """
    Reads "hours" hours BACK from time_from of feedback data from a postgres database
    """
    if not con:
        con = create_connection()
        
    zfrom, zto = gen_time_preds(time_from, hours)
    feedback = pd.read_sql('SELECT ZUSER, ZWHEN, ZSELECTED FROM ZFEEDBACK WHERE ZWHEN >= %(zfrom)s AND ZWHEN < %(zto)s AND ZUSER = %(zuser)s  ORDER BY ZWHEN',con=con, params={'zfrom':zfrom, 'zto':zto, 'zuser':user})
    feedback.zwhen = pd.to_datetime(feedback.zwhen,unit='s') + APPLE_DELTA
    feedback.columns = ['user',
                        'recorded_timestamp',
                        'state']
    
    localize_time(feedback, user, con)

    return feedback



def read_data(user,time_from,hours=pd.to_timedelta(24,'h')):
    """
    Reads "hours" hours BACK from time_from
    of hrq, accel, moodactivity, r2r  from a postgres database
    to be re-implemented in sqlalchemy ORM way when required
    """
    con = create_connection()
    band_off_times = gen_band_off_times(time_from, hours, user, con)

    hrquality = read_hrquality(user, time_from, hours, con, band_off_times)
    accelerometer = read_accelerometer(user, time_from, hours, con, band_off_times)
    r2r = read_r2r(user, time_from, hours, con, band_off_times)
    mood = read_mood(user, time_from, hours, con, band_off_times)

    return hrquality, accelerometer,  r2r, mood


def band_off_filter(contact):
    #### Generate intervals when the band was not worn
    band_off_times = []
    band_off_pair = {}
    for row in contact.iterrows():
        if not band_off_pair:
            if row[1]['state'] == 0:
                band_off_pair['start'] = row[1]['recorded_timestamp']
        else:
            if row[1]['state'] == 1:
                band_off_pair['end'] = row[1]['recorded_timestamp']
                band_off_times.append(band_off_pair)
                band_off_pair = {}
    if band_off_pair:
        band_off_pair['end'] = pd.to_datetime('2099-07-16')
        band_off_times.append(band_off_pair)
    return band_off_times
